//
//  ApplicationCoordinator.swift
//  Boot
//
//  Created by Chris James on 06/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

final class AppCoordinator: BaseCoordinator {
    
    private let coordinatorFactory: CoordinatorFactory
    private let router: Router
    
    init(router: Router, coordinatorFactory: CoordinatorFactory) {
        self.router = router
        self.coordinatorFactory = coordinatorFactory
    }
    
    override func start(with option: DeepLinkOption?) {
        runLoginFlow(with: option)
    }
    
    private func runLoginFlow(with option: DeepLinkOption?) {
        let coordinator = coordinatorFactory.makeLoginCoordinator(router: router)
        addDependency(coordinator)
        coordinator.start(with: option)
    }
    
}
