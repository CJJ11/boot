//
//  AppDelegate.swift
//  Boot
//
//  Created by Chris James on 04/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit
import Firebase
import Kingfisher
import Agrume
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    
    private var appCoordinator: Coordinator?
    private var rootController: UINavigationController?

    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        setupAgrumeDownloader()
        Messaging.messaging().delegate = self
        UNUserNotificationCenter.current().delegate = self
        
        let window = UIWindow(frame: UIScreen.main.bounds)
         window.tintColor = UIColor(red: 173/255, green: 139/255, blue: 59/255, alpha: 1)
        
        let navController = UINavigationController()
        navController.setNavigationBarHidden(true, animated: false)
        navController.navigationBar.isHidden = true
        navController.isNavigationBarHidden = true
        
        let appCoordinator = AppCoordinator(
            router: RouterImp(navController: navController),
            coordinatorFactory: CoordinatorFactoryImp()
        )
        
        self.window = window
        self.appCoordinator = appCoordinator
        self.rootController = navController
        
        let notification = launchOptions?[.remoteNotification] as? [String: AnyObject]
        let deepLink = DeepLinkOption.build(with: notification)
        
        window.rootViewController = rootController
        window.makeKeyAndVisible()
        appCoordinator.start(with: deepLink)
        return true
    }
    

    private func setupAgrumeDownloader(){
        AgrumeServiceLocator.shared.setDownloadHandler { url, completion in
            KingfisherManager.shared.retrieveImage(with: url){result in
                switch result {
                case .success(let value):
                    completion(value.image)
                    
                case .failure:
                    completion(nil)
                }
            }
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        let dict = userInfo as? [String: AnyObject]
        let deepLink = DeepLinkOption.build(with: dict)
        appCoordinator?.start(with: deepLink)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }

}

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert, .badge, .sound])
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo
        let dict = userInfo as? [String: AnyObject]
        let deepLink = DeepLinkOption.build(with: dict)
        appCoordinator?.start(with: deepLink)
        
        completionHandler()
    }
}


extension AppDelegate : MessagingDelegate {

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        
        if let uid = Auth.auth().currentUser?.uid {
            let ref = Firestore.firestore().collection("minesites").document("cadia").collection("users").document(uid)
            
            ref.updateData(["\(Profile.notificationTokenKey).\(fcmToken)": true]) { err in
                if let err = err {
                    print("Error updating document: \(err)")
                } else {
                    print("Document successfully updated")
                }
            }
        }
    
    }

}
