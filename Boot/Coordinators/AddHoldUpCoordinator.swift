//
//  AddHoldUpCoordinator.swift
//  Boot
//
//  Created by Chris James on 13/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit
import CoreServices
import AVFoundation

protocol AddHoldUpCoordinatorOutput: CoordinatorOutput {
    
}

class AddHoldUpCoordinator: BaseCoordinator, AddHoldUpCoordinatorOutput {
    
    private let auth: AuthSource
    private let router: Router
    private let moduleFactory: ModuleFactory
    private let coordinatorFactory: CoordinatorFactory
    
    var finishFlow: (() -> Void)?

    init(auth: AuthSource,
         router: Router,
         moduleFactory: ModuleFactory,
         coordinatorFactory: CoordinatorFactory){
        
        self.auth = auth
        self.router = router
        self.moduleFactory = moduleFactory
        self.coordinatorFactory = coordinatorFactory
    }
    
    override func start(from router: Router) {
        showAddHoldUpView(parentRouter: router)
    }
    
    //MARK: - Show current flow's controllers
    
    private func showAddHoldUpView(parentRouter: Router) {
        guard let uid = auth.getUid(),
            let username = auth.getName(),
            let profileUrl = auth.getProfileUrl() else {return}
        
        let addHoldUpView = moduleFactory.makeAddHoldUpView(
            uid: uid,
            username: username,
            profileUrl: profileUrl
        )

        addHoldUpView.closeBtnTapped = {[weak self] in
            self?.finishFlow?()
        }

        router.setRootModule(addHoldUpView, hideNavBar: false)
        parentRouter.present(router, animated: true, completion: nil)
    }
    
}


