//
//  AddInspectionCoordinator.swift
//  Boot
//
//  Created by Chris James on 23/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation

protocol AddInspectionCoordinatorOutput: CoordinatorOutput {
    //var finishFlow: (() -> Void)? { get set }
}

class AddInspectionCoordinator: BaseCoordinator, AddInspectionCoordinatorOutput {
    
    var finishFlow: (() -> Void)?
    
    private let auth: AuthSource
    private let router: Router
    private let moduleFactory: ModuleFactory
    private let coordinatorFactory: CoordinatorFactory
    private let dataSourceFactory: DataSourceFactory
    
    init(auth: AuthSource,
         router: Router,
         moduleFactory: ModuleFactory,
         coordinatorFactory: CoordinatorFactory,
         dataSourceFactory: DataSourceFactory){
        
        self.auth = auth
        self.router = router
        self.moduleFactory = moduleFactory
        self.coordinatorFactory = coordinatorFactory
        self.dataSourceFactory = dataSourceFactory
    }
    
    override func start(from router: Router) {
        showAddInspectionView(parentRouter: router)
    }
    
    //MARK: - Show current flow's controllers

    private func showAddInspectionView(parentRouter: Router) {
        let dataSource = dataSourceFactory.makeAddInspectionDataSource(authSource: auth)
        let addInspectionView = moduleFactory.makeAddInspectionView(dataSource: dataSource)
        addInspectionView.closeBtnTapped = {[weak self] in
            self?.finishFlow?()
        }
        
        router.setRootModule(addInspectionView, hideNavBar: false)
        parentRouter.present(router, animated: true, completion: {
            dataSource.startListening()
        })
    }

}


