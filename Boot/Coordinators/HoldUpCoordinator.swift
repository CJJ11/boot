//
//  HazardCoordinator.swift
//  Boot
//
//  Created by Chris James on 12/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation

protocol HoldUpCoordinatorOutput: CoordinatorOutput {
    var logout: (() -> Void)? {get set}
}

class HoldUpCoordinator: BaseCoordinator, HoldUpCoordinatorOutput {
    
    private let auth: AuthSource
    private let role: RoleType
    private let router: Router
    private let moduleFactory: ModuleFactory
    private let coordinatorFactory: CoordinatorFactory
    private let dataSourceFactory: DataSourceFactory
    private var holdUpDataSource: HoldUpDataSource?
    
    var logout: (() -> Void)?
    var finishFlow: (() -> Void)?

    
    init(auth: AuthSource,
         role: RoleType,
         router: Router,
         moduleFactory: ModuleFactory,
         coordinatorFactory: CoordinatorFactory,
         dataSourceFactory: DataSourceFactory){
        
        self.auth = auth
        self.role = role
        self.router = router
        self.moduleFactory = moduleFactory
        self.coordinatorFactory = coordinatorFactory
        self.dataSourceFactory = dataSourceFactory
    }
    
    override func start() {
        showHoldUpView()
    }
    
    //MARK: - Show current flow's controllers
    
    private func showHoldUpView() {
        let dataSource = dataSourceFactory.makeHoldUpDataSource(authSource: auth)
        holdUpDataSource = dataSource
        let holdUpView = moduleFactory.makeHoldUpView(role: role, dataSource: dataSource)
        
        holdUpView.onAddHoldUpBtnTapped =  { [weak self] in
            self?.runAddHoldUpFlow()
        }
        
        holdUpView.onProfileTapped = {[weak self] in
            self?.runProfileFlow()
        }

        router.setRootModule(holdUpView, hideNavBar: false)
    }
    
    //MARK: - Run other flows
    
        private func runAddHoldUpFlow() {
            var coordinator = coordinatorFactory.makeAddHoldUpCoordinator()
            
            coordinator.finishFlow = {[weak self, weak coordinator] in
                self?.router.dismissModule(animated: true, completion: nil)
                self?.removeDependency(coordinator)
            }
            addDependency(coordinator)
            coordinator.start(from: router)
        }
    
    private func runProfileFlow() {
        var coordinator = coordinatorFactory.makeProfileCoordinator()
        addDependency(coordinator)
        
        coordinator.finishFlow = {[weak self, weak coordinator] in
            self?.router.dismissModule(animated: true, completion: nil)
            self?.removeDependency(coordinator)
        }
        
        coordinator.logout = {[weak self, weak coordinator] in
            self?.router.dismissModule(animated: true, completion: nil)
            self?.removeDependency(coordinator)
            self?.logout?()
        }
        coordinator.start(from: router)
    }
    
    deinit {
        holdUpDataSource?.stopListening()
    }
}
