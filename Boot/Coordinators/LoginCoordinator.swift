//
//  LoginCoordinator.swift
//  Boot
//
//  Created by Chris James on 10/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UserNotifications
import UIKit
import FirebaseFirestore


protocol LoginCoordinatorOutput: CoordinatorOutput {
    
}

class LoginCoordinator: BaseCoordinator {
    
    private let router: Router
    private let moduleFactory: ModuleFactory
    private let coordinatorFactory: CoordinatorFactory
    private let auth: AuthSource
    
    private var loginView: LoginView?

    
    var finishFlow: (() -> Void)?
    
    init(auth: AuthSource,
         router: Router,
         moduleFactory: ModuleFactory,
         coordinatorFactory: CoordinatorFactory){
        
        self.auth = auth
        self.router = router
        self.moduleFactory = moduleFactory
        self.coordinatorFactory = coordinatorFactory
    }
    
    override func start(with option: DeepLinkOption?) {
        
        showLoginView()
        auth.signedIn {[weak self] signedIn, asSupervisor in
            if signedIn && asSupervisor {
                self?.runSupervisorFlow(with: option)
            } else if signedIn {
                self?.runTechnicianFlow()
            } else {
                self?.loginView?.triggerAnimation()
            }
        }
    }
    
    
    //MARK: - Show current flow's controllers
    
    private func showLoginView() {
        loginView = moduleFactory.makeLoginView()
        
        loginView?.onSupervisorSelect = {[weak self] in
            self?.runSupervisorFlow()
        }
        
        loginView?.onTechnicianSelect = {[weak self] in
            self?.runTechnicianFlow()
        }
        
        router.setRootModule(loginView, hideNavBar: true)
    }
    
    private func registerForRemoteNotifications(){
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    
    //MARK: - Run other flows
    
    private func runSupervisorFlow(with option: DeepLinkOption? = nil) {
        registerForRemoteNotifications()
        var coordinator = coordinatorFactory.makeSupervisorTabBarCoordinator(router: router)
        coordinator.logout = {[weak self, weak coordinator] in
            DispatchQueue.main.async {
                self?.router.popToRootModule(animated: true)
                self?.removeDependency(coordinator)
                self?.loginView?.triggerAnimation()
            }
        }
        addDependency(coordinator)
        coordinator.start(with: option)
    }
    
    private func runTechnicianFlow() {
        var coordinator = coordinatorFactory.makeTechnicianTabBarCoordinator(router: router)
        coordinator.logout = {[weak self, weak coordinator] in
            DispatchQueue.main.async {
            self?.router.popToRootModule(animated: true)
            self?.removeDependency(coordinator)
            self?.loginView?.triggerAnimation()
            }
        }
        addDependency(coordinator)
        coordinator.start()
    }
    

    //    deinit {
    //        notificationDataSource?.stopListening()
    //    }
}
