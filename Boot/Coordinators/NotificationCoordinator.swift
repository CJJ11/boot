//
//  NotificationCoordinator.swift
//  Boot
//
//  Created by Chris James on 05/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation

protocol NotificationCoordinatorOutput: CoordinatorOutput {
    var logout: (() -> Void)? {get set}
    var showInspection:(() -> Void)? {get set}
    var showHoldUp: (() -> Void)? {get set}
}

class NotificationCoordinator: BaseCoordinator, NotificationCoordinatorOutput {
    
    private let auth: AuthSource
    private let router: Router
    private let moduleFactory: ModuleFactory
    private let coordinatorFactory: CoordinatorFactory
    private let dataSourceFactory: DataSourceFactory
    private var notificationDataSource: NotificationDataSource?
    
    var showInspection:(() -> Void)?
    var showHoldUp: (() -> Void)?
    var finishFlow: (() -> Void)?
    var logout: (() -> Void)?
    
    init(auth: AuthSource,
         router: Router,
         moduleFactory: ModuleFactory,
         coordinatorFactory: CoordinatorFactory,
         dataSourceFactory: DataSourceFactory){
        
        self.auth = auth
        self.router = router
        self.moduleFactory = moduleFactory
        self.coordinatorFactory = coordinatorFactory
        self.dataSourceFactory = dataSourceFactory
    }
    
    override func start() {
        showNotificationView()
    }
    
    //MARK: - Show current flow's controllers
    
    private func showNotificationView() {
        guard let dataSource = dataSourceFactory.makeNotificationDataSource(authSource: auth)
            else {return}
        
        notificationDataSource = dataSource
        let notificationView = moduleFactory.makeNotificationView(dataSource: dataSource)
        
        notificationView.onNotificationSelect =  { [weak self] viewModel in
            
            let type = viewModel.notification.type
            
            switch type {
            case .inspectionFinished, .inspectionStarted:
                self?.showInspection?()
                print("Inspection noti")
            case .holdUpCreated:
                self?.showHoldUp?()
                print("Hold up noti")
            }
        }

        notificationView.onProfileTapped = {[weak self] in
            self?.runProfileFlow()
        }
        
        router.setRootModule(notificationView, hideNavBar: false)
    }
    
    //MARK: - Run other flows
    
    private func runProfileFlow() {
        var coordinator = coordinatorFactory.makeProfileCoordinator()
        addDependency(coordinator)
        
        coordinator.finishFlow = {[weak self, weak coordinator] in
            self?.router.dismissModule(animated: true, completion: nil)
            self?.removeDependency(coordinator)
        }
        
        coordinator.logout = {[weak self, weak coordinator] in
            self?.router.dismissModule(animated: true, completion: nil)
            self?.removeDependency(coordinator)
            self?.logout?()
        }
        coordinator.start(from: router)
    }

}
