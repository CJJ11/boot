//
//  ProfileCoordinator.swift
//  Boot
//
//  Created by Chris James on 12/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import FirebaseFirestore
import FirebaseMessaging


protocol ProfileCoordinatorOutput: CoordinatorOutput {
    var logout: (() -> Void)? {get set}
}

class ProfileCoordinator: BaseCoordinator, ProfileCoordinatorOutput {
    
    private let auth: AuthSource
    private let router: Router
    private let moduleFactory: ModuleFactory
    private let coordinatorFactory: CoordinatorFactory
    private let dataSourceFactory: DataSourceFactory
    
    var finishFlow: (() -> Void)?
    var logout: (() -> Void)?
    
    init(auth: AuthSource,
         router: Router,
         moduleFactory: ModuleFactory,
         coordinatorFactory: CoordinatorFactory,
         dataSourceFactory: DataSourceFactory){
        
        self.auth = auth
        self.router = router
        self.moduleFactory = moduleFactory
        self.coordinatorFactory = coordinatorFactory
        self.dataSourceFactory = dataSourceFactory
    }
    
    override func start(from router: Router) {
        showProfileView(parentRouter: router)
    }
    //MARK: - Show current flow's controllers
    
    private func showProfileView(parentRouter: Router) {
        guard let dataSource = dataSourceFactory.makeProfileDataSource(authSource: auth) else {return}
        let profileView = moduleFactory.makeProfileView(dataSource: dataSource)
        
        profileView.onCloseTapped = { [weak self] in
            self?.finishFlow?()
        }
        
        profileView.onLogoutTapped = {[weak self] in
            self?.removeNotiTokenAndSignOut()
        }
        
        router.setRootModule(profileView, hideNavBar: false)
        parentRouter.present(router, animated: true, completion: nil)
    }
    
    private func removeNotiTokenAndSignOut(){
        guard let uid = auth.getUid() else {
            UIApplication.shared.applicationIconBadgeNumber = 0
            logout?()
            return
        }
        
        if let token = Messaging.messaging().fcmToken {
            let ref = Firestore.firestore().collection("minesites")
                .document("cadia").collection("users").document(uid)
            
            ref.updateData(
                ["\(Profile.notificationTokenKey).\(token)": FieldValue.delete()]
            ) {[weak self] error in
                
                self?.auth.signOut(completion: { (success) in
                    UIApplication.shared.applicationIconBadgeNumber = 0
                    self?.logout?()
                })
            }
        } else {
            auth.signOut(completion: {[weak self] (success) in
                UIApplication.shared.applicationIconBadgeNumber = 0
                self?.logout?()
            })
        }
    }
    

    
    
    

}
