//
//  StepCoordinator.swift
//  Boot
//
//  Created by Chris James on 07/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

protocol SupervisorInspectionCoordinatorOutput: class {
    var finishFlow: (() -> Void)? { get set }
}

class SupervisorInspectionCoordinator: BaseCoordinator, SupervisorInspectionCoordinatorOutput {
    
    var finishFlow: (() -> Void)?
    
    private let auth: AuthSource
    private let router: Router
    private let moduleFactory: ModuleFactory
    private let coordinatorFactory: CoordinatorFactory
    private let dataSourceFactory: DataSourceFactory
    
    init(
        auth: AuthSource,
         router: Router,
         moduleFactory: ModuleFactory,
         coordinatorFactory: CoordinatorFactory,
         dataSourceFactory: DataSourceFactory){
        
        self.auth = auth
        self.router = router
        self.moduleFactory = moduleFactory
        self.coordinatorFactory = coordinatorFactory
        self.dataSourceFactory = dataSourceFactory
    }
    
    override func start() {
        showInspectionView()
    }
    
    //MARK: - Show current flow's controllers
    
    private func showInspectionView() {
        let dataSource = dataSourceFactory.makeSupervisorInspDataSource(authSource: auth)
        let inspectionView = moduleFactory.makeInspectionView(dataSource: dataSource, roleType: .supervisor)
        
        inspectionView.onInspectionSelect = { [weak self] (viewModel) in
            if let inspection = viewModel.inspection {
                self?.showStepView(inspection: inspection)
            }
        }
        
        inspectionView.onAddInspectionTapped = {[weak self] in
            self?.runAddInspectionFlow()
        }
        
        router.setRootModule(inspectionView, hideNavBar: false)
    }
    
    private func showInspectionDetailView(inspection: Inspection) {
        let inspectionDetailView = moduleFactory.makeInspectionDetailView(inspection: inspection)
        router.push(inspectionDetailView, animated: true, hideToolBar: true, completion: nil)
    }
    
    private func showStepView(inspection: Inspection){
        let dataSource = dataSourceFactory.makeStepDataSource(authSource: auth, inspection: inspection)
        let stepView = moduleFactory.makeStepView(
            dataSource: dataSource,
            inspection: inspection,
            roleType: .supervisor
        )
        
        stepView.onInspectionSignedOff = {[weak self] in
            self?.router.popModule(animated: true)
        }
        
        stepView.onInspectionCancelled = {[weak self] in
            self?.router.popModule(animated: true)
        }
        router.push(stepView, animated: true, hideToolBar: true, completion: nil)
        dataSource.startListening()
    }
    
    
    //MARK: - Run other flows
    
    private func runAddInspectionFlow(){
        var coordinator = coordinatorFactory.makeAddInspectionCoordinator()
        coordinator.finishFlow = {[weak self, weak coordinator] in
            self?.router.dismissModule(animated: true, completion: nil)
            self?.removeDependency(coordinator)
        }
        addDependency(coordinator)
        coordinator.start(from: router)
    }
    
}
