//
//  TabBarCoordinator.swift
//  Boot
//
//  Created by Chris James on 11/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

protocol SupervisorTabBarCoordinatorOutput: CoordinatorOutput {
    var logout: (() -> Void)? {get set}
}

class SupervisorTabBarCoordinator: BaseCoordinator, SupervisorTabBarCoordinatorOutput {
    
    private let auth: AuthSource
    private let moduleFactory: ModuleFactory
    private let router: Router
    private let coordinatorFactory: CoordinatorFactory
    private let dataSourceFactory: DataSourceFactory
    private var tabBarDataSource: TabBarDataSource?
    private var tabBarView: SupervisorTabBarView?
    
    var finishFlow: (() -> Void)?
    var logout: (() -> Void)?
    
    init(
        auth: AuthSource,
        router: Router,
        moduleFactory: ModuleFactory,
        coordinatorFactory: CoordinatorFactory,
        dataSourceFactory: DataSourceFactory) {
        
        self.auth = auth
        self.router = router
        self.moduleFactory = moduleFactory
        self.coordinatorFactory = coordinatorFactory
        self.dataSourceFactory = dataSourceFactory
    }
    
    override func start(with option: DeepLinkOption?) {
        guard let dataSource = dataSourceFactory.makeTabBarDataSource(authSource: auth) else {return}
        let (tabBarView, inspectionNavController) = moduleFactory.makeSupervisorTabBarView(dataSource: dataSource)
        self.tabBarDataSource  = dataSource
        
        tabBarView.onInspectionFlowSelect = showInspectionFlow
        tabBarView.onHoldUpFlowSelect = showHoldUpFlow
        tabBarView.onNotificationFlowSelect = showNotificationFlow
        
        if let option = option {
            switch option {
            case .inspection(let inspectionId):
                print("Inspection deep link option: \(inspectionId ?? "couldn't find ID")")
                tabBarView.showTab(tab: .inspections)
                
            case .holdUp(let holdUpId):
                print("Hold up deep link option: \(holdUpId ?? "couldn't find ID")")
                tabBarView.showTab(tab: .holdUps)
            }
        } else {
            let inspectionCoordinator = self.coordinatorFactory.makeSupervisorInspectionCoordinator(
                navController: inspectionNavController
            )
            self.addDependency(inspectionCoordinator)
            inspectionCoordinator.start()
        }
        
        self.tabBarView = tabBarView
        router.push(tabBarView, animated: false, hideToolBar: false, completion: nil)
    }
    
    
    private func showInspectionFlow(navController: UINavigationController){
        if navController.viewControllers.isEmpty == true {
            let inspectionCoordinator = self.coordinatorFactory.makeSupervisorInspectionCoordinator(
                navController: navController
            )
            self.addDependency(inspectionCoordinator)
            inspectionCoordinator.start()
        }
    }
    
    private func showHoldUpFlow(navController: UINavigationController){
        if navController.viewControllers.isEmpty == true {
            let holdUpCoordinator = self.coordinatorFactory.makeHoldUpCoordinator(
                role: .supervisor,
                navController: navController
            )
            self.addDependency(holdUpCoordinator)
            holdUpCoordinator.start()
        }
    }
    
    private func showNotificationFlow(navController: UINavigationController){
        if navController.viewControllers.isEmpty == true {
            var coordinator = self.coordinatorFactory.makeNotificationCoordinator(
                navController: navController
            )
            
            coordinator.finishFlow = {[weak self, weak coordinator] in
                self?.removeDependency(coordinator)
            }
            
            coordinator.logout = {[weak self, weak coordinator] in
                self?.removeDependency(coordinator)
                self?.logout?()
            }
            
            coordinator.showHoldUp = {[weak self] in
                self?.tabBarView?.showTab(tab: .holdUps)
            }
            
            coordinator.showInspection = {[weak self] in
                self?.tabBarView?.showTab(tab: .inspections)
            }
            
            self.addDependency(coordinator)
            coordinator.start()
        }
    }
    
}
