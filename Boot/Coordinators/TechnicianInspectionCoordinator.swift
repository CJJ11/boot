//
//  TechnicianInspectionCoordinator.swift
//  Boot
//
//  Created by Chris James on 24/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

protocol TechnicianInspectionCoordinatorOutput: class {
    var finishFlow: (() -> Void)? { get set }
}

class TechnicianInspectionCoordinator: BaseCoordinator, TechnicianInspectionCoordinatorOutput {
    
    var finishFlow: (() -> Void)?
    private let auth: AuthSource
    private let router: Router
    private let moduleFactory: ModuleFactory
    private let coordinatorFactory: CoordinatorFactory
    private let dataSourceFactory: DataSourceFactory
    
    private var selectedInspection: Inspection?
    private var inspectionDataSource: TechInspectionDataSource?
    private var checkDataSources = [Int: CheckDataSource]()
    
    
    init(auth: AuthSource,
         router: Router,
         moduleFactory: ModuleFactory,
         coordinatorFactory: CoordinatorFactory,
         dataSourceFactory: DataSourceFactory){
        
        self.auth = auth
        self.router = router
        self.moduleFactory = moduleFactory
        self.coordinatorFactory = coordinatorFactory
        self.dataSourceFactory = dataSourceFactory
    }
    
    override func start() {
        showInspectionView()
    }
    
    //MARK: - Show current flow's controllers
    
    private func showInspectionView() {
        let dataSource = dataSourceFactory.makeTechInspectionDataSource(authSource: auth)
        inspectionDataSource = dataSource
        let inspectionView = moduleFactory.makeTechInspectionView(dataSource: dataSource)
        
        inspectionView.onInspectionSelect = { [weak self] (viewModel) in
            
            if let inspection = viewModel.inspection {
                self?.selectedInspection = inspection
            if viewModel.status == .signOff || viewModel.status == .complete {
                self?.showStepView(inspection: inspection)
            } else {
                self?.showInspectionDetailView()
            }
            }
        }
        
        router.setRootModule(inspectionView, hideNavBar: false)
    }
    
    
    private func showStepView(inspection: Inspection){
        let dataSource = dataSourceFactory.makeStepDataSource(authSource: auth, inspection: inspection)
        let stepView = moduleFactory.makeStepView(
            dataSource: dataSource,
            inspection: inspection,
            roleType: .technician
        )
        router.push(stepView, animated: true, hideToolBar: true, completion: nil)
    }
    
    
    private func showInspectionDetailView() {
        guard let inspection = selectedInspection else {return}
        let inspectionDetailView = moduleFactory.makeInspectionDetailView(inspection: inspection)
        inspectionDetailView.startInspection = {[weak self] in
            self?.showCheckView(stepNumber: 0)
        }
        router.push(inspectionDetailView, animated: true, hideToolBar: true, completion: nil)
    }
    
    
    private func showCheckView(stepNumber: Int){
        guard let inspection = selectedInspection else {return}

        let dataSource = dataSourceFactory.makeCheckDataSource(
            authSource: auth,
            inspectionUid: inspection.uid,
            stepNumber: stepNumber
        )
        checkDataSources[stepNumber] = dataSource

        let title = "Step \(stepNumber + 1) of \(inspection.steps.count)"
        let checkView = moduleFactory.makeCheckView(
            dataSource: dataSource,
            title: title,
            stepNumber: stepNumber
        )
        
        checkView.onNextTapped = {[weak self] (stepNumber) in
            let nextStepNumber = stepNumber + 1
            if nextStepNumber < inspection.steps.count {
                self?.showCheckView(stepNumber: nextStepNumber)
                
            } else {
                let title = self?.checkDataSources[stepNumber]?.getOverviewTitle() ?? "Last step reached"
                let message = self?.checkDataSources[stepNumber]?.getOverviewDescription()
                let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { (action) in
                    self?.router.popToRootModule(animated: true)
                    self?.checkDataSources.removeAll()
                }))
                
                self?.router.present(alert, animated: true, completion: nil)
            }
        }
        
        checkView.onAddHoldUpTapped = {[weak self] in
            self?.runAddHoldUpFlow()
        }
        
        router.push(checkView, animated: true, hideToolBar: true, completion: nil)
    }
    
    //MARK: - Run other flows
    
    private func runAddHoldUpFlow() {
        var coordinator = coordinatorFactory.makeAddHoldUpCoordinator()
        
        coordinator.finishFlow = {[weak self, weak coordinator] in
            self?.router.dismissModule(animated: true, completion: nil)
            self?.removeDependency(coordinator)
        }
        addDependency(coordinator)
        coordinator.start(from: router)
    }
    
    deinit {
        inspectionDataSource?.stopListening()
        checkDataSources.values.forEach({$0.stopListening()})
    }
}
