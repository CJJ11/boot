//
//  TechnicianTabBarCoordinator.swift
//  Boot
//
//  Created by Chris James on 24/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

protocol TechnicianTabBarCoordinatorOutput: CoordinatorOutput {
    var logout: (() -> Void)? {get set}
}

class TechnicianTabBarCoordinator: BaseCoordinator, TechnicianTabBarCoordinatorOutput {
    
    private let auth: AuthSource
    private let moduleFactory: ModuleFactory
    private let router: Router
    private let coordinatorFactory: CoordinatorFactory
    private let dataSourceFactory: DataSourceFactory
    private var tabBarDataSource: TabBarDataSource?
    
    var finishFlow: (() -> Void)?
    var logout: (() -> Void)?
    
    init(
        auth: AuthSource,
        router: Router,
        moduleFactory: ModuleFactory,
        coordinatorFactory: CoordinatorFactory,
        dataSourceFactory: DataSourceFactory) {
        
        self.auth = auth
        self.router = router
        self.moduleFactory = moduleFactory
        self.coordinatorFactory = coordinatorFactory
        self.dataSourceFactory = dataSourceFactory
    }
    
    override func start() {
        guard let dataSource = dataSourceFactory
            .makeTabBarDataSource(authSource: auth) else {return}
        let (tabBarView, inspectionNavController) = moduleFactory
            .makeTechnicianTabBarView(dataSource: dataSource)
        self.tabBarDataSource  = dataSource
        
        let inspectionCoordinator = self.coordinatorFactory
            .makeTechnicianInspectionCoordinator(navController: inspectionNavController)
        self.addDependency(inspectionCoordinator)
        inspectionCoordinator.start()
        
        tabBarView.onInspectionFlowSelect = showInspectionFlow
        tabBarView.onHoldUpFlowSelect = showHoldUpFlow
        tabBarView.onNotificationFlowSelect = showNotificationFlow

        router.push(tabBarView, animated: false, hideToolBar: false, completion: nil)
    }
    
    private func showInspectionFlow(navController: UINavigationController){
        if navController.viewControllers.isEmpty == true {
            let inspectionCoordinator = self.coordinatorFactory
                .makeTechnicianInspectionCoordinator(navController: navController)
            self.addDependency(inspectionCoordinator)
            inspectionCoordinator.start()
        }
    }
    
    private func showHoldUpFlow(navController: UINavigationController){
        if navController.viewControllers.isEmpty == true {
            var coordinator = self.coordinatorFactory
                .makeHoldUpCoordinator(role: .technician, navController: navController)
            
            coordinator.finishFlow = {[weak self, weak coordinator] in
                self?.removeDependency(coordinator)
            }
            
            coordinator.logout = {[weak self, weak coordinator] in
                self?.removeDependency(coordinator)
                self?.logout?()
            }
            
            self.addDependency(coordinator)
            coordinator.start()
        }
    }
    
    private func showNotificationFlow(navController: UINavigationController){
        if navController.viewControllers.isEmpty == true {
            var coordinator = self.coordinatorFactory
                .makeNotificationCoordinator(navController: navController)
            
            coordinator.finishFlow = {[weak self, weak coordinator] in
                self?.removeDependency(coordinator)
            }
            
            coordinator.logout = {[weak self, weak coordinator] in
                self?.removeDependency(coordinator)
                self?.logout?()
            }
            
            self.addDependency(coordinator)
            coordinator.start()
        }
    }
    
}
