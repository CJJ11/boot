//
//  CheckType.swift
//  Boot
//
//  Created by Chris James on 05/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation

enum CheckType: Int {
    case simpleBoolean = 0
    case slider = 1
    case picker = 2
    
//    var description: String {
//        get{
//            switch self{
//            case .hat: return "Hard Hat"
//            case .boots: return "Safety Boots"
//            case .glasses: return "Safety Glasses"
//            case .goggles: return "Safety Goggles"
//            case .earplugs: return "Ear Plugs"
//            case .earmuffs: return "Ear Muffs"
//            case .gloves: return "Gloves"
//            }
//        }
//    }
//
//    var image: UIImage {
//        get{
//            switch self{
//            case .hat: return #imageLiteral(resourceName: "second")
//            case .boots: return #imageLiteral(resourceName: "second")
//            case .glasses: return #imageLiteral(resourceName: "second")
//            case .goggles: return #imageLiteral(resourceName: "second")
//            case .earplugs: return #imageLiteral(resourceName: "second")
//            case .earmuffs: return #imageLiteral(resourceName: "second")
//            case .gloves: return #imageLiteral(resourceName: "second")
//            }
//        }
//    }
    
}
