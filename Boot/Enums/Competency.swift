//
//  Competency.swift
//  Boot
//
//  Created by Chris James on 18/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation

enum Competency: Int {
    case workingAtHeights = 0
    
    static let count = 1
    
    var description: String {
        switch(self){
        case .workingAtHeights: return "Working at Heights"
        }
    }
}
