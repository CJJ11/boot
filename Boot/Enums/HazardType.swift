//
//  Hazards.swift
//  boot
//
//  Created by Chris James on 04/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

enum HazardType: Int, CaseIterable {
    case fallsSlipsAndTrips = 0
    case confinedSpaces = 1
    case fallingObjects = 2
    case manualHandling = 3
    case noise = 4
    case dustFumeAndVapour = 5
    case pressure = 6
    case temperature = 7
    case electrical = 8
    case fireAndExplosion = 9
    case chemical = 10
    case uv = 11
    case radiation = 12
    case animal = 13
    case biological = 14
    case vibration = 15
    case kinetic = 16
    case mechanical = 17
    case proximity = 18
    case interface = 19
    case sharp = 20
    case vacuum = 21
    case stressors = 22
    case other = 23
    
    var description: String {
        get{
            switch self{
            case .fallsSlipsAndTrips: return "Falls, slips & trips"
            case .confinedSpaces: return "Confined spaces"
            case .fallingObjects: return "Falling objects"
            case .manualHandling: return "Manual handling"
            case .noise: return "Noise"
            case .dustFumeAndVapour: return "Dust, fume & vapour"
            case .pressure: return "Pressure"
            case .temperature: return "Temperature"
            case .electrical: return "Electrical"
            case .fireAndExplosion: return "Fire & explosion"
            case .chemical: return "Chemical"
            case .uv: return "UV"
            case .radiation: return "Radiation"
            case .animal: return "Animal"
            case .biological: return "Biological"
            case .vibration: return "Vibration"
            case .kinetic: return "Kinetic"
            case .mechanical: return "Mechanical"
            case .proximity: return "Proximity"
            case .interface: return "Interface"
            case .sharp: return "Sharp"
            case .vacuum: return "Vacuum"
            case .stressors: return "Stressors"
            case .other: return "Other"
            }
        }
    }
    
    var image: UIImage {
        get{
            switch self{
            case .fallsSlipsAndTrips: return UIImage(named: "hazardWorkingAtHeights")!
            case .confinedSpaces: return UIImage(named: "hazardConfinedSpace")!
            case .fallingObjects: return UIImage(named: "hazardFallingObjects")!
            case .electrical: return UIImage(named: "hazardElectricity")!
            case .radiation: return UIImage(named: "hazardRadiation")!
            case .mechanical: return UIImage(named: "hazardPinchPoints")!
            default: return UIImage(named: "hazardGeneral")!
                
                //                case .manualHandling: return "Manual handling"
                //                case .noise: return "Noise"
                //                case .dustFumeAndVapour: return "Dust, fume & vapour"
                //                case .pressure: return "Pressure"
                //                case .temperature: return "Temperature"
                //
                //                case .fireAndExplosion: return "Fire & explosion"
                //                case .chemical: return "Chemical"
                //                case .uv: return "UV"
                //
                //                case .animal: return "Animal"
                //                case .biological: return "Biological"
                //                case .vibration: return "Vibration"
                //                case .kinetic: return "Kinetic"
                //
                //                case .proximity: return "Proximity"
                //                case .interface: return "Interface"
                //                case .sharp: return "Sharp"
                //                case .vacuum: return "Vacuum"
                //                case .stressors: return "Stressors"
                //                case .other: return "Other"
            }
        }
    }
    
}
