//
//  HoldUpStatus.swift
//  Boot
//
//  Created by Chris James on 30/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

enum HoldUpStatus: Int, CaseIterable {
    case open = 0
    case closed = 1
    
    static let count = 2
    
    var title: String {
        switch self {
        case .open: return "Open"
        case .closed: return "Closed"
        }
    }
    
    var emptyText: String {
        switch self {
        case .open: return "Hit the + in the top corner to add a hold-up"
        case .closed: return "Hold-ups that have been closed will appear here"
        }
        
    }
    
    var emptyImage: UIImage {
        switch self {
        case .open: return #imageLiteral(resourceName: "ListAdd")
        case .closed: return #imageLiteral(resourceName: "ListDone")
        }
    }
}

