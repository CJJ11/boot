//
//  InspectionStatus.swift
//  Boot
//
//  Created by Chris James on 16/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

enum InspectionStatus: Int, CaseIterable {
    case boot = 0
    case notStarted = 1
    case underway = 2
    case signOff = 3
    case complete = 4
    case cancelled = 5
    
    static let count = 6
    
    var title: String {
        switch self {
        case .boot: return ""
        case .notStarted: return "Not Started"
        case .underway: return "Underway"
        case .signOff: return "Sign Off"
        case .complete: return "Complete"
        case .cancelled: return "Cancelled"
        }
    }
    
    var cell: String {
        switch self {
        case .boot: return String(describing: InspBootCell.self)
        case .notStarted: return String(describing: InspNotStartedCell.self)
        case .underway: return String(describing: InspUnderwayCell.self)
        case .signOff: return String(describing: InspSignOffCell.self)
        case .complete: return String(describing: InspCompleteCell.self)
        case .cancelled: return String(describing: InspCancelledCell.self)
        }
    }
    
    var overview: String {
        switch self {
        case .boot: return ""
        case .notStarted: return "The inspection has not been started."
        case .underway: return "The inspection can be resumed at any time."
        case .signOff: return "The inspection has been sent to your supervisor for sign-off."
        case .complete: return "The inspection is now completed."
        case .cancelled: return "The inspection has been cancelled."
        }
    }
    
    var techDesc: String {
        switch self {
        //case .holdUp: return "Held Up"
        case .notStarted: return"To Do"
        case .underway: return "Started"
        case .signOff: return "Done"
        default: return ""
        }
    }
    
    var techEmptyImage: UIImage {
        switch self {
        case .signOff: return #imageLiteral(resourceName: "Happy")
        case .notStarted: return #imageLiteral(resourceName: "ListDone")
        case .underway: return #imageLiteral(resourceName: "Timer")
        default: return UIImage()
        }
        
    }
    
    var techEmptyText: String {
        switch self {
        case .signOff: return "Inspections that are completed will appear here"
        case .notStarted: return "Inspections that need doing will appear here"
        case .underway: return "Inspections that have been started will appear here"
        default: return ""
        }
    }

}
//extension InspectionStatus {
//    //Initializer to enable custom sorting of inspections in a tableView without having to
//    //change the raw values.  Allows new InspectionStatus values to be added easily in the future.
//    init?(sortValue: Int) {
//        switch sortValue {
//        case 0: self = .holdUp
//        case 1: self = .notStarted
//        case 2: self = .underway
//        case 3: self = .complete
//        case 4: self = .cancelled
//        default: return nil
//        }
//    }
//}
