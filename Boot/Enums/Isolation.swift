//
//  IsolationType.swift
//  boot
//
//  Created by Chris James on 04/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation

enum Isolation: Int {
    case type1 = 0
    case type2 = 1
    
    var description: String {
        get{
            switch self{
            case .type1: return "Type 1"
            case .type2: return "Type 2"
            }
        }
    }
    
}
