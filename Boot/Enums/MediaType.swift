//
//  MediaType.swift
//  Boot
//
//  Created by Chris James on 19/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation

enum MediaType: Int {
    case image = 0
    case audio = 1
    case video = 2
    
    var description: String {
        get {
            switch(self){
            case .image: return "Photo"
            case .audio: return "Audio"
            case .video: return "Video"
            }
        }
    }
    
    var sortValue: Int {
        get {
            switch(self){
            case .video: return 0
            case .image: return 1
            case .audio: return 2
            }
        }
    }
}
