//
//  NotificationType.swift
//  Boot
//
//  Created by Chris James on 05/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation

enum NotificationType: Int, CaseIterable {
    case holdUpCreated = 0
    case inspectionStarted = 1
    case inspectionFinished = 2
}
