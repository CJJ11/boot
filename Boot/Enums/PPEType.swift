//
//  PPE.swift
//  boot
//
//  Created by Chris James on 04/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

enum PPEType: Int {
    case respiratory = 0
    case eye = 1
    case hearing = 2
    case hand = 3
    case foot = 4
    case head = 5
    case heights = 6
    case skin = 7
    case other = 8
    
    var description: String {
        get{
            switch self{
            case .respiratory: return "Respiratory"
            case .eye: return "Eye"
            case .hearing: return "Hearing"
            case .hand: return "Hand"
            case .foot: return "Foot"
            case .head: return "Head"
            case .heights: return "Heights"
            case .skin: return "Skin"
            case .other: return "Other"
            }
        }
    }
    
    var image: UIImage {
        get{
            switch self{
            case .respiratory: return #imageLiteral(resourceName: "ppeRespiratory")
            case .eye: return #imageLiteral(resourceName: "ppeEyes")
            case .hearing: return #imageLiteral(resourceName: "ppeHearing")
            case .hand: return #imageLiteral(resourceName: "ppeGloves")
            case .foot: return #imageLiteral(resourceName: "ppeFoot")
            case .head: return #imageLiteral(resourceName: "ppeHead")
            case .heights: return #imageLiteral(resourceName: "ppeHeights")
            case .skin: return #imageLiteral(resourceName: "ppeSkin")
            case .other: return #imageLiteral(resourceName: "ppeOther")
            }
        }
    }
    
}
