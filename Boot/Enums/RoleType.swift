//
//  RoleType.swift
//  Boot
//
//  Created by Chris James on 13/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation

enum RoleType: Int {
    case technician = 0
    case supervisor = 1
    case admin = 2
    
    var description: String {
        switch(self){
        case .technician: return "Technician"
        case .supervisor: return "Supervisor"
        case .admin: return "Administrator"
        }
    }
}
