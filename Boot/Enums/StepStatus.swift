//
//  StepStatus.swift
//  Boot
//
//  Created by Chris James on 18/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

enum StepStatus: Int, CaseIterable {
    case notStarted = 0
    case underway = 1
    case complete = 2
    
    static let count = 3

    var image: UIImage {
        switch self {
        case .notStarted: return #imageLiteral(resourceName: "Cross")
        case .underway: return #imageLiteral(resourceName: "Clock")
        case .complete: return #imageLiteral(resourceName: "Tick")
        }
    }
    
}
