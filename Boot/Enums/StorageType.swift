//
//  StorageType.swift
//  Boot
//
//  Created by Chris James on 19/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation

enum StorageType {
    case profileImage
    case holdUpAttachment
}
