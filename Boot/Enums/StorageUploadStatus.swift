//
//  StorageUploadStatus.swift
//  Boot
//
//  Created by Chris James on 19/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation

enum StorageUploadStatus {
    case notStarted
    case uploading
    case paused
    case success
    case failure
    
    var description: String {
        get {
            switch(self){
            case .notStarted: return "Initiating.."
            case .uploading: return "Uploading..."
            case .paused: return "Upload paused"
            case .success: return "Uploaded successfully"
            case .failure: return "Error uploading file"
            }
        }
    }
    
}
