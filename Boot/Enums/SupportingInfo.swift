//
//  SupportingInfo.swift
//  Boot
//
//  Created by Chris James on 19/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

enum SupportingInfo: Int {
    case takePhoto = 0
    case recordVideo = 1
    case recordAudio = 2
    case libraryUpload = 3
    
    static let count = 4
    
    var description: String {
        get {
            switch self {
            case .takePhoto: return "Take photo"
            case .recordVideo: return "Record video"
            case .recordAudio: return "Record audio"
            case .libraryUpload: return "Choose from library"
            }
        }
    }
    
    var image: UIImage {
        get {
            switch self {
            case .takePhoto: return #imageLiteral(resourceName: "Photo")
            case .recordVideo: return #imageLiteral(resourceName: "Video")
            case .recordAudio: return #imageLiteral(resourceName: "Audio")
            case .libraryUpload: return #imageLiteral(resourceName: "Library")
            }
        }
    }
}
