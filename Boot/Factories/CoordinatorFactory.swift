//
//  CoordinatorFactory.swift
//  Boot
//
//  Created by Chris James on 08/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

protocol CoordinatorFactory {
    func makeLoginCoordinator(router: Router) -> Coordinator
    
    func makeSupervisorTabBarCoordinator(router: Router) -> Coordinator & SupervisorTabBarCoordinatorOutput
    func makeTechnicianTabBarCoordinator(router: Router) -> Coordinator & TechnicianTabBarCoordinatorOutput
    
    func makeSupervisorInspectionCoordinator(navController: UINavigationController) -> Coordinator
    func makeTechnicianInspectionCoordinator(navController: UINavigationController) -> Coordinator
    func makeAddInspectionCoordinator() -> Coordinator & AddInspectionCoordinatorOutput
    
    func makeHoldUpCoordinator(role: RoleType, navController: UINavigationController) -> Coordinator & HoldUpCoordinatorOutput
    func makeAddHoldUpCoordinator() -> Coordinator & AddHoldUpCoordinatorOutput
    
    func makeNotificationCoordinator(navController: UINavigationController) -> Coordinator & NotificationCoordinatorOutput
    func makeProfileCoordinator() -> Coordinator & ProfileCoordinatorOutput
}

final class CoordinatorFactoryImp: CoordinatorFactory {
    
    func makeLoginCoordinator(router: Router) -> Coordinator {
        return LoginCoordinator(auth: FirebaseAuthSource(),
                                router: router,
                                moduleFactory: ModuleFactoryImp(),
                                coordinatorFactory: CoordinatorFactoryImp()
        )
    }
    
    
    func makeSupervisorInspectionCoordinator(navController: UINavigationController) -> Coordinator {
        return SupervisorInspectionCoordinator(
            auth: FirebaseAuthSource(),
            router: RouterImp(navController: navController),
            moduleFactory: ModuleFactoryImp(),
            coordinatorFactory: CoordinatorFactoryImp(),
            dataSourceFactory: DataSourceFactoryImp()
        )
    }
    
    func makeTechnicianInspectionCoordinator(navController: UINavigationController) -> Coordinator {
        return TechnicianInspectionCoordinator(
            auth: FirebaseAuthSource(),
            router: RouterImp(navController: navController),
            moduleFactory: ModuleFactoryImp(),
            coordinatorFactory: CoordinatorFactoryImp(),
            dataSourceFactory: DataSourceFactoryImp()
        )
    }
    
    func makeAddInspectionCoordinator() -> Coordinator & AddInspectionCoordinatorOutput {
        let navController = UINavigationController()
        navController.navigationBar.prefersLargeTitles = true
        return AddInspectionCoordinator(
            auth: FirebaseAuthSource(),
            router: RouterImp(navController: navController),
            moduleFactory: ModuleFactoryImp(),
            coordinatorFactory: CoordinatorFactoryImp(),
            dataSourceFactory: DataSourceFactoryImp()
        )
    }
    
    func makeHoldUpCoordinator(role: RoleType, navController: UINavigationController) -> Coordinator & HoldUpCoordinatorOutput {
        return HoldUpCoordinator(
            auth: FirebaseAuthSource(),
            role: role,
            router: RouterImp(navController: navController),
            moduleFactory: ModuleFactoryImp(),
            coordinatorFactory: CoordinatorFactoryImp(),
            dataSourceFactory: DataSourceFactoryImp()
        )
    }
    
    func makeNotificationCoordinator(navController: UINavigationController) -> Coordinator & NotificationCoordinatorOutput {
        return NotificationCoordinator(
            auth: FirebaseAuthSource(),
            router: RouterImp(navController: navController),
            moduleFactory: ModuleFactoryImp(),
            coordinatorFactory: CoordinatorFactoryImp(),
            dataSourceFactory: DataSourceFactoryImp()
        )
    }
    
    func makeAddHoldUpCoordinator() -> Coordinator & AddHoldUpCoordinatorOutput {
        let navController = UINavigationController()
        navController.navigationBar.prefersLargeTitles = true
        return AddHoldUpCoordinator(
            auth: FirebaseAuthSource(),
            router: RouterImp(navController: navController),
            moduleFactory: ModuleFactoryImp(),
            coordinatorFactory: CoordinatorFactoryImp()
        )
    }
    
    func makeProfileCoordinator() -> Coordinator & ProfileCoordinatorOutput {
        let navController = UINavigationController()
        navController.navigationBar.prefersLargeTitles = true
        return ProfileCoordinator(
            auth: FirebaseAuthSource(),
            router: RouterImp(navController: navController),
            moduleFactory: ModuleFactoryImp(),
            coordinatorFactory: CoordinatorFactoryImp(),
            dataSourceFactory: DataSourceFactoryImp()
        )
    }
    
    func makeSupervisorTabBarCoordinator(router: Router) -> Coordinator & SupervisorTabBarCoordinatorOutput {
        return SupervisorTabBarCoordinator(
            auth: FirebaseAuthSource(),
            router: router,
            moduleFactory: ModuleFactoryImp(),
            coordinatorFactory: CoordinatorFactoryImp(),
            dataSourceFactory: DataSourceFactoryImp()
        )
    }
    
    func makeTechnicianTabBarCoordinator(router: Router) -> Coordinator & TechnicianTabBarCoordinatorOutput {
        return TechnicianTabBarCoordinator(
            auth: FirebaseAuthSource(),
            router: router,
            moduleFactory: ModuleFactoryImp(),
            coordinatorFactory: CoordinatorFactoryImp(),
            dataSourceFactory: DataSourceFactoryImp()
        )
    }
    
}

