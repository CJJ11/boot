//
//  DataSourceFactory.swift
//  Boot
//
//  Created by Chris James on 20/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import FirebaseFirestore

protocol DataSourceFactory {
    func makeSupervisorInspDataSource(authSource: AuthSource) -> SupervisorInspDataSource
    func makeInspectionDataSource(authSource: AuthSource) -> InspectionDataSource
    func makeTechInspectionDataSource(authSource: AuthSource) -> TechInspectionDataSource
    func makeAddInspectionDataSource(authSource: AuthSource) -> AddInspectionDataSource
    
    func makeCheckDataSource(authSource: AuthSource, inspectionUid: String, stepNumber: Int) -> CheckDataSource
    func makeStepDataSource(authSource: AuthSource, inspection: Inspection) -> StepDataSource
    func makeHoldUpDataSource(authSource: AuthSource) -> HoldUpDataSource
    
    func makeNotificationDataSource(authSource: AuthSource) -> NotificationDataSource?
    func makeTabBarDataSource(authSource: AuthSource) -> TabBarDataSource?
    func makeProfileDataSource(authSource: AuthSource) -> ProfileDataSource?
}

final class DataSourceFactoryImp: DataSourceFactory {
    
    func makeTechInspectionDataSource(authSource: AuthSource) -> TechInspectionDataSource {
        let reference = Firestore.firestore()
            .collection("minesites")
            .document("cadia")
            .collection("inspections")
        
        return TechInspectionFirebaseDataSource(reference: reference, authSource: authSource)
    }
    
    func makeSupervisorInspDataSource(authSource: AuthSource) -> SupervisorInspDataSource {
        return SupervisorInspFirebaseDataSource(authSource: authSource)
    }
    
    func makeInspectionDataSource(authSource: AuthSource) -> InspectionDataSource {
        return InspectionFirebaseDataSource(authSource: authSource)
    }
    
    func makeAddInspectionDataSource(authSource: AuthSource) -> AddInspectionDataSource {
        let templatesRef = Firestore.firestore()
                    .collection("minesites")
                    .document("cadia")
                    .collection("inspectionTemplates")
        
        let inspectionsRef = Firestore.firestore()
            .collection("minesites")
            .document("cadia")
            .collection("inspections")
        
        return AddInspectionFirebaseDataSource(
            currentInspectionsRef: inspectionsRef,
            inspectionTemplatesRef: templatesRef,
            authSource: authSource
        )
    }
    
    func makeCheckDataSource(authSource: AuthSource, inspectionUid: String, stepNumber: Int) -> CheckDataSource {
        return CheckFirebaseDataSource(authSource: authSource, inspectionUid: inspectionUid, stepNumber: stepNumber)
    }
    
    func makeStepDataSource(authSource: AuthSource, inspection: Inspection) -> StepDataSource {
        return StepFirebaseDataSource(authSource: authSource, inspection: inspection)
    }
    
    func makeHoldUpDataSource(authSource: AuthSource) -> HoldUpDataSource {
        let reference = Firestore.firestore()
            .collection("minesites")
            .document("cadia")
            .collection("holdUps")
        return HoldUpFirebaseDataSource(reference: reference, authSource: authSource)
    }
    
    func makeNotificationDataSource(authSource: AuthSource) -> NotificationDataSource? {
        return NotificationFirebaseDataSource(authSource: authSource)
    }
    
    func makeTabBarDataSource(authSource: AuthSource) -> TabBarDataSource? {
        return TabBarFirebaseDataSource(authSource: authSource)
    }
    
    func makeProfileDataSource(authSource: AuthSource) -> ProfileDataSource? {
        return ProfileFirebaseDataSource(authSource: authSource)
    }
    
}
