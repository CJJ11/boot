//
//  ModuleFactory.swift
//  Boot
//
//  Created by Chris James on 08/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

protocol ModuleFactory {
    func makeLoginView() -> LoginView
    func makeProfileView(dataSource: ProfileDataSource) -> ProfileView
    
    func makeCheckView(dataSource: CheckDataSource, title: String, stepNumber: Int) -> CheckView
    func makeStepView(dataSource: StepDataSource, inspection: Inspection, roleType: RoleType) -> StepView
    func makeTechInspectionView(dataSource: TechInspectionDataSource) -> TechInspectionView
    func makeInspectionView(dataSource: SupervisorInspDataSource, roleType: RoleType) -> InspectionView
    func makeInspectionDetailView(inspection: Inspection) -> InspectionDetailView
    func makeAddInspectionView(dataSource: AddInspectionDataSource) -> AddInspectionView
    func makeHoldUpView(role: RoleType, dataSource: HoldUpDataSource) -> HoldUpView
    func makeAddHoldUpView(uid: String, username: String, profileUrl: URL) -> AddHoldUpView
    func makeNotificationView(dataSource: NotificationDataSource) -> NotificationView
    
    func makeSupervisorTabBarView(dataSource: TabBarDataSource) -> (SupervisorTabBarView, UINavigationController)
    func makeTechnicianTabBarView(dataSource: TabBarDataSource) -> (TechnicianTabBarView, UINavigationController)
}


final class ModuleFactoryImp: ModuleFactory {
    
    func makeLoginView() -> LoginView {
        return LoginVC(nibName: nil, bundle: nil)
    }
    
    func makeProfileView(dataSource: ProfileDataSource) -> ProfileView {
        return ProfileVC(dataSource: dataSource)
    }
    
    func makeCheckView(dataSource: CheckDataSource, title: String, stepNumber: Int) -> CheckView {
        return CheckVC(title: title, step: stepNumber, dataSource: dataSource)
    }
    
    func makeStepView(dataSource: StepDataSource, inspection: Inspection, roleType: RoleType) -> StepView {
        return StepVC(inspection: inspection, dataSource: dataSource, roleType: roleType)
    }
    
    func makeTechInspectionView(dataSource: TechInspectionDataSource) -> TechInspectionView {
        return TechInspectionVC(dataSource: dataSource)
    }
    
    func makeInspectionView(dataSource: SupervisorInspDataSource, roleType: RoleType) -> InspectionView {
        return InspectionVC(dataSource: dataSource)
    }
    
    func makeInspectionDetailView(inspection: Inspection) -> InspectionDetailView {
        let viewModel = InspectionDetailViewModel(inspection: inspection)
        return InspectionDetailVC(viewModel: viewModel)
    }
    
    func makeAddInspectionView(dataSource: AddInspectionDataSource) -> AddInspectionView {
        return AddInspectionVC(dataSource: dataSource)
    }
    
    func makeHoldUpView(role: RoleType, dataSource: HoldUpDataSource) -> HoldUpView {
        return HoldUpVC(role: role, dataSource: dataSource)
    }
    
    func makeNotificationView(dataSource: NotificationDataSource) -> NotificationView {
        return NotificationVC(dataSource: dataSource)
    }
    
    func makeAddHoldUpView(uid: String, username: String, profileUrl: URL) -> AddHoldUpView {
        let mediaPickerFactory = MediaPickerFactoryImp()
        let uploaderFactory = FirebaseStorageUploaderFactory()
        return AddHoldUpVC(
            uid: uid,
            username: username,
            profileUrl: profileUrl,
            mediaPickerFactory: mediaPickerFactory,
            uploaderFactory: uploaderFactory
        )
    }
    
    func makeSupervisorTabBarView(dataSource: TabBarDataSource) -> (SupervisorTabBarView, UINavigationController) {
        let inspectionNavController = UINavigationController()
        inspectionNavController.tabBarItem = UITabBarItem(title: "Inspections", image: UIImage(named: "Inspection"), tag: 0)
        inspectionNavController.tabBarItem.landscapeImagePhone = UIImage(named: "InspectionCompact")
        inspectionNavController.navigationBar.prefersLargeTitles = true
        
        let holdUpController = UINavigationController()
        holdUpController.tabBarItem = UITabBarItem(title: "Hold Ups", image: UIImage(named: "HoldUp"), tag: 1)
        holdUpController.tabBarItem.landscapeImagePhone = UIImage(named: "HoldUpCompact")
        holdUpController.navigationBar.prefersLargeTitles = true
        
        let notificationNavController = UINavigationController()
        notificationNavController.tabBarItem = UITabBarItem(title: "Notifications", image: UIImage(named: "Notification"), tag: 2)
        notificationNavController.tabBarItem.landscapeImagePhone = UIImage(named: "NotificationCompact")
        notificationNavController.navigationBar.prefersLargeTitles = true
        
        let viewControllers = [inspectionNavController, holdUpController, notificationNavController]
        let tabBarController = SupervisorTabBarController.init(viewControllers: viewControllers, dataSource: dataSource)
        
        return (tabBarController, inspectionNavController)
    }
    
    func makeTechnicianTabBarView(dataSource: TabBarDataSource) -> (TechnicianTabBarView, UINavigationController) {
        let inspectionNavController = UINavigationController()
        inspectionNavController.tabBarItem = UITabBarItem(title: "Inspections", image: UIImage(named: "Inspection"), tag: 0)
        inspectionNavController.tabBarItem.landscapeImagePhone = UIImage(named: "InspectionCompact")
        inspectionNavController.navigationBar.prefersLargeTitles = true
        
        let holdUpController = UINavigationController()
        holdUpController.tabBarItem = UITabBarItem(title: "Hold Ups", image: UIImage(named: "HoldUp"), tag: 1)
        holdUpController.tabBarItem.landscapeImagePhone = UIImage(named: "HoldUpCompact")
        holdUpController.navigationBar.prefersLargeTitles = true
        
//        let notificationNavController = UINavigationController()
//        notificationNavController.tabBarItem = UITabBarItem(title: "Notifications", image: UIImage(named: "Notification"), tag: 2)
//        notificationNavController.tabBarItem.landscapeImagePhone = UIImage(named: "NotificationCompact")
//        notificationNavController.navigationBar.prefersLargeTitles = true
        
        let viewControllers = [inspectionNavController, holdUpController]//, notificationNavController]
        let tabBarController = TechnicianTabBarController.init(viewControllers: viewControllers, dataSource: dataSource)
        
        return (tabBarController, inspectionNavController)
    }
    
}
