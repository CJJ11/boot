//
//  DeepLinkOption.swift
//  Boot
//
//  Created by Chris James on 08/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation

struct DeepLinkURLConstants {
    static let HoldUp = "holdUp"
    static let Inspection = "inspection"
}

enum DeepLinkOption {
    
    case holdUp(String?)
    case inspection(String?)
    
    static func build(with dict: [String : AnyObject]?) -> DeepLinkOption? {
        guard let id = dict?["launch_id"] as? String else { return nil }
        
        let inspectionID = dict?["inspection_id"] as? String
        let holdUpID = dict?["hold_up_id"] as? String
        
        switch id {
        case DeepLinkURLConstants.HoldUp: return .holdUp(holdUpID)
        case DeepLinkURLConstants.Inspection: return .inspection(inspectionID)
        default: return nil
        }
    }
}
