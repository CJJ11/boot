//
//  Hazard.swift
//  Boot
//
//  Created by Chris James on 29/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation

struct Hazard: Equatable {
    let uid: String
    let type: HazardType
    let description: String
    
    func toDictionary() -> [String: Any] {
        return [Hazard.typeKey: type.rawValue as Any,
                Hazard.descriptionKey: description as Any
        ]
    }
    
    //DB keys
    private static let typeKey = "type"
    private static let descriptionKey = "desc"
}

extension Hazard {
    
    init?(uid: String, dictionary: [String: Any]){
        guard let hazardTypeRawValue = dictionary[Hazard.typeKey] as? Int,
            let hazardType = HazardType(rawValue: hazardTypeRawValue),
            let description = dictionary[Hazard.descriptionKey] as? String
            else {return nil}
        
        self.init(uid: uid,
                  type: hazardType,
                  description: description)
    }
    
}
