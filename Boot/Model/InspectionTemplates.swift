//
//  InspectionTemplates.swift
//  Boot
//
//  Created by Chris James on 28/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import FirebaseFirestore.FIRTimestamp

//Locations

struct InspectionTemplates {

    let crusher1Inspection = Inspection(
        equipment: "Crusher 1",
        location: Loc.crushing.location,
        locationIconUrl: Loc.crushing.iconUrl,
        scope: "Pre-start inspection of crusher",
        timeRequired: "2 hours",
        tools: "Personal gas monitor",
        isolations: "Lvl 2 Isolation",
        competencies: "Confined Space, Underground Famil",
        permits: "Confined Space Permit, Permit to Work",
        hazards: [
            Hazard(uid: "0", type: HazardType.confinedSpaces, description: "Confined Space")],
        ppeList: [
            PPE(uid: "0", type: PPEType.hearing, description: "Ear muffs"),
            PPE(uid: "1", type: PPEType.respiratory, description: "Respirator"),
            PPE(uid: "2", type: PPEType.eye, description: "Safety goggles")],
        
        steps: [
            Step(
                number: 0,
                description: "Inspect gearbox",
                checks: [
                    Check(
                        number: 0,
                        description: "Gearbox is free of oil leaks"),
                    Check(
                        number: 1,
                        description: "All bolts and nuts secured"),
                    Check(
                        number: 2,
                        description: "Record gearbox oil level",
                        sliderMin: 20,
                        sliderMax: 100,
                        sliderUnits: "%",
                        sliderDecimals: 0)
                ]
                )!,
            Step(
                number: 1,
                description: "Inspection crusher structure",
                checks: [
                    Check(
                        number: 0,
                        description: "All handrails and walkways are in good condition"),
                    Check(
                        number: 1,
                        description: "Screen grids are in order"),
                    Check(number: 2, description: "Record condition of grizzly",
                          pickerOptions: [
                            PickerOption(number: 0, description: "As new condition"),
                            PickerOption(number: 1, description: "Minor wear"),
                            PickerOption(number: 2, description: "Moderate wear"),
                            PickerOption(number: 3, description: "Significant wear"),
                        ])
                ]
                )!,
            Step(
                number: 2,
                description: "Inspect electrical system",
                checks: [
                    Check(
                        number: 0,
                        description: "All power and control cables are free of damage"),
                    Check(
                        number: 1,
                        description: "Cable entries to local control station are secure"),
                    Check(
                        number: 2,
                        description: "All cable tray covers are in place"),
                    Check(
                        number: 3,
                        description: "Mechanical limit switches actuate freely")
                ]
                )!
            
            ]
    )
    
    let crusher2Inspection = Inspection(
        equipment: "Crusher 2",
        location: Loc.crushing.location,
        locationIconUrl: Loc.crushing.iconUrl,
        scope: "Pre-start inspection of crusher",
        timeRequired: "2 hours",
        tools: "Personal gas monitor",
        isolations: "Lvl 2 Isolation",
        competencies: "Confined Space, Underground Famil",
        permits: "Confined Space Permit, Permit to Work",
        hazards: [
            Hazard(uid: "0", type: HazardType.confinedSpaces, description: "Confined Space")],
        ppeList: [
            PPE(uid: "0", type: PPEType.hearing, description: "Ear muffs"),
            PPE(uid: "1", type: PPEType.respiratory, description: "Respirator"),
            PPE(uid: "2", type: PPEType.eye, description: "Safety goggles")],
        
        steps: [
            Step(
                number: 0,
                description: "Inspect gearbox",
                checks: [
                    Check(
                        number: 0,
                        description: "Gearbox is free of oil leaks"),
                    Check(
                        number: 1,
                        description: "All bolts and nuts secured"),
                    Check(
                        number: 2,
                        description: "Record gearbox oil level",
                        sliderMin: 20,
                        sliderMax: 100,
                        sliderUnits: "%",
                        sliderDecimals: 0)
                ]
                )!,
            Step(
                number: 1,
                description: "Inspection crusher structure",
                checks: [
                    Check(
                        number: 0,
                        description: "All handrails and walkways are in good condition"),
                    Check(
                        number: 1,
                        description: "Screen grids are in order"),
                    Check(number: 2, description: "Record condition of grizzly",
                          pickerOptions: [
                            PickerOption(number: 0, description: "As new condition"),
                            PickerOption(number: 1, description: "Minor wear"),
                            PickerOption(number: 2, description: "Moderate wear"),
                            PickerOption(number: 3, description: "Significant wear"),
                        ])
                ]
                )!,
            Step(
                number: 2,
                description: "Inspect electrical system",
                checks: [
                    Check(
                        number: 0,
                        description: "All power and control cables are free of damage"),
                    Check(
                        number: 1,
                        description: "Cable entries to local control station are secure"),
                    Check(
                        number: 2,
                        description: "All cable tray covers are in place"),
                    Check(
                        number: 3,
                        description: "Mechanical limit switches actuate freely")
                ]
                )!
            
        ]
    )
    
    let crusher3Inspection = Inspection(
        equipment: "Crusher 3",
        location: Loc.crushing.location,
        locationIconUrl: Loc.crushing.iconUrl,
        scope: "Pre-start inspection of crusher",
        timeRequired: "2 hours",
        tools: "Personal gas monitor",
        isolations: "Lvl 2 Isolation",
        competencies: "Confined Space, Underground Famil",
        permits: "Confined Space Permit, Permit to Work",
        hazards: [
            Hazard(uid: "0", type: HazardType.confinedSpaces, description: "Confined Space")],
        ppeList: [
            PPE(uid: "0", type: PPEType.hearing, description: "Ear muffs"),
            PPE(uid: "1", type: PPEType.respiratory, description: "Respirator"),
            PPE(uid: "2", type: PPEType.eye, description: "Safety goggles")],
        
        steps: [
            Step(
                number: 0,
                description: "Inspect gearbox",
                checks: [
                    Check(
                        number: 0,
                        description: "Gearbox is free of oil leaks"),
                    Check(
                        number: 1,
                        description: "All bolts and nuts secured"),
                    Check(
                        number: 2,
                        description: "Record gearbox oil level",
                        sliderMin: 20,
                        sliderMax: 100,
                        sliderUnits: "%",
                        sliderDecimals: 0)
                ]
                )!,
            Step(
                number: 1,
                description: "Inspection crusher structure",
                checks: [
                    Check(
                        number: 0,
                        description: "All handrails and walkways are in good condition"),
                    Check(
                        number: 1,
                        description: "Screen grids are in order"),
                    Check(number: 2, description: "Record condition of grizzly",
                          pickerOptions: [
                            PickerOption(number: 0, description: "As new condition"),
                            PickerOption(number: 1, description: "Minor wear"),
                            PickerOption(number: 2, description: "Moderate wear"),
                            PickerOption(number: 3, description: "Significant wear"),
                        ])
                ]
                )!,
            Step(
                number: 2,
                description: "Inspect electrical system",
                checks: [
                    Check(
                        number: 0,
                        description: "All power and control cables are free of damage"),
                    Check(
                        number: 1,
                        description: "Cable entries to local control station are secure"),
                    Check(
                        number: 2,
                        description: "All cable tray covers are in place"),
                    Check(
                        number: 3,
                        description: "Mechanical limit switches actuate freely")
                ]
                )!
            
        ]
    )
    
    let mill1Inspection = Inspection(
        equipment: "SAG Mill 1",
        location: Loc.milling.location,
        locationIconUrl: Loc.milling.iconUrl,
        scope: "Pre-start inspection of SAG mill",
        timeRequired: "4 hours",
        tools: "Borescope",
        isolations: "Lvl 1 Isolation",
        competencies: "Working at Heights",
        permits: "Working at Heights, Permit to Work",
        hazards: [
            Hazard(uid: "0", type: HazardType.fallsSlipsAndTrips, description: "Working at heights"),
            Hazard(uid: "1", type: HazardType.manualHandling, description: "Manual handling")],
        ppeList: [
            PPE(uid: "0", type: PPEType.heights, description: "Safety harness"),
            PPE(uid: "1", type: PPEType.head, description: "Safety helmet"),
            PPE(uid: "2", type: PPEType.foot, description: "Safety boots")],
        
        steps: [
            Step(
                number: 0,
                description: "Inspect drive motor area",
                checks: [
                    Check(
                        number: 0,
                        description: "Check brake pads, accumulators, filters"),
                    Check(
                        number: 1,
                        description: "Check coupling alignment"),
                    Check(
                        number: 2,
                        description: "Check hydraulic unit"),
                    Check(
                        number: 3,
                        description: "Check lube packing at pinion"),
                    Check(
                        number: 4,
                        description: "Check torque of motor hold-down bolts"),
                    
                ]
                )!,
            Step(
                number: 1,
                description: "Inspect trommel",
                checks: [
                    Check(
                        number: 0,
                        description: "Check bolt torques"),
                    Check(
                        number: 1,
                        description: "Check drip ring"),
                    Check(
                        number: 2,
                        description: "Record bolt torque",
                        sliderMin: 4500,
                        sliderMax: 5200,
                        sliderUnits: "Nm",
                        sliderDecimals: 0)
                ]
                )!,
            Step(
                number: 2,
                description: "Inspect main bearings",
                checks: [
                    Check(
                        number: 0,
                        description: "Check bearing clearances"),
                    Check(
                        number: 1,
                        description: "Check bearing seals"),
                    
                    Check(number: 2,
                          description: "Check oil condition",
                          pickerOptions: [
                            PickerOption(number: 0, description: "Good condition"),
                            PickerOption(number: 1, description: "Minor signs of contaminants"),
                            PickerOption(number: 2, description: "Minor signs of wear debris"),
                            PickerOption(number: 3, description: "Minor signs of moisture"),
                            PickerOption(number: 4, description: "Oil needs replacement")
                        ])
                ]
                )!]
    )
    
    let mill2Inspection = Inspection(
        equipment: "SAG Mill 2",
        location: Loc.milling.location,
        locationIconUrl: Loc.milling.iconUrl,
        scope: "Pre-start inspection of SAG mill",
        timeRequired: "4 hours",
        tools: "Borescope",
        isolations: "Lvl 1 Isolation",
        competencies: "Working at Heights",
        permits: "Working at Heights, Permit to Work",
        hazards: [
            Hazard(uid: "0", type: HazardType.fallsSlipsAndTrips, description: "Working at heights"),
            Hazard(uid: "1", type: HazardType.manualHandling, description: "Manual handling")],
        ppeList: [
            PPE(uid: "0", type: PPEType.heights, description: "Safety harness"),
            PPE(uid: "1", type: PPEType.head, description: "Safety helmet"),
            PPE(uid: "2", type: PPEType.foot, description: "Safety boots")],
        
        steps: [
            Step(
                number: 0,
                description: "Inspect drive motor area",
                checks: [
                    Check(
                        number: 0,
                        description: "Check brake pads, accumulators, filters"),
                    Check(
                        number: 1,
                        description: "Check coupling alignment"),
                    Check(
                        number: 2,
                        description: "Check hydraulic unit"),
                    Check(
                        number: 3,
                        description: "Check lube packing at pinion"),
                    Check(
                        number: 4,
                        description: "Check torque of motor hold-down bolts"),
                    
                    ]
                )!,
            Step(
                number: 1,
                description: "Inspect trommel",
                checks: [
                    Check(
                        number: 0,
                        description: "Check bolt torques"),
                    Check(
                        number: 1,
                        description: "Check drip ring"),
                    Check(
                        number: 2,
                        description: "Record bolt torque",
                        sliderMin: 4500,
                        sliderMax: 5200,
                        sliderUnits: "Nm",
                        sliderDecimals: 0)
                ]
                )!,
            Step(
                number: 2,
                description: "Inspect main bearings",
                checks: [
                    Check(
                        number: 0,
                        description: "Check bearing clearances"),
                    Check(
                        number: 1,
                        description: "Check bearing seals"),
                    
                    Check(number: 2,
                          description: "Check oil condition",
                          pickerOptions: [
                            PickerOption(number: 0, description: "Good condition"),
                            PickerOption(number: 1, description: "Minor signs of contaminants"),
                            PickerOption(number: 2, description: "Minor signs of wear debris"),
                            PickerOption(number: 3, description: "Minor signs of moisture"),
                            PickerOption(number: 4, description: "Oil needs replacement")
                        ])
                ]
                )!]
    )
    
    
    let float1Inspection = Inspection(
        equipment: "Float Circuit 1",
        location: Loc.flotation.location,
        locationIconUrl: Loc.flotation.iconUrl,
        scope: "Pre-start inspection of Float Circuit 1",
        timeRequired: "3.5 hours",
        tools: "Borescope",
        isolations: "Lvl 2 Isolation",
        competencies: "Authorised Isolator",
        permits: "Permit to Work",
        hazards: [
            Hazard(uid: "0", type: HazardType.electrical, description: "Electricity"),
            Hazard(uid: "1", type: HazardType.mechanical, description: "Pinch points")],
        ppeList: [
            PPE(uid: "0", type: PPEType.skin, description: "Barrier cream"),
            PPE(uid: "1", type: PPEType.head, description: "Safety helmet"),
            PPE(uid: "2", type: PPEType.foot, description: "Safety boots"),
            PPE(uid: "3", type: PPEType.eye, description: "Safety glasses")],
        
        steps: [
            Step(
                number: 0,
                description: "Check sump levels",
                checks: [
                    Check(
                        number: 0,
                        description: "Check sump 1 level",
                        sliderMin: 0,
                        sliderMax: 100,
                        sliderUnits: "%",
                        sliderDecimals: 0),
                    Check(
                        number: 1,
                        description: "Check sump 2 level",
                        sliderMin: 0,
                        sliderMax: 100,
                        sliderUnits: "%",
                        sliderDecimals: 0),
                    Check(
                        number: 2,
                        description: "Check sump 3 level",
                        sliderMin: 0,
                        sliderMax: 100,
                        sliderUnits: "%",
                        sliderDecimals: 0)
                ]
                )!,
            Step(
                number: 1,
                description: "Inspect screens",
                checks: [
                    Check(number: 0,
                          description: "Check Screen 1 deck condition",
                          pickerOptions: [
                            PickerOption(number: 0, description: "Excellent"),
                            PickerOption(number: 1, description: "Good"),
                            PickerOption(number: 2, description: "Fair"),
                            PickerOption(number: 3, description: "Poor"),
                            PickerOption(number: 4, description: "Very poor")
                        ]),
                    Check(number: 1,
                          description: "Check Screen 2 deck condition",
                          pickerOptions: [
                            PickerOption(number: 0, description: "Excellent"),
                            PickerOption(number: 1, description: "Good"),
                            PickerOption(number: 2, description: "Fair"),
                            PickerOption(number: 3, description: "Poor"),
                            PickerOption(number: 4, description: "Very poor")
                        ]),
                    Check(number: 2,
                          description: "Check Screen 3 deck condition",
                          pickerOptions: [
                            PickerOption(number: 0, description: "Excellent"),
                            PickerOption(number: 1, description: "Good"),
                            PickerOption(number: 2, description: "Fair"),
                            PickerOption(number: 3, description: "Poor"),
                            PickerOption(number: 4, description: "Very poor")
                        ])
                ]
                )!,
            Step(
                number: 2,
                description: "Inspect pipework",
                checks: [
                    Check(number: 0, description: "Valve 1 is in the open position"),
                    Check(number: 1, description: "Pipework is free of leaks"),
                    Check(number: 2, description: "Pipe supports are in good condition"),
                    Check(number: 3, description: "All flange bolts are in place")
                ]
                )!]
    )
    
    let float2Inspection = Inspection(
        equipment: "Float Circuit 2",
        location: Loc.flotation.location,
        locationIconUrl: Loc.flotation.iconUrl,
        scope: "Pre-start inspection of Float Circuit 1",
        timeRequired: "3.5 hours",
        tools: "Borescope",
        isolations: "Lvl 2 Isolation",
        competencies: "Authorised Isolator",
        permits: "Permit to Work",
        hazards: [
            Hazard(uid: "0", type: HazardType.electrical, description: "Electricity"),
            Hazard(uid: "1", type: HazardType.mechanical, description: "Pinch points")],
        ppeList: [
            PPE(uid: "0", type: PPEType.skin, description: "Barrier cream"),
            PPE(uid: "1", type: PPEType.head, description: "Safety helmet"),
            PPE(uid: "2", type: PPEType.foot, description: "Safety boots"),
            PPE(uid: "3", type: PPEType.eye, description: "Safety glasses")],
        
        steps: [
            Step(
                number: 0,
                description: "Check sump levels",
                checks: [
                    Check(
                        number: 0,
                        description: "Check sump 1 level",
                        sliderMin: 0,
                        sliderMax: 100,
                        sliderUnits: "%",
                        sliderDecimals: 0),
                    Check(
                        number: 1,
                        description: "Check sump 2 level",
                        sliderMin: 0,
                        sliderMax: 100,
                        sliderUnits: "%",
                        sliderDecimals: 0),
                    Check(
                        number: 2,
                        description: "Check sump 3 level",
                        sliderMin: 0,
                        sliderMax: 100,
                        sliderUnits: "%",
                        sliderDecimals: 0)
                ]
                )!,
            Step(
                number: 1,
                description: "Inspect screens",
                checks: [
                    Check(number: 0,
                          description: "Check Screen 1 deck condition",
                          pickerOptions: [
                            PickerOption(number: 0, description: "Excellent"),
                            PickerOption(number: 1, description: "Good"),
                            PickerOption(number: 2, description: "Fair"),
                            PickerOption(number: 3, description: "Poor"),
                            PickerOption(number: 4, description: "Very poor")
                        ]),
                    Check(number: 1,
                          description: "Check Screen 2 deck condition",
                          pickerOptions: [
                            PickerOption(number: 0, description: "Excellent"),
                            PickerOption(number: 1, description: "Good"),
                            PickerOption(number: 2, description: "Fair"),
                            PickerOption(number: 3, description: "Poor"),
                            PickerOption(number: 4, description: "Very poor")
                        ]),
                    Check(number: 2,
                          description: "Check Screen 3 deck condition",
                          pickerOptions: [
                            PickerOption(number: 0, description: "Excellent"),
                            PickerOption(number: 1, description: "Good"),
                            PickerOption(number: 2, description: "Fair"),
                            PickerOption(number: 3, description: "Poor"),
                            PickerOption(number: 4, description: "Very poor")
                        ])
                ]
                )!,
            Step(
                number: 2,
                description: "Inspect pipework",
                checks: [
                    Check(number: 0, description: "Valve 1 in the open position"),
                    Check(number: 1, description: "Pipework is free of leaks"),
                    Check(number: 2, description: "Pipe supports are in good condition"),
                    Check(number: 3, description: "All flange bolts are in place")
                ]
                )!]
    )
    
    let conveyor1Inspection = Inspection(
        equipment: "Conveyor 1",
        location: "Conveying",
        locationIconUrl: Loc.conveying.iconUrl,
        scope: "Pre-start inspection of conveyor",
        timeRequired: "3 hours",
        tools: "Basic hand tools",
        isolations: "Personal Isolation",
        competencies: "Underground Famil, MEM30205",
        permits: "Permit to Work",
        hazards: [
            Hazard(uid: "0", type: HazardType.mechanical, description: "Pinch points"),
            Hazard(uid: "1", type: HazardType.fallingObjects, description: "Hang up")],
        ppeList: [
            PPE(uid: "0", type: PPEType.head, description: "Safety helmet"),
            PPE(uid: "1", type: PPEType.hand, description: "Safety gloves"),
            PPE(uid: "2", type: PPEType.respiratory, description: "Respirator")],
        
        steps: [
            Step(
                number: 0,
                description: "Head-end belt checks",
                checks: [
                    Check(
                        number: 0,
                        description: "Belt is properly aligned"),
                    Check(
                        number: 1,
                        description: "Clearances at chute & skirting areas are adequate"),
                    Check(
                        number: 2,
                        description: "Belt wear is acceptable, including edges & splices")
                ]
                )!,
            
            Step(
                number: 1,
                description: "Tail-end belt checks",
                checks: [
                    Check(
                        number: 0,
                        description: "Scrapers are in good condition & adjusted correctly"),
                    Check(
                        number: 1,
                        description: "Check take-up tension",
                        sliderMin: 3800,
                        sliderMax: 4200,
                        sliderUnits: "N",
                        sliderDecimals: 0)
                ]
                )!,
            
            Step(
                number: 2,
                description: "Safety checks",
                checks: [
                    Check(
                        number: 0,
                        description: "Pull-wires & e-stops are accessible & in good condition"),
                    Check(
                        number: 1,
                        description: "All guarding and covers have been replaced"),
                    Check(
                        number: 2,
                        description: "All fire fighting equipment is in date")
                ]
                )!,
            
            Step(
                number: 3,
                description: "Housekeeping checks",
                checks: [
                    Check(
                        number: 0,
                        description: "Build-up under conveyor has been cleared"),
                    Check(
                        number: 1,
                        description: "All hoses have been coiled"),
                    Check(
                        number: 2,
                        description: "General tidiness of the conveyor area",
                        pickerOptions: [
                            PickerOption(number: 0, description: "Poor"),
                            PickerOption(number: 1, description: "Fair"),
                            PickerOption(number: 2, description: "Good"),
                            PickerOption(number: 3, description: "Excellent")]
                    )]
                )!]
    )
    
    
    let conveyor2Inspection = Inspection(
        equipment: "Conveyor 2",
        location: "Conveying",
        locationIconUrl: Loc.conveying.iconUrl,
        scope: "Pre-start inspection of conveyor",
        timeRequired: "3 hours",
        tools: "Basic hand tools",
        isolations: "Personal Isolation",
        competencies: "Underground Famil, MEM30205",
        permits: "Permit to Work",
        hazards: [
            Hazard(uid: "0", type: HazardType.mechanical, description: "Pinch points"),
            Hazard(uid: "1", type: HazardType.fallingObjects, description: "Hang up")],
        ppeList: [
            PPE(uid: "0", type: PPEType.head, description: "Safety helmet"),
            PPE(uid: "1", type: PPEType.hand, description: "Safety gloves"),
            PPE(uid: "2", type: PPEType.respiratory, description: "Respirator")],
        
        steps: [
            Step(
                number: 0,
                description: "Head-end belt checks",
                checks: [
                    Check(
                        number: 0,
                        description: "Belt is properly aligned"),
                    Check(
                        number: 1,
                        description: "Clearances at chute & skirting areas are adequate"),
                    Check(
                        number: 2,
                        description: "Belt wear is acceptable, including edges & splices")
                ]
                )!,
            
            Step(
                number: 1,
                description: "Tail-end belt checks",
                checks: [
                    Check(
                        number: 0,
                        description: "Scrapers are in good condition & adjusted correctly"),
                    Check(
                        number: 1,
                        description: "Check take-up tension",
                        sliderMin: 3800,
                        sliderMax: 4200,
                        sliderUnits: "N",
                        sliderDecimals: 0)
                ]
                )!,
            
            Step(
                number: 2,
                description: "Safety checks",
                checks: [
                    Check(
                        number: 0,
                        description: "Pull-wires & e-stops are accessible & in good condition"),
                    Check(
                        number: 1,
                        description: "All guarding and covers have been replaced"),
                    Check(
                        number: 2,
                        description: "All fire fighting equipment is in date")
                ]
                )!,
            
            Step(
                number: 3,
                description: "Housekeeping checks",
                checks: [
                    Check(
                        number: 0,
                        description: "Build-up under conveyor has been cleared"),
                    Check(
                        number: 1,
                        description: "All hoses have been coiled"),
                    Check(
                        number: 2,
                        description: "General tidiness of the conveyor area",
                        pickerOptions: [
                            PickerOption(number: 0, description: "Poor"),
                            PickerOption(number: 1, description: "Fair"),
                            PickerOption(number: 2, description: "Good"),
                            PickerOption(number: 3, description: "Excellent")]
                    )]
                )!]
    )
    
    
    let conveyor3Inspection = Inspection(
        equipment: "Conveyor 3",
        location: "Conveying",
        locationIconUrl: Loc.conveying.iconUrl,
        scope: "Pre-start inspection of conveyor",
        timeRequired: "3 hours",
        tools: "Basic hand tools",
        isolations: "Personal Isolation",
        competencies: "Underground Famil, MEM30205",
        permits: "Permit to Work",
        hazards: [
            Hazard(uid: "0", type: HazardType.mechanical, description: "Pinch points"),
            Hazard(uid: "1", type: HazardType.fallingObjects, description: "Hang up")],
        ppeList: [
            PPE(uid: "0", type: PPEType.head, description: "Safety helmet"),
            PPE(uid: "1", type: PPEType.hand, description: "Safety gloves"),
            PPE(uid: "2", type: PPEType.respiratory, description: "Respirator")],
        
        steps: [
            Step(
                number: 0,
                description: "Head-end belt checks",
                checks: [
                    Check(
                        number: 0,
                        description: "Belt is properly aligned"),
                    Check(
                        number: 1,
                        description: "Clearances at chute & skirting areas are adequate"),
                    Check(
                        number: 2,
                        description: "Belt wear is acceptable, including edges & splices")
                ]
                )!,
            
            Step(
                number: 1,
                description: "Tail-end belt checks",
                checks: [
                    Check(
                        number: 0,
                        description: "Scrapers are in good condition & adjusted correctly"),
                    Check(
                        number: 1,
                        description: "Check take-up tension",
                        sliderMin: 3800,
                        sliderMax: 4200,
                        sliderUnits: "N",
                        sliderDecimals: 0)
                ]
                )!,
            
            Step(
                number: 2,
                description: "Safety checks",
                checks: [
                    Check(
                        number: 0,
                        description: "Pull-wires & e-stops are accessible & in good condition"),
                    Check(
                        number: 1,
                        description: "All guarding and covers have been replaced"),
                    Check(
                        number: 2,
                        description: "All fire fighting equipment is in date")
                ]
                )!,
            
            Step(
                number: 3,
                description: "Housekeeping checks",
                checks: [
                    Check(
                        number: 0,
                        description: "Build-up under conveyor has been cleared"),
                    Check(
                        number: 1,
                        description: "All hoses have been coiled"),
                    Check(
                        number: 2,
                        description: "General tidiness of the conveyor area",
                        pickerOptions: [
                            PickerOption(number: 0, description: "Poor"),
                            PickerOption(number: 1, description: "Fair"),
                            PickerOption(number: 2, description: "Good"),
                            PickerOption(number: 3, description: "Excellent")]
                    )]
                )!]
    )
    
    let inspections: [Inspection]
    
    init(){
        inspections = [crusher1Inspection, crusher2Inspection, crusher3Inspection, mill1Inspection, mill2Inspection, float1Inspection, float2Inspection,
        conveyor1Inspection, conveyor2Inspection, conveyor3Inspection]
    }

    
    enum Loc: String {
        case crushing = "Crushing"
        case flotation = "Flotation"
        case milling = "Milling"
        case conveying = "Conveying"
        
        var location: String {
            get {
                switch(self){
                case .crushing: return "Crushing"
                case .flotation: return "Flotation"
                case .milling: return "Milling"
                case .conveying: return "Conveying"
                }
            }
        }
        
        var iconUrl: URL {
            get {
                switch(self){
                case .crushing: return URL(string: "https://firebasestorage.googleapis.com/v0/b/boot-dab5b.appspot.com/o/locationIcon-images%2FCrushing.png?alt=media&token=99af5836-0e4c-4759-b018-34df2b069c8d")!
                    
                case .flotation: return URL(string: "https://firebasestorage.googleapis.com/v0/b/boot-dab5b.appspot.com/o/locationIcon-images%2FFlotation.png?alt=media&token=4af6d3ba-7501-4874-aad5-ef327677ee3c")!
                    
                case .milling: return URL(string: "https://firebasestorage.googleapis.com/v0/b/boot-dab5b.appspot.com/o/locationIcon-images%2FGrinding.png?alt=media&token=c93eb401-327b-444d-8df4-3074316c3fa7")!
                    
                case .conveying: return URL(string: "https://firebasestorage.googleapis.com/v0/b/boot-dab5b.appspot.com/o/locationIcon-images%2FConveying.png?alt=media&token=fd322243-4246-46ed-97b7-ce7a98d5d7b3")!
                }
            }
        }
    }
    
    
}
