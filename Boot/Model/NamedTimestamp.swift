//
//  NamedTimestamp.swift
//  Boot
//
//  Created by Chris James on 23/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import FirebaseFirestore.FIRTimestamp

struct NamedTimestamp {
    
    let username: String
    let uid: String
    let timestamp: Timestamp
    
}
