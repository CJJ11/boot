//
//  PPE.swift
//  Boot
//
//  Created by Chris James on 29/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation

struct PPE: Equatable {
    let uid: String
    let type: PPEType
    let description: String
    
    func toDictionary() -> [String: Any] {
        return [PPE.typeKey: type.rawValue as Any,
                PPE.descriptionKey: description as Any
        ]
    }
    
    //DB keys
    private static let typeKey = "type"
    private static let descriptionKey = "desc"
}

extension PPE {
    
    init?(uid: String, dictionary: [String: Any]){
        guard let ppeTypeRawValue = dictionary[PPE.typeKey] as? Int,
            let ppeType = PPEType(rawValue: ppeTypeRawValue),
            let description = dictionary[PPE.descriptionKey] as? String
            else {return nil}
        
        self.init(uid: uid,
                  type: ppeType,
                  description: description)
    }
    
}
