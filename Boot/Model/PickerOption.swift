//
//  PickerOption.swift
//  Boot
//
//  Created by Chris James on 23/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation

struct PickerOption: Equatable {
    let number: Int
    let description: String
    
    //DB keys
    private static let descriptionKey = "desc"
}

extension PickerOption {
    init?(uid: String, dictionary: [String: Any]){
        guard let number = Int(uid),
            let description = dictionary[PickerOption.descriptionKey] as? String
            else {return nil}
        
        self.init(
            number: number,
            description: description
        )
    }
    
    func toDictionary() -> [String: Any] {
        return [PickerOption.descriptionKey: description as Any]
    }

}
