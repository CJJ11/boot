//
//  ProfileTemplates.swift
//  Boot
//
//  Created by Chris James on 29/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseFirestore


struct ProfileTemplate {
    let email: String
    let password: String
    let name: String
    let newcrestEmail: String
    let phone: String
    let profile: URL
    let imageId: String
}

struct ProfileTemplates {
    
    let techs = [
        ProfileTemplate(email: "tech1@boot.com", password: "aaaaaa", name: "Randy Peyton", newcrestEmail: "randy.peyton@newcrest.com", phone: "04562289794", profile: URL(string: "https://firebasestorage.googleapis.com/v0/b/boot-dab5b.appspot.com/o/profile-images%2Ffull-res%2FScreenshot%202019-05-29%20at%2017.23.17.png?alt=media&token=98397988-67e9-4af8-b930-a0545909d444")!, imageId: "Screenshot 2019-05-29 at 17.23.17.png"),
        
        ProfileTemplate(email: "tech2@boot.com", password: "aaaaaa", name: "Allyn Kendall", newcrestEmail: "allyn.kendall@newcrest.com", phone: "0471117384", profile:  URL(string: "https://firebasestorage.googleapis.com/v0/b/boot-dab5b.appspot.com/o/profile-images%2Ffull-res%2FScreenshot%202019-05-29%20at%2017.23.40.png?alt=media&token=a7853b92-f262-41e2-b350-17d66332d668")!, imageId: "Screenshot 2019-05-29 at 17.23.40.png"),
        
        ProfileTemplate(email: "tech3@boot.com", password: "aaaaaa", name: "Sandy Lesley", newcrestEmail: "sandy.lesley@newcrest.com", phone: "0444993338", profile:  URL(string: "https://firebasestorage.googleapis.com/v0/b/boot-dab5b.appspot.com/o/profile-images%2Ffull-res%2FScreenshot%202019-05-29%20at%2017.24.29.png?alt=media&token=716ee77b-a39d-46f2-9315-93ee30797735")!, imageId: "Screenshot 2019-05-29 at 17.24.29.png"),
        
        
        ProfileTemplate(email: "tech4@boot.com", password: "aaaaaa", name: "Kelley Kit", newcrestEmail: "kelley.kit@newcrest.com", phone: "0498876576", profile: URL(string: "https://firebasestorage.googleapis.com/v0/b/boot-dab5b.appspot.com/o/profile-images%2Ffull-res%2FScreenshot%202019-05-29%20at%2017.24.49.png?alt=media&token=19c6e8e2-5801-4cf1-852f-7a6f5bde37c0")!, imageId: "Screenshot 2019-05-29 at 17.24.49.png"),
        
        
        ProfileTemplate(email: "tech5@boot.com", password: "aaaaaa", name: "Alex Parris", newcrestEmail: "alex.parris@newcrest.com", phone: "0412998437", profile: URL(string: "https://firebasestorage.googleapis.com/v0/b/boot-dab5b.appspot.com/o/profile-images%2Ffull-res%2FScreenshot%202019-05-29%20at%2017.25.20.png?alt=media&token=d33bf39e-4c0c-4f9b-ac31-82c88c5b7b65")!, imageId: "Screenshot 2019-05-29 at 17.25.20.png"),
        
        ProfileTemplate(email: "tech6@boot.com", password: "aaaaaa", name: "Tayler Kendall", newcrestEmail: "tayler.kendall@newcrest.com", phone: "0400339879", profile: URL(string:  "https://firebasestorage.googleapis.com/v0/b/boot-dab5b.appspot.com/o/profile-images%2Ffull-res%2FScreenshot%202019-05-29%20at%2017.25.48.png?alt=media&token=4fa831ea-0dd1-43eb-a805-3bc9941d53c0")!, imageId: "Screenshot 2019-05-29 at 17.25.48.png")
    ]
    
    let profiles: [Profile]
    
    init(){
        profiles = techs.map({Profile(name: $0.name, email: $0.newcrestEmail, phone: $0.phone, role: .technician, minesite: "Cadia", profileImgUrl: $0.profile, profileImgId: $0.imageId, unseenNotiCount: 0)})
    }
    
    func getRandomProfile() -> Profile {
        return profiles.randomElement()!
    }
    
    func createProfiles(index: Int) {
        guard profiles.indices.contains(index) else {return}
        let profile = profiles[index]
        let ref = Firestore.firestore().collection("minesites").document("cadia").collection("users")
        
        Auth.auth().createUser(withEmail: profile.email, password: "aaaaaa") { (result, error) in
            
            if error == nil, let uid = result?.user.uid {
                
                //update profile
                let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
                changeRequest?.displayName = profile.name
                changeRequest?.photoURL = profile.profileImgUrl
                changeRequest?.commitChanges { (error) in
                    
                    //create profile in database
                    if error == nil {
                        let dict = profile.toDictionary()
                        ref.document(uid).setData(dict)
                        
                    }
                }
            }
        }
    }
    
}
