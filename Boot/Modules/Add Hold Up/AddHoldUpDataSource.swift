//
//  AddHoldUpDataSource.swift
//  Boot
//
//  Created by Chris James on 25/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import FirebaseFirestore


protocol AddHoldUpDataSource: DataSource, AnyObject {
    var dataUpdate: (([HoldUpViewModel]) -> Void)? {get set}
    var openCount: ((Int) -> Void)? {get set}
    var closedCount: ((Int) -> Void)? {get set}
    func toggleViewModels(fixed: Bool)
}

class AddHoldUpFirebaseDataSource: HoldUpDataSource {
    var refreshing: ((Bool) -> Void)?
    var empty: ((Bool, Bool) -> Void)?
    
    
    private var fixed: Bool = false
    var dataUpdate: (([HoldUpViewModel]) -> Void)?
    var openCount: ((Int) -> Void)?
    var closedCount: ((Int) -> Void)?
    
    private let openDataSource: FirebaseCollectionDataSource<HoldUp, HoldUpViewModel>
    private let closedDataSource: FirebaseCollectionDataSource<HoldUp, HoldUpViewModel>
    
    
    init(reference: CollectionReference) {
        let openQuery = reference.whereField(HoldUp.getFixedKey(), isEqualTo: false)
        let closedQuery = reference.whereField(HoldUp.getFixedKey(), isEqualTo: true)
        
        openDataSource = FirebaseCollectionDataSource(query: openQuery)
        closedDataSource = FirebaseCollectionDataSource(query: closedQuery)
        
        openDataSource.dataUpdate = {[weak self] (viewModels) in
            self?.openCount?(viewModels.count)
            if self?.fixed == false {
                self?.dataUpdate?(viewModels)
            }
        }
        
        closedDataSource.dataUpdate = {[weak self] (viewModels) in
            self?.closedCount?(viewModels.count)
            if self?.fixed == true {
                self?.dataUpdate?(viewModels)
            }
        }
    }
    
    func startListening() {
        openDataSource.startListening()
        closedDataSource.startListening()
    }
    
    func stopListening() {
        openDataSource.stopListening()
        closedDataSource.stopListening()
    }
    
    func toggleViewModels(fixed: Bool) {
        self.fixed = fixed
        if fixed {
            dataUpdate?(closedDataSource.viewModels)
        } else {
            dataUpdate?(openDataSource.viewModels)
        }
    }
    
}

