//
//  AddHoldUpVC.swift
//  Boot
//
//  Created by Chris James on 13/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit
import FirebaseFirestore

protocol AddHoldUpView: BaseView, List, BottomButtons {
    var closeBtnTapped: (() -> Void)? {get set}
}

class AddHoldUpVC: UIViewController, AddHoldUpView {
    
    weak var tableView: UITableView!
    weak var bottomButtonView: BottomButtonView!
    
    private let mediaCellName = String(describing: AddHoldUpMediaCell.self)
    private let descriptionCellName = String(describing: AddHoldUpDescriptionCell.self)
    private let attachmentCellName = String(describing: AddHoldUpAttCell.self)
    private let minDescriptionChar = 3
    
    private var mediaPicker: MediaPicker
    private let uploaderFactory: StorageUploaderFactory
    private var holdUpDescription: String = ""
    private var viewModels = [AddHoldUpAttViewModel]()
    private var uploaders = [StorageUploader]()
    
    private let uid: String
    private let username: String
    private let profileUrl: URL
    
    var closeBtnTapped: (() -> Void)?
    
    enum AddHoldUpSection: Int {
        case description = 0
        case supportingInfo = 1
        case attachments = 2
        
        static let count = 3
    }
    
    init(uid: String, username: String, profileUrl: URL, mediaPickerFactory: MediaPickerFactory,
         uploaderFactory: StorageUploaderFactory){
        self.uid = uid
        self.username = username
        self.profileUrl = profileUrl
        self.mediaPicker = mediaPickerFactory.make()
        self.uploaderFactory = uploaderFactory
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Not implemented")
    }
    
    override func loadView() {
        super.loadView()

        addTableView(estimatedRowHeight: 60.0)
        addBottomButtons(primaryBtnTitle: "Report Hold-Up", secondaryBtnTitle: "Cancel")
        
        layoutTableView(bottomView: bottomButtonView)
        layoutButtomButons()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Report Hold-Up"
        extendedLayoutIncludesOpaqueBars = true
        setupTableView()
        setupBottomButtonView()
        setMediaPickerCallbacks()
    }
    
    private func setupTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: mediaCellName, bundle: nil),
                           forCellReuseIdentifier: mediaCellName)
        tableView.register(UINib(nibName: descriptionCellName, bundle: nil),
                           forCellReuseIdentifier: descriptionCellName)
        tableView.register(UINib(nibName: attachmentCellName, bundle: nil),
                           forCellReuseIdentifier: attachmentCellName)
        tableView.allowsSelection = true
    }

    private func setMediaPickerCallbacks(){
        mediaPicker.mediaPicked =  {[weak self] fileUrl, mediaType in
            self?.addAttachment(fileUrl: fileUrl, mediaType: mediaType)
        }
        
        mediaPicker.presentImagePicker = {[weak self] imagePicker in
            self?.present(imagePicker, animated: true, completion: nil)
        }
        
        mediaPicker.presentAudioRecorder = {[weak self] audioRecorder in
            if let audioRecorderVC = audioRecorder.toPresent() {
                self?.present(audioRecorderVC, animated: true, completion: nil)
            }
        }
        
        mediaPicker.presentAlertController = {[weak self] alertController in
            self?.present(alertController, animated: true, completion: nil)
        }
    }
    
    private func addAttachment(fileUrl: URL, mediaType: MediaType) {
        let attachment = AddHoldUpAtt(localFileUrl: fileUrl, mediaType: mediaType)
        let viewModel = AddHoldUpAttViewModel(attachment: attachment)
        let uploader = uploaderFactory.make(attachment: attachment)
        uploader.callback = {[weak self] status, downloadUrl, fileName, progress, error in
            self?.updateAttachmentStatus(
                fileUrl: uploader.fileUrl,
                status: status,
                downloadUrl: downloadUrl,
                fileName: fileName,
                progress: progress,
                error: error
            )
        }
        uploaders.append(uploader)
        viewModels.append(viewModel)
        tableView.insertRows(at: [IndexPath(row: viewModels.count - 1, section: 2)], with: .automatic)
        uploader.startUpload()
    }
    
    private func removeAttachment(index: Int){
        viewModels[index].deleteFile()
        viewModels.remove(at: index)
        tableView.deleteRows(at: [IndexPath(row: index, section: 2)], with: .automatic)
    }
    
    private func updateAttachmentStatus(fileUrl: URL, status: StorageUploadStatus,
                                        downloadUrl: URL?, fileName: String?, progress: Float?, error: Error? ){
        
        guard let index = viewModels.firstIndex(where: {$0.getFileUrl() == fileUrl}) else {return}
        viewModels[index].setStatus(status)
        
        if let error = error {
            print("Upload attachment error: " + error.localizedDescription)
        }
        
        if status == StorageUploadStatus.uploading, let progress = progress {
            viewModels[index].setProgress(progress)
        }
        
        if let downloadUrl = downloadUrl, let fileName = fileName {
            viewModels[index].setDownloadUrl(downloadUrl)
            viewModels[index].setFileName(fileName)
        }
        let indexPath = IndexPath(row: index, section: AddHoldUpSection.attachments.rawValue)
        tableView.reloadRows(at: [indexPath], with: .none)
        updateSubmitBtn()
    }
    
    private func updateSubmitBtn(){
        let attachmentsUploaded = viewModels.filter({$0.getStatus() != .success}).isEmpty
        let descriptionAdded = holdUpDescription.count > minDescriptionChar
        bottomButtonView.enablePrimaryBtn(isEnabled: attachmentsUploaded && descriptionAdded)
    }
    
    private func setupBottomButtonView(){
        bottomButtonView.enablePrimaryBtn(isEnabled: false)
        bottomButtonView.primaryBtnTapped = {[weak self] in
            if let attachments = self?.viewModels.map({$0.getAttachment()}),
                let description = self?.holdUpDescription,
                let uid = self?.uid,
                let username = self?.username,
                let profileUrl = self?.profileUrl {

                let uploadDict = HoldUp.getUploadDictionary(
                    description: description,
                    reporterUsername: username,
                    reporterUid: uid,
                    reporterProfileUrl: profileUrl,
                    attachments: attachments
                )

                let reference = Firestore.firestore()
                    .collection("minesites")
                    .document("cadia")
                    .collection("holdUps")
                reference.addDocument(data: uploadDict)

                self?.closeBtnTapped?()
            }
        }
        
        bottomButtonView.secondaryBtnTapped = {[weak self] in
            //Do some cleanup here to delete attachments if necessary
            //self?.dismiss(animated: true, completion: nil)
            self?.closeBtnTapped?()
        }
    }
    
    deinit {
        viewModels.forEach({$0.deleteFile()})
    }
    
}

extension AddHoldUpVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return AddHoldUpSection.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let addHoldUpSection = AddHoldUpSection(rawValue: section) else {return 0}
        switch(addHoldUpSection){
        case .description: return 1
        case .supportingInfo: return SupportingInfo.count
        case .attachments: return viewModels.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let addHoldUpSection = AddHoldUpSection(rawValue: indexPath.section) else {return UITableViewCell()}
        switch (addHoldUpSection){
        case .description:
            let cell = tableView.dequeueReusableCell(
                withIdentifier: descriptionCellName,
                for: indexPath) as? AddHoldUpDescriptionCell ?? AddHoldUpDescriptionCell()
            
            cell.textChanged = {[weak self] description in
                DispatchQueue.main.async {
                    self?.tableView.beginUpdates()
                    self?.tableView.endUpdates()
                    self?.holdUpDescription = description
                    self?.updateSubmitBtn()
                }
            }
            return cell
            
        case .supportingInfo:
            let cell = tableView.dequeueReusableCell(
                withIdentifier: mediaCellName,
                for: indexPath) as? AddHoldUpMediaCell ?? AddHoldUpMediaCell()
            
            if let supportingInfo = SupportingInfo(rawValue: indexPath.row) {
                cell.descriptionLbl.text = supportingInfo.description
                cell.iconImgView.image = supportingInfo.image
            }
            return cell
            
        case .attachments:
            let cell = tableView.dequeueReusableCell(
                withIdentifier: attachmentCellName,
                for: indexPath) as? AddHoldUpAttCell ?? AddHoldUpAttCell()
            
            cell.viewModel = viewModels[indexPath.row]
            return cell
        }
    }
    
}

extension AddHoldUpVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if AddHoldUpSection(rawValue: indexPath.section) == .supportingInfo,
            let type = SupportingInfo(rawValue: indexPath.row){
            switch(type){
            case .recordAudio: mediaPicker.recordAudio()
            case .recordVideo: mediaPicker.recordVideo()
            case .takePhoto: mediaPicker.takePhoto()
            case .libraryUpload: mediaPicker.getMediaFromLibrary()
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        guard AddHoldUpSection(rawValue: indexPath.section) == .attachments else {return nil}
        return [UITableViewRowAction(style: .destructive, title: "Remove", handler:{[weak self] action, indexpath in
            if let attachment = self?.viewModels[indexPath.row] {
                attachment.deleteFile()
                self?.viewModels.remove(at: indexPath.row)
                self?.tableView.deleteRows(at: [indexPath], with: .automatic)
            }
        })]
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return AddHoldUpSection(rawValue: indexPath.section) == .attachments
    }
}

