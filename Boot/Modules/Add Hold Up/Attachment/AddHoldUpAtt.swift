//
//  AddHoldUpAtt.swift
//  Boot
//
//  Created by Chris James on 20/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import Firebase

struct AddHoldUpAtt {
    let mediaType: MediaType
    var downloadUrl: URL?
    var fileName: String?

    let localFileUrl: URL
    var status: StorageUploadStatus
    var progress: Float
    
    func toDictionary() -> [String: Any] {
        return [
            HoldUpAtt.fileNameKey: fileName as Any,
            HoldUpAtt.storageUrlKey: downloadUrl?.absoluteString as Any,
            HoldUpAtt.mediaTypeKey: mediaType.rawValue as Any
        ]
    }

}

extension AddHoldUpAtt {
    init(localFileUrl: URL, mediaType: MediaType){
        self.localFileUrl = localFileUrl
        self.mediaType = mediaType
        self.status = .notStarted
        self.progress = 0.0
        self.downloadUrl = nil
        self.fileName = nil
    }
    
}
