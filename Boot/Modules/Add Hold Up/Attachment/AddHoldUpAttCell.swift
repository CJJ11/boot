//
//  AddHoldUpAttCell.swift
//  Boot
//
//  Created by Chris James on 21/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

class AddHoldUpAttCell: UITableViewCell {
    
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var sizeLbl: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var statusImg: UIImageView!
    @IBOutlet weak var thumbnailImg: UIImageView!
    
    var viewModel: AddHoldUpAttViewModel? = nil {
        didSet {
            typeLbl.text = viewModel?.getMediaType()
            statusLbl.text = viewModel?.getStatusString()
            progressView.isHidden = viewModel?.getProgressIsHidden() ?? true
            if let progress = viewModel?.getProgress() {
                progressView.progress = progress
            }
            thumbnailImg.image = viewModel?.getThumbnail()
            sizeLbl.text = viewModel?.getFileSize()
            statusImg.image = viewModel?.getStatusImg()
        }
    }
    
}
