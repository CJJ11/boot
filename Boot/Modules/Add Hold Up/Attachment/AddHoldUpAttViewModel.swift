//
//  HoldUpAttachmentViewModel.swift
//  Boot
//
//  Created by Chris James on 15/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

struct AddHoldUpAttViewModel {
    private var attachment: AddHoldUpAtt
    let uid: String
    
    init(attachment: AddHoldUpAtt){
        self.attachment = attachment
        self.uid = attachment.localFileUrl.absoluteString
    }
    
    func getAttachment() -> AddHoldUpAtt {
        return attachment
    }
    
    func getStatus() -> StorageUploadStatus {
        return attachment.status
    }
    
    mutating func setStatus(_ status: StorageUploadStatus) {
        attachment.status = status
    }
    
    func getProgress() -> Float {
        return attachment.progress
    }
    
    mutating func setProgress(_ progress: Float){
        attachment.progress = progress
    }

    func getDownloadUrl() -> URL? {
        return attachment.downloadUrl
    }
    
    mutating func setDownloadUrl(_ url: URL){
        attachment.downloadUrl = url
    }
    
    func getFileName() -> String? {
        return attachment.fileName
    }
    
    mutating func setFileName(_ fileName: String){
        attachment.fileName = fileName
    }
    
    func getFileUrl() -> URL {
        return attachment.localFileUrl
    }
    
    func getMediaType() -> String {
        return attachment.mediaType.description
    }
    
    func getStatusString() -> String {
        return attachment.status.description
    }
    
    func getProgressIsHidden() -> Bool {
        return attachment.status != .uploading
            && attachment.status != .paused
    }
    
    func getThumbnail() -> UIImage? {
        switch(attachment.mediaType){
        case .image:
            return MediaService.getImage(from: attachment.localFileUrl)
            
        case .video:
            return MediaService.getVideoThumbnail(from: attachment.localFileUrl)
            
        case .audio:
            return #imageLiteral(resourceName: "Audio")
        }
    }
    
    func getFileSize() -> String? {
        return MediaService.sizeInMB(for: attachment.localFileUrl)
    }
    
    func deleteFile() {
        MediaService.deleteFile(at: attachment.localFileUrl)
    }
    
    func getStatusImg() -> UIImage? {
        switch(attachment.status){
        case .success: return #imageLiteral(resourceName: "Tick")
        case .failure: return #imageLiteral(resourceName: "Exclamation")
        default: return nil
        }
    }
    

    
    
}
