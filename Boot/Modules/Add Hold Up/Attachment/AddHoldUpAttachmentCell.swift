//
//  HoldUpAttachmentCell.swift
//  Boot
//
//  Created by Chris James on 14/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

class AddHoldUpAttCell: UITableViewCell {
    
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var statusImgView: UIImageView!
    @IBOutlet weak var progressView: UIProgressView!
    
    var viewModel: AddHoldUpAttViewModel = AddHoldUpAttViewModel() {
        didSet {
            typeLbl.text = viewModel.getMediaType()
            statusLbl.text = viewModel.getStatusString()
            progressView.isHidden = viewModel.getProgressIsHidden()
            progressView.progress = viewModel.getProgress()
        }
    }

}
