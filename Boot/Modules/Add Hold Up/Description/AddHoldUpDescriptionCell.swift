//
//  HoldUpDescriptionCell.swift
//  Boot
//
//  Created by Chris James on 14/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

class AddHoldUpDescriptionCell: UITableViewCell {

    @IBOutlet weak var descriptionTextView: UITextView!
    
    let placeHolderText = "Enter a brief description..."
    var textChanged: ((String) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        descriptionTextView.delegate = self
        descriptionTextView.textColor = .lightGray
        descriptionTextView.text = placeHolderText
    }
    
    func textChanged(action: @escaping (String) -> Void) {
        self.textChanged = action
    }
    
}

extension AddHoldUpDescriptionCell: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        textChanged?(textView.text)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = placeHolderText
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
}
