//
//  HoldUpAttachment.swift
//  Boot
//
//  Created by Chris James on 15/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation

struct AddHoldUpAttachment {
    let uid: String?
    let mediaType: MediaType
    let fileUrl: URL //Rename to downloadURL???
    let thumbnailUrl: URL?
    let videoStreamUrl: URL?
    
    func toDictionary() -> [String: Any] {
        return [
            AddHoldUpAttachment.fileUrlKey: fileUrl.absoluteString as Any,
            AddHoldUpAttachment.thumbnailUrlKey: thumbnailUrl?.absoluteString as Any,
            AddHoldUpAttachment.videoStreamUrlKey: videoStreamUrl?.absoluteString as Any,
            AddHoldUpAttachment.mediaTypeKey: mediaType.rawValue as Any
        ]
    }

    static let fileUrlKey = "fileUrl"
    static let thumbnailUrlKey = "thumbUrl"
    static let videoStreamUrlKey = "videoUrl"
    static let mediaTypeKey = "mediaType"
}

extension AddHoldUpAttachment {
    
    init(fileUrl: URL, mediaType: MediaType){
        self.uid = nil
        self.fileUrl = fileUrl
        self.mediaType = mediaType
        self.thumbnailUrl = nil
        self.videoStreamUrl = nil
    }
    
    init?(uid: String, dictionary: [String: Any]){
        guard let fileUrlString = dictionary[AddHoldUpAttachment.fileUrlKey] as? String,
            let fileUrl = URL(string: fileUrlString),
            let mediaTypeRawValue = dictionary[AddHoldUpAttachment.mediaTypeKey] as? Int,
            let mediaType = MediaType(rawValue: mediaTypeRawValue)
            else {return nil}
        
        var thumbnailUrl: URL?
        if let thumbnailUrlString = dictionary[AddHoldUpAttachment.thumbnailUrlKey] as? String {
            thumbnailUrl = URL(string: thumbnailUrlString)
        }
        
        var videoStreamUrl: URL?
        if let videoStreamUrlString = dictionary[AddHoldUpAttachment.videoStreamUrlKey] as? String {
            videoStreamUrl = URL(string: videoStreamUrlString)
        }
        
        if mediaType == .video && (thumbnailUrl == nil || videoStreamUrl == nil) {
            return nil
        }
        
        if mediaType == .image && thumbnailUrl == nil {
            return nil
        }
        
        self.init(
            uid: uid,
            mediaType: mediaType,
            fileUrl: fileUrl,
            thumbnailUrl: thumbnailUrl,
            videoStreamUrl: videoStreamUrl
        )
    }
    
}
