//
//  AddHoldUpCell.swift
//  Boot
//
//  Created by Chris James on 13/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

class AddHoldUpMediaCell: UITableViewCell {

    @IBOutlet weak var iconImgView: UIImageView!
    @IBOutlet weak var descriptionLbl: UILabel!

}
