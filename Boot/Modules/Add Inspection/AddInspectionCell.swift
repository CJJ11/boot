//
//  AddInspectionCell.swift
//  Boot
//
//  Created by Chris James on 23/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

protocol AddInspectionCellDelegate: AnyObject {
    func switchValueChanged(viewModel: AddInspectionViewModel, selected: Bool)
}

class AddInspectionCell: UITableViewCell {

    @IBOutlet weak var iconImg: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var alreadyAddedLbl: UILabel!
    @IBOutlet weak var inspectionSwitch: UISwitch!
    
    weak var delegate: AddInspectionCellDelegate?
    
    var viewModel: AddInspectionViewModel? = nil {
        didSet{
            iconImg.kf.indicatorType = .activity
            iconImg.kf.setImage(with: viewModel?.locationIconUrl)
            titleLbl.text = viewModel?.equipment
            inspectionSwitch.isOn = viewModel?.selected ?? false
            
            inspectionSwitch.isHidden = viewModel?.alreadyAdded == true
            alreadyAddedLbl.isHidden = viewModel?.alreadyAdded == false
        }
    }
    
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        if let viewModel = viewModel {
            delegate?.switchValueChanged(viewModel: viewModel, selected: sender.isOn)
        }
    }
    
}
