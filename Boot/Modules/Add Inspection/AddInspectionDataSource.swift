//
//  AddInspectionDataSource.swift
//  Boot
//
//  Created by Chris James on 24/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation

import Foundation
import FirebaseFirestore


protocol AddInspectionDataSource: DataSource, AnyObject {
    var dataUpdate: (([AddInspectionViewModel]) -> Void)? {get set}
    var enableAddBtn: ((Bool) -> Void)? {get set}
    var inspectionAddSuccess: (() -> Void)? {get set}
    var inspectionAddFailure: ((Error) -> Void)? {get set}
    
    func inspectionSelected(viewModel: AddInspectionViewModel, selected: Bool)
    func addInspections()
}

class AddInspectionFirebaseDataSource: AddInspectionDataSource {
    typealias InspectionDataSource = FirebaseCollectionDataSource<Inspection, AddInspectionViewModel>
    
    var enableAddBtn: ((Bool) -> Void)?
    var inspectionAddSuccess: (() -> Void)?
    var inspectionAddFailure: ((Error) -> Void)?
    
    private let authSource: AuthSource
    private let currentInspectionsDataSource: InspectionDataSource
    private let inspectionTemplatesDataSource: InspectionDataSource
    private let currentInspectionsRef: CollectionReference
    private let inspectionTemplatesRef: CollectionReference
    private var currentInspections = [AddInspectionViewModel]()
    
    var dataUpdate: (([AddInspectionViewModel]) -> Void)?
    var refreshing: ((Bool) -> Void)?
    var empty: ((Bool, Bool) -> Void)?
    
    
    init(currentInspectionsRef: CollectionReference, inspectionTemplatesRef: CollectionReference, authSource: AuthSource) {
        
        self.authSource = authSource
        self.currentInspectionsRef = currentInspectionsRef
        self.inspectionTemplatesRef = inspectionTemplatesRef
        
        currentInspectionsDataSource = InspectionDataSource(query: currentInspectionsRef, authSource: authSource)
        inspectionTemplatesDataSource = InspectionDataSource(query: inspectionTemplatesRef, authSource: authSource)

        currentInspectionsDataSource.dataUpdate = {[weak self] (currentInspections) in
            self?.currentInspections = currentInspections
            self?.updateViewModels()
        }

        inspectionTemplatesDataSource.dataUpdate = {[weak self] (viewModels) in
            self?.updateViewModels()
        }
        
        inspectionTemplatesDataSource.empty = {[weak self] (empty, animating) in
            self?.empty?(empty, animating)
        }
        
        inspectionTemplatesDataSource.refreshing = {[weak self] (refreshing) in
            self?.refreshing?(refreshing)
        }
    }
    
    private func updateViewModels(){
        var updatedViewModels = [AddInspectionViewModel]()
        for viewModel in inspectionTemplatesDataSource.viewModels {
            let added = alreadyAdded(viewModel.templateUid)
            var updatedViewModel = viewModel
            updatedViewModel.setAlreadyAdded(added)
            updatedViewModels.append(updatedViewModel)
        }
        dataUpdate?(updatedViewModels)
    }
    
    private func alreadyAdded(_ templateUid: String) -> Bool {
        return currentInspections.contains(where: {
            $0.templateUid == templateUid && $0.status != .cancelled
        })
    }
    
    func startListening() {
        currentInspectionsDataSource.startListening()
        inspectionTemplatesDataSource.startListening()
    }
    
    func stopListening() {
        currentInspectionsDataSource.stopListening()
        inspectionTemplatesDataSource.stopListening()
    }
    
    func resumeListening() {
        //currentInspectionsDataSource.resumeListening()
        //inspectionTemplatesDataSource.resumeListening()
    }
    
    func pullToRefresh() {
        //
    }

    func inspectionSelected(viewModel: AddInspectionViewModel, selected: Bool){
        if let index =  inspectionTemplatesDataSource.indexOf(viewModel.uid){
             inspectionTemplatesDataSource.viewModels[index].setSelected(selected)
            updateViewModels()
        }
        let inspectionSelected = !inspectionTemplatesDataSource.viewModels.filter({$0.selected}).isEmpty
        enableAddBtn?(inspectionSelected)
    }
    
    func addInspections(){
        guard let uid = authSource.getUid(), let username = authSource.getName() else {return}
        let selectedInspections =  inspectionTemplatesDataSource.viewModels.filter({$0.selected})
        stopListening()
        
        let db = Firestore.firestore()
        let batch = db.batch()
        
        for viewModel in selectedInspections {
            let inspectionRef = db.collection("minesites")
                .document("cadia")
                .collection("inspections")
                .document()

            let uploadDict = Inspection.add(inspection: viewModel.inspection, uid: uid, username: username)
            batch.setData(uploadDict, forDocument: inspectionRef)
        }

        batch.commit() {[weak self] err in
            if let err = err {
                self?.inspectionAddFailure?(err)
                self?.startListening()
            } else {
                self?.inspectionAddSuccess?()
            }
        }
    }
    
}
