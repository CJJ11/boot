//
//  AddInspectionVC.swift
//  Boot
//
//  Created by Chris James on 23/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit
import Dwifft

protocol AddInspectionView: BaseView, List, BottomButtons {
    var closeBtnTapped: (() -> Void)? {get set}
}

class AddInspectionVC: UIViewController, AddInspectionView {

    weak var emptyView: UIView!
    weak var refreshControl: UIRefreshControl!
    weak var tableView: UITableView!
    weak var bottomButtonView: BottomButtonView!
    
    var dataSource: AddInspectionDataSource
    var closeBtnTapped: (() -> Void)?
    
    private var cellHeights = [IndexPath : CGFloat]()
    private var headerHeights = [Int : CGFloat]()
    private var footerHeights = [Int : CGFloat]()
    
    var diffCalculator: TableViewDiffCalculator<String, AddInspectionViewModel>?
    private let cellName = String(describing: AddInspectionCell.self)
    
    init(dataSource: AddInspectionDataSource){
        self.dataSource = dataSource
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Not implemented")
    }
    
    override func loadView() {
        super.loadView()
        addTableView(estimatedRowHeight: 50.0, style: .grouped)
        addBottomButtons(primaryBtnTitle: "Add", secondaryBtnTitle: "Cancel")

        layoutTableView(bottomView: bottomButtonView)
        layoutButtomButons()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Add Inspections"
        extendedLayoutIncludesOpaqueBars = true
        
        setupBottomButtonView()
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: cellName, bundle: nil), forCellReuseIdentifier: cellName)
        tableView.allowsSelection = false
        
        diffCalculator = TableViewDiffCalculator(tableView: tableView)
        diffCalculator?.tableView = tableView
        diffCalculator?.deletionAnimation = .none
        diffCalculator?.insertionAnimation = .fade
        
        setupDataSource()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        dataSource.startListening()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        dataSource.stopListening()
    }
    
    private func setupDataSource(){
        dataSource.dataUpdate = {[weak self] (viewModels) in
            self?.diffCalculator?.sectionedValues = SectionedValues(
                values: viewModels,
                valueToSection: {$0.location},
                sortSections: {$0 < $1},
                sortValues: {$0.equipment < $1.equipment})
        }
        
        dataSource.enableAddBtn = {[weak self] (enable) in
            self?.bottomButtonView.enablePrimaryBtn(isEnabled: enable)
        }
        
        dataSource.inspectionAddSuccess = {[weak self]() in
            self?.closeBtnTapped?()
        }
        
        dataSource.inspectionAddFailure = {[weak self] (error) in
            let errorAlert = UIAlertController(title: "Error",
                                               message: error.localizedDescription,
                                               preferredStyle: .alert)
            self?.present(errorAlert, animated: true, completion: nil)
        }
        
    }
    
    private func setupBottomButtonView(){
        bottomButtonView.enablePrimaryBtn(isEnabled: false)
        bottomButtonView.primaryBtnTapped = {[weak self] in
            self?.dataSource.addInspections()
        }
        
        bottomButtonView.secondaryBtnTapped = {[weak self] in
            self?.closeBtnTapped?()
        }
    }
    
}

extension AddInspectionVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        footerHeights[section] = view.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        headerHeights[section] = view.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath] ?? 60.0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        return footerHeights[section] ?? 60.0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return headerHeights[section] ?? 60.0
    }
}

extension AddInspectionVC: UITableViewDataSource {
    
    func numberOfSections(in: UITableView) -> Int {
        return diffCalculator?.numberOfSections() ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return diffCalculator?.numberOfObjects(inSection: section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = diffCalculator?.value(atIndexPath: indexPath) else {return UITableViewCell()}
        let cell = tableView.dequeueReusableCell(withIdentifier: cellName,
                                                 for: indexPath) as? AddInspectionCell ?? AddInspectionCell()
        cell.viewModel = viewModel
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return diffCalculator?.value(forSection: section)
    }
    
}

extension AddInspectionVC: AddInspectionCellDelegate {
    
    func switchValueChanged(viewModel: AddInspectionViewModel, selected: Bool) {
        dataSource.inspectionSelected(viewModel: viewModel, selected: selected)
    }
    
}

