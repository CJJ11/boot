//
//  AddInspectionViewModel.swift
//  Boot
//
//  Created by Chris James on 24/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

struct AddInspectionViewModel: ViewModel {
    
    let inspection: Inspection
    let uid: String
    let equipment: String
    let location: String
    let locationIconUrl: URL
    let templateUid: String
    let status: InspectionStatus
    
    var selected: Bool
    var alreadyAdded: Bool
    
    mutating func setSelected(_ selected: Bool){
        self.selected = selected
    }
    
    mutating func setAlreadyAdded(_ alreadyAdded: Bool){
        self.alreadyAdded = alreadyAdded
        if alreadyAdded {
            selected = false
        }
    }
}

extension AddInspectionViewModel: Equatable {
    
    init?(document: Document, authSource: AuthSource){
        guard let inspection = document as? Inspection else {return nil}
        self.inspection = inspection
        self.equipment = inspection.equipment
        self.uid = inspection.uid
        self.location = inspection.location
        self.locationIconUrl = inspection.locationIconUrl
        self.templateUid = inspection.templateUid
        self.selected = false
        self.alreadyAdded = false
        self.status = inspection.status
    }
    
}
