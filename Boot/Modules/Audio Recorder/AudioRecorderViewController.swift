//
//  AudioRecorderViewController.swift
//  Boot
//
//  Created by Chris James on 13/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit
import AVFoundation

protocol AudioRecorder: class, Presentable {
    var audioCaptureCompleted: ((URL) -> ())? {get set}
}

class AudioRecorderViewController: UIViewController, AVAudioRecorderDelegate, AudioRecorder {
    
    @IBOutlet weak var audioLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var recordBtn: UIButton!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var useAudioBtn: UIButton!
    @IBOutlet weak var retakeBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var stopBtn: UIButton!
    
    var audioCaptureCompleted: ((URL) -> ())?
    var audioCaptureCancelled: (() -> ())?

    var recorder: AVAudioRecorder!
    var player: AVAudioPlayer?
    var outputURL: URL!
    var timer: Timer?
    var milliseconds: Int = 0
    var state: RecorderState = .readyToRecord
    
    enum RecorderState {
        case readyToRecord
        case recording
        case readyToPlay
        case playing
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        timeLbl.font = UIFont.monospacedDigitSystemFont(ofSize: 40, weight: .regular)
        recordBtn.layer.cornerRadius = 4
        setupOutputUrl()
        setupRecorder()
        setupSession()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateControls()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.stopRecording),
            name: UIApplication.didEnterBackgroundNotification,
            object: nil
        )
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    private func setupOutputUrl(){
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
        let outputPath = documentsPath.appendingPathComponent("\(NSUUID().uuidString).m4a")
        outputURL = URL(fileURLWithPath: outputPath)
    }
    
    private func setupRecorder(){
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 44100,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        do {
            try recorder = AVAudioRecorder(url: outputURL, settings: settings)
        }
        catch let error as NSError {
            NSLog("Error: \(error)")
        }
        recorder.delegate = self
        recorder.prepareToRecord()
    }
    
    private func setupSession(){
        do {
            try AVAudioSession.sharedInstance().setCategory(
                .playAndRecord,
                mode: .default,
                options: [AVAudioSession.CategoryOptions.defaultToSpeaker]
            )
            try AVAudioSession.sharedInstance().setActive(true)
        }
        catch let error as NSError {
            NSLog("Error: \(error)")
        }
    }
    
    private func setupPlayer(){
        do {
            try player = AVAudioPlayer(contentsOf: outputURL)
        }
        catch let error as NSError {
            NSLog("error: \(error)")
        }
        player?.delegate = self
    }
    
    
    @objc func stopRecording() {
        if recorder.isRecording {
            recorder.stop()
        }
        player?.stop()
        updateControls()
    }
    
    @objc func updateTimeLabel(timer: Timer) {
        milliseconds = milliseconds + 1
        let milli = (milliseconds % 60) + 39
        let sec = (milliseconds / 60) % 60
        let min = milliseconds / 3600
        timeLbl.text = NSString(format: "%02d:%02d.%02d", min, sec, milli) as String
    }
    
    func cleanup() {
        timer?.invalidate()
        if recorder.isRecording {
            recorder.stop()
            recorder.deleteRecording()
        }
        if let player = player {
            player.stop()
            self.player = nil
        }
    }

    func updateControls() {
        recordBtn.isHidden = true
        cancelBtn.isHidden = true
        playBtn.isHidden = true
        stopBtn.isHidden = true
        retakeBtn.isHidden = true
        useAudioBtn.isHidden = true
        audioLbl.isHidden = true
        
        switch(state){
        case .readyToRecord:
            recordBtn.isHidden = false
            cancelBtn.isHidden = false
            audioLbl.isHidden = false
            
        case .recording:
            recordBtn.isHidden = false
            
        case .readyToPlay:
            playBtn.isHidden = false
            retakeBtn.isHidden = false
            useAudioBtn.isHidden = false
            
        case .playing:
            stopBtn.isHidden = false
            retakeBtn.isHidden = false
            useAudioBtn.isHidden = false
        }
    }

    @IBAction func recordBtnTapped(_ sender: ShutterBtn) {
        timer?.invalidate()
        
        if recorder.isRecording {
            sender.buttonState = .normal
            recorder.stop()
            state = .readyToPlay
        } else {
            sender.buttonState = .recording
            milliseconds = 0
            timeLbl.text = "00:00.00"
            timer = Timer.scheduledTimer(
                timeInterval: 0.0167,
                target: self,
                selector: #selector(self.updateTimeLabel(timer:)),
                userInfo: nil,
                repeats: true)
            recorder.deleteRecording()
            recorder.record()
            state = .recording
        }
        updateControls()
    }
    
    @IBAction func playBtnTapped(_ sender: Any) {
        setupPlayer()
        player?.play()
        state = .playing
        updateControls()
    }
    
    @IBAction func stopBtnTapped(_ sender: Any) {
        player?.stop()
        state = .readyToPlay
        updateControls()
    }
    
    @IBAction func useAudioBtnTapped(_ sender: Any) {
        cleanup()
        state = .readyToRecord
        audioCaptureCompleted?(outputURL)
        updateControls()
        //self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func retakeBtnTapped(_ sender: Any) {
        recorder.deleteRecording()
        self.player = nil
        timeLbl.text = "00:00.00"
        state = .readyToRecord
        updateControls()
    }
    
    @IBAction func cancelBtnTapped(_ sender: Any) {
        cleanup()
        updateControls()
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension AudioRecorderViewController: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        state = .readyToPlay
        updateControls()
    }
}


