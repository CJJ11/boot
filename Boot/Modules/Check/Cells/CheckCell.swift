//
//  CheckCell.swift
//  Boot
//
//  Created by Chris James on 05/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

protocol CheckCellDelegate: AnyObject {
    func checkBoxValueChanged(checkNumber: Int, value: Bool)
}

class CheckCell: UITableViewCell {
    
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var checkBox: UISwitch!
    
    weak var delegate: CheckCellDelegate?
    
    var viewModel: CheckViewModel? = nil {
        didSet {
            descriptionLbl.text = viewModel?.description
            checkBox.isOn = viewModel?.pass ?? false
        }
    }

    @IBAction func checkBoxValueChanged(_ sender: UISwitch) {
        if let number = viewModel?.number {
            delegate?.checkBoxValueChanged(checkNumber: number, value: sender.isOn)
        }
    }
}


