//
//  CheckFooterView.swift
//  Boot
//
//  Created by Chris James on 24/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

class CheckFooterView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var footerLbl: UILabel!
    
    var footerDescription: String? {
        didSet {
            footerLbl.text = footerDescription
        }
    }
    
}
