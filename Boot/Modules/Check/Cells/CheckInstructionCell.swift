//
//  CheckInstructionCell.swift
//  Boot
//
//  Created by Chris James on 17/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

class CheckInstructionCell: UITableViewCell {

    @IBOutlet weak var instructionLbl: UILabel!
    
    var viewModel: CheckViewModel? {
        didSet {
            instructionLbl.text = viewModel?.description
        }
    }
}
