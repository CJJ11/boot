//
//  CheckPickerCell.swift
//  Boot
//
//  Created by Chris James on 23/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

//protocol CheckPickerCellDelegate: AnyObject {
//    func checkPickerCellTapped(checkNumber: Int)
//}

class CheckPickerCell: UITableViewCell {

    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var selectionLbl: UILabel!
    
    
    //weak var delegate: CheckPickerCellDelegate?
        
        var viewModel: CheckViewModel? = nil {
            didSet {
                descriptionLbl.text = viewModel?.description
                selectionLbl.text = viewModel?.pickerString ?? "No selection"
                if viewModel?.pickerString == nil {
                    selectionLbl.textColor = .lightGray
                } else {
                    selectionLbl.textColor = .darkGray
                }
            }
        }
    
    
}
