//
//  CheckSliderCell.swift
//  Boot
//
//  Created by Chris James on 23/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

protocol CheckSliderCellDelegate: AnyObject {
    func checkSliderValueChanged(checkNumber: Int, value: Float)
    func checkResetTapped(checkNumber: Int)
}

class CheckSliderCell: UITableViewCell {
    
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var valueLbl: UILabel!
    @IBOutlet weak var minValueLbl: UILabel!
    @IBOutlet weak var maxValueLbl: UILabel!
    @IBOutlet weak var unitsLbl: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var resetBtn: UIButton!
    
    weak var delegate: CheckSliderCellDelegate?
    
    var viewModel: CheckViewModel? = nil {
        didSet {
            descriptionLbl.text = viewModel?.description
            
            if let sliderMin = viewModel?.sliderMin {
                minValueLbl.text = String(format: stringFormat(), sliderMin)
            } else {
                minValueLbl.text = ""
            }
            
            if let sliderMax = viewModel?.sliderMax {
                maxValueLbl.text = String(format: stringFormat(), sliderMax)
            } else {
                maxValueLbl.text = ""
            }
            
            if let sliderVal = viewModel?.sliderVal {
                setValueLbl(value: sliderVal)
            } else {
                clearValueLbl()
            }
            
            unitsLbl.text = viewModel?.sliderUnits
            
            slider.minimumValue = viewModel?.sliderMin ?? 0
            slider.maximumValue = viewModel?.sliderMax ?? 100
            slider.setValue(viewModel?.sliderVal ?? 0.0, animated: true)
            
            slider.isEnabled = viewModel?.sliderMin != nil && viewModel?.sliderMax != nil
            resetBtn.isEnabled = viewModel?.sliderVal != nil
        }
    }
    
    private func setValueLbl(value: Float){
        valueLbl.text = String(format: stringFormat(), value)
        valueLbl.textColor = .darkGray
    }
    
    private func clearValueLbl(){
        valueLbl.text = "0.0"
        valueLbl.textColor = .lightGray
    }
    
    private func stringFormat() -> String {
        if let decimals = viewModel?.sliderDecimals {
            return "%.\(decimals)f"
        } else {
            return "%.1f"
        }
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider, forEvent event: UIEvent) {
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .began:
                setValueLbl(value: sender.value)
                
            case .moved:
                setValueLbl(value: sender.value)
                
            case .ended:
                resetBtn.isEnabled = true
                if let number = viewModel?.number {
                    delegate?.checkSliderValueChanged(checkNumber: number, value: sender.value)
                }
                
            default:
                break
            }
        }
    }
    
    @IBAction func resetBtnTapped(_ sender: Any) {
        if let number = viewModel?.number {
            delegate?.checkResetTapped(checkNumber: number)
        }
        resetBtn.isEnabled = false
    }
    
    
}
