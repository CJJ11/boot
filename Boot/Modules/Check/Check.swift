//
//  Check.swift
//  Boot
//
//  Created by Chris James on 05/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import FirebaseFirestore.FIRTimestamp

struct Check: Equatable {
    let number: Int
    let type: CheckType
    let description: String
    private var pass: Bool = false
    
    private var username: String?
    private var userId: String?
    private var timestamp: Timestamp?
    
    //Check Type 1: Slider
    let sliderMin: Float?
    let sliderMax: Float?
    let sliderUnits: String?
    let sliderDecimals: Int?
    private var sliderVal: Float?
    
    
    //Check Type 2: Picker
    let pickerOptions: [PickerOption]?
    private var pickerVal: Int?
    
    func getPass() -> Bool {
        return pass
    }
    
    func getSliderVal() -> Float? {
        return sliderVal
    }
    
    func getPickerVal() -> Int? {
        return pickerVal
    }
    
    mutating func setPass(pass: Bool, userId: String, username: String ){
        guard type == .simpleBoolean else {return}
        self.pass = pass
        self.timestamp = Timestamp(date: Date())
        self.userId = userId
        self.username = username
    }
    
    mutating func setSliderVal(sliderVal: Float?, userId: String, username: String){
        guard type == .slider else {return}
        self.pass = sliderVal != nil
        self.sliderVal = sliderVal
        self.timestamp = Timestamp(date: Date())
        self.userId = userId
        self.username = username
    }
    
    mutating func setPickerVal(pickerVal: Int?, userId: String, username: String){
        guard type == .picker else {return}
        
        if let pickerVal = pickerVal, let pickerOptions = pickerOptions, pickerVal < pickerOptions.count {
            self.pass = true
            self.pickerVal = pickerVal
        } else {
            self.pass = false
            self.pickerVal = nil
        }
        
        self.timestamp = Timestamp(date: Date())
        self.userId = userId
        self.username = username
    }
    
    func getTimestamp() -> Timestamp? {
        return timestamp
    }
    
    func getUsername() -> String? {
        return username
    }
    
    func getUserId() -> String? {
        return userId
    }
    
    func getStarted() -> Bool {
        return timestamp != nil
    }
    
    func toDictionary() -> [String: Any] {
        
        var pickerOptionsDict = [String: Any]()
        if let pickerOptions = pickerOptions {
            for pickerOption in pickerOptions {
                pickerOptionsDict[String(pickerOption.number)] = pickerOption.toDictionary()
            }
        }
        
        return [Check.typeKey: type.rawValue as Any,
                Check.descriptionKey: description as Any,
                Check.passKey: pass as Any,
                
                Check.usernameKey: username as Any,
                Check.userId: userId as Any,
                Check.timestampKey: timestamp as Any,
                
                Check.sliderMinKey: sliderMin as Any,
                Check.sliderMaxKey: sliderMax as Any,
                Check.sliderValKey: sliderVal as Any,
                Check.sliderUnitsKey: sliderUnits as Any,
                Check.sliderDecimalsKey: sliderDecimals as Any,
                
                Check.pickerValKey: pickerVal as Any,
                Check.pickerOptionsKey: pickerOptionsDict as Any
        ]
    }
    
    //DB keys
    private static let typeKey = "type"
    private static let descriptionKey = "desc"
    private static let passKey = "pass"
    
    private static let timestampKey = "ts"
    private static let usernameKey = "user"
    private static let userId = "userId"
    
    private static let sliderMinKey = "sliderMin"
    private static let sliderMaxKey = "sliderMax"
    private static let sliderValKey = "sliderVal"
    private static let sliderUnitsKey = "sliderUnits"
    private static let sliderDecimalsKey = "sliderDecimals"
    
    private static let pickerValKey = "pickerVal"
    private static let pickerOptionsKey = "pickerOptions"
    
}

extension Check {
    
//    init(number: Int, type: CheckType, description: String){
//        self.number = number
//        self.type = type
//        self.description = description
//        self.sliderMin = nil
//        self.sliderMax = nil
//        self.sliderVal = nil
//        self.sliderUnits = nil
//        self.sliderDecimals = nil
//        self.pickerVal = nil
//        self.pickerOptions = nil
//    }
    
    init?(uid: String, dictionary: [String: Any]){
        guard let number = Int(uid),
            let checkTypeRawValue = dictionary[Check.typeKey] as? Int,
            let checkType = CheckType(rawValue: checkTypeRawValue),
            let description = dictionary[Check.descriptionKey] as? String,
            let pass = dictionary[Check.passKey] as? Bool
            else {return nil}
        
        let username = dictionary[Check.usernameKey] as? String
        let userId = dictionary[Check.userId] as? String
        let timestamp = dictionary[Check.timestampKey] as? Timestamp
        
        let sliderMin = dictionary[Check.sliderMinKey] as? Float
        let sliderMax = dictionary[Check.sliderMaxKey] as? Float
        let sliderVal = dictionary[Check.sliderValKey] as? Float
        let sliderUnits = dictionary[Check.sliderUnitsKey] as? String
        let sliderDecimals = dictionary[Check.sliderDecimalsKey] as? Int
        
        if checkType == .slider && (sliderMin == nil || sliderMax == nil){
            return nil
        }
        
       let pickerVal = dictionary[Check.pickerValKey] as? Int
        
        var pickerOptions = [PickerOption]()
        if let pickerOptionsDict = dictionary[Check.pickerOptionsKey] as? [String: Any] {
            for (key, value) in pickerOptionsDict {
                if let optionDict = value as? [String: Any],
                    let pickerOption = PickerOption(uid: key, dictionary: optionDict){
                    pickerOptions.append(pickerOption)
                }
            }
        }
        pickerOptions.sort(by: {$0.number < $1.number})
        
        if checkType == .picker && pickerOptions.isEmpty {
            return nil
        }
        
        self.init(
            number: number,
            type: checkType,
            description: description,
            pass: pass,
            username: username,
            userId: userId,
            timestamp: timestamp,
            sliderMin: sliderMin,
            sliderMax: sliderMax,
            sliderUnits: sliderUnits,
            sliderDecimals: sliderDecimals,
            sliderVal: sliderVal,
            pickerOptions: pickerOptions,
            pickerVal: pickerVal
        )
    }
    
    init(number: Int, description: String){
        self.number = number
        self.type = .simpleBoolean
        self.description = description
        self.sliderMin = nil
        self.sliderMax = nil
        self.sliderVal = nil
        self.sliderUnits = nil
        self.sliderDecimals = nil
        self.pickerVal = nil
        self.pickerOptions = nil
    }
    
    init(number: Int, description: String, sliderMin: Float, sliderMax: Float, sliderUnits: String, sliderDecimals: Int){
        self.init(
            number: number,
            type: .slider,
            description: description,
            pass: false,
            username: nil,
            userId: nil,
            timestamp: nil,
            sliderMin: sliderMin,
            sliderMax: sliderMax,
            sliderUnits: sliderUnits,
            sliderDecimals: sliderDecimals,
            sliderVal: nil,
            pickerOptions: nil,
            pickerVal: nil)
    }
    
    init(number: Int, description: String, pickerOptions: [PickerOption]){
        self.init(
            number: number,
            type: .picker,
            description: description,
            pass: false,
            username: nil,
            userId: nil,
            timestamp: nil,
            sliderMin: nil,
            sliderMax: nil,
            sliderUnits: nil,
            sliderDecimals: nil,
            sliderVal: nil,
            pickerOptions: pickerOptions,
            pickerVal: nil)
    }
    
    enum CheckStatus {
        case notStarted
        case pass
        case fail
    }
}
