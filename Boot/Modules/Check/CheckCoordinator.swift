//
//  CheckCoordinator.swift
//  Boot
//
//  Created by Chris James on 06/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

protocol CheckCoordinatorOutput: class {
    var finishFlow: (() -> Void)? { get set }
}

class CheckCoordinator: BaseCoordinator, CheckCoordinatorOutput {
    var finishFlow: (() -> Void)?

    private let router: Router
    private let moduleFactory: ModuleFactory
    private let coordinatorFactory: CoordinatorFactory
    private let dataSourceFactory: DataSourceFactory
    
    private var checkDataSource: CheckDataSource?
    
    init(router: Router,
         moduleFactory: ModuleFactory,
         coordinatorFactory: CoordinatorFactory,
         dataSourceFactory: DataSourceFactory){
        
        self.router = router
        self.moduleFactory = moduleFactory
        self.coordinatorFactory = coordinatorFactory
        self.dataSourceFactory = dataSourceFactory
    }
    
    override func start() {
        showCheckView()
    }
    
    //MARK: - Show current flow's controllers
    
    private func showCheckView() {
        let dataSource = dataSourceFactory.makeCheckDataSource()
        checkDataSource = dataSource
        let checkView = moduleFactory.makeCheckView(dataSource: dataSource)
        
        checkView.onCheckBoxValueChanged = { [weak self] (uid, value) in
            self?.checkDataSource?.setCheckValue(uid: uid, value: value)
        }
        //Some callback here where we can call the finish flow method
        
        dataSource.startListening()
        router.push(checkView, animated: true, hideToolBar: true, completion: nil)
    }
    
    //MARK: - Run other flows
    
    deinit {
        checkDataSource?.stopListening()
    }
}
