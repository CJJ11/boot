//
//  CheckMC.swift
//  Boot
//
//  Created by Chris James on 06/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import FirebaseFirestore


protocol CheckDataSource: AnyObject {
    var dataUpdate: (([CheckViewModel]) -> Void)? {get set}
    
    var enableNext: ((Bool) -> Void)? {get set}
    
    func startListening()
    func stopListening()
    
    func setCheckValue(checkNumber: Int, value: Bool)
    
    func setSliderValue(checkNumber: Int, value: Float?)
    func resetSlider(checkNumber: Int)
    
    func setPickerValue(checkNumber: Int, value: Int?)
    func resetPicker(checkNumber: Int)
    
    func getFooterText() -> String?
    func getOverviewTitle() -> String?
    func getOverviewDescription() -> String?
}

class CheckFirebaseDataSource: CheckDataSource {
    
    var refreshing: ((Bool) -> Void)?
    var empty: ((Bool, Bool) -> Void)?
    
    private var observer: ListenerRegistration?
    private let reference: DocumentReference
    private let authSource: AuthSource
    private let stepNumber: Int
    private var inspection: Inspection?
    
    var dataUpdate: (([CheckViewModel]) -> Void)?
    var enableNext: ((Bool) -> Void)?
    
    init(authSource: AuthSource, inspectionUid: String, stepNumber: Int){
        self.authSource = authSource
        self.reference = Firestore.firestore()
            .collection("minesites")
            .document("cadia")
            .collection("inspections")
            .document(inspectionUid)
        
        self.stepNumber = stepNumber
    }
    
    func startListening(){
        observer = reference.addSnapshotListener {[weak self] snapshot, error in
            guard let snapshot = snapshot else {
                print("error adding snapshot" + error.debugDescription)
                return
            }
            
            if let data = snapshot.data(),
                let inspection = Inspection(uid: snapshot.documentID, dictionary: data) {
                self?.inspection = inspection
                self?.sendDataUpdate(inspection)
            }
        }
    }
    
    private func sendDataUpdate(_ inspection: Inspection){
        if let step = inspection.steps.first(where: {$0.number == stepNumber}) {
            var viewModels = step.checks.map({CheckViewModel(check: $0)})
            let instruction = CheckViewModel(instruction: step.description)
            viewModels.insert(instruction, at: 0)
            dataUpdate?(viewModels)
            enableNext?(viewModels.filter({!$0.pass}).isEmpty)
            print("check datasource update")
        }
    }
    
    func stopListening(){
        observer?.remove()
    }
    
    deinit {
        stopListening()
    }
    
    func setCheckValue(checkNumber: Int, value: Bool) {
        guard let inspection = inspection,
            let uid = authSource.getUid(),
            let username = authSource.getName(),
            let profileUrl = authSource.getProfileUrl() else {return}
        
        if let uploadDict = Inspection.setCheckValue(
            inspection: inspection,
                stepNumber: stepNumber,
                checkNumber: checkNumber,
                pass: value,
                userId: uid,
                username: username,
                profileUrl: profileUrl) {
             reference.updateData(uploadDict)
        }
       
    }
    
    
    func setSliderValue(checkNumber: Int, value: Float?) {
        guard let inspection = inspection,
            let uid = authSource.getUid(),
            let username = authSource.getName(),
            let profileUrl = authSource.getProfileUrl() else {return}
        
        if let uploadDict = Inspection.setCheckSliderValue(
            inspection: inspection,
            stepNumber: stepNumber,
            checkNumber: checkNumber,
            sliderVal: value,
            userId: uid,
            username: username,
            profileUrl: profileUrl) {
            reference.updateData(uploadDict)
        }
    }
    
    
    func resetSlider(checkNumber: Int) {
        setSliderValue(checkNumber: checkNumber, value: nil)
    }
    
    
    func setPickerValue(checkNumber: Int, value: Int?) {
        guard let inspection = inspection,
            let uid = authSource.getUid(),
            let username = authSource.getName(),
            let profileUrl = authSource.getProfileUrl() else {return}
        
        if let uploadDict = Inspection.setCheckPickerValue(
            inspection: inspection,
            stepNumber: stepNumber,
            checkNumber: checkNumber,
            pickerVal: value,
            userId: uid,
            username: username,
            profileUrl: profileUrl) {
            reference.updateData(uploadDict)
        }
    }
    
    
    func resetPicker(checkNumber: Int) {
        setPickerValue(checkNumber: checkNumber, value: nil)
    }
    
    func getFooterText() -> String? {
        guard let inspection = inspection,
            let namedInspection = inspection.steps[stepNumber].latest() else {return nil}
            let timeSince = dateService.ds.timeSince(from: namedInspection.timestamp.dateValue())
            let username = authSource.getName(
                uid: namedInspection.uid,
                username: namedInspection.username
        )
            return "Last actioned by \(username ?? "") \(timeSince)"
    }
    
    func getOverviewTitle() -> String? {
        guard let inspection = inspection else {return nil}
        let totalStepsCount = inspection.steps.count
        let stepsCompletedCount = inspection.steps.filter({$0.checksPass()}).count
        
        if totalStepsCount == stepsCompletedCount {
            return "Inspection Complete!"
        }
        return  "\(stepsCompletedCount)/\(totalStepsCount) Steps Complete"
    }
    
    func getOverviewDescription() -> String? {
        guard let inspection = inspection else {return nil}
        return inspection.status.overview
    }
    
    
}
