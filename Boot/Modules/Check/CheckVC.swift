//
//  CheckVC.swift
//  Boot
//
//  Created by Chris James on 06/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit
import Dwifft

protocol CheckView: BaseView, List, BottomButtons {
    var stepNumber: Int {get}
    var onNextTapped: ((Int) -> Void)? {get set}
    var onAddHoldUpTapped: (() -> Void)? {get set}
}

class CheckVC: UIViewController, CheckView {
    
    weak var tableView: UITableView!
    weak var bottomButtonView: BottomButtonView!
    weak var addHoldUpBtn: UIBarButtonItem!
    
    let navBarTitle: String
    let stepNumber: Int

    var onNextTapped: ((Int) -> Void)?
    var onAddHoldUpTapped: (() -> Void)?
    
    private weak var dataSource: CheckDataSource?
    
    private var diffCalculator: TableViewDiffCalculator<CheckViewModel.CellType, CheckViewModel>?
    private var cellHeights = [IndexPath : CGFloat]()
    private var headerHeights = [Int : CGFloat]()
    private var footerHeights = [Int : CGFloat]()
    private let checkCellName = String(describing: CheckCell.self)
    private let checkSliderCellName = String(describing: CheckSliderCell.self)
    private let instructionCellName = String(describing: CheckInstructionCell.self)
    private let pickerCellName = String(describing: CheckPickerCell.self)
    private let footerCellName = String(describing: CheckFooterView.self)
    
    init(title: String, step: Int, dataSource: CheckDataSource){
        self.navBarTitle = title
        self.stepNumber = step
        self.dataSource = dataSource
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Not implemented")
    }
    
    override func loadView(){
        super.loadView()
        
        addTableView(estimatedRowHeight: 60.0, style: .grouped)
        addBottomButtons(primaryBtnTitle: "Next", secondaryBtnTitle: "Skip")
        
        layoutTableView(bottomView: bottomButtonView)
        layoutButtomButons()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = navBarTitle
        setupBottomButton()
        addHoldUpBarButton()
        setupTableView()
        setupDiffCalculator()
        setupDataSourceCallbacks()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        dataSource?.startListening()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        dataSource?.stopListening()
    }

    
    private func setupBottomButton(){
        bottomButtonView.enablePrimaryBtn(isEnabled: false)
        bottomButtonView.enableSecondaryBtn(isEnabled: true)
        bottomButtonView.primaryBtnTapped = {[weak self] in
            if let stepNumber = self?.stepNumber {
                self?.onNextTapped?(stepNumber)
            }
        }
        bottomButtonView.secondaryBtnTapped = {[weak self] in
            if let stepNumber = self?.stepNumber {
                self?.onNextTapped?(stepNumber)
            }
        }
    }
    
    private func addHoldUpBarButton(){
        //let addHoldUpBtn = UIBarButtonItem(barButtonSystemItem: .add,
//                                           target: self,
//                                           action: #selector(addHoldUpBtnTapped))
        
        let addHoldUpBtn = UIBarButtonItem(title: "Report Hold-Up", style: .plain, target: self, action: #selector(addHoldUpBtnTapped))
        addHoldUpBtn.isEnabled = true
        navigationItem.rightBarButtonItem = addHoldUpBtn
        self.addHoldUpBtn = addHoldUpBtn
    }
    
    @objc private func addHoldUpBtnTapped(){
        onAddHoldUpTapped?()
    }
    
    private func setupTableView(){
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: instructionCellName, bundle: nil), forCellReuseIdentifier: instructionCellName)
        tableView.register(UINib(nibName: checkCellName, bundle: nil), forCellReuseIdentifier: checkCellName)
        tableView.register(UINib(nibName: checkSliderCellName, bundle: nil), forCellReuseIdentifier: checkSliderCellName)
        tableView.register(UINib(nibName: pickerCellName, bundle: nil), forCellReuseIdentifier: pickerCellName)
        tableView.register(UINib(nibName: footerCellName, bundle: nil), forHeaderFooterViewReuseIdentifier: footerCellName)
        tableView.allowsSelection = true
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedSectionHeaderHeight = 60
        tableView.sectionFooterHeight = UITableView.automaticDimension
        tableView.estimatedSectionFooterHeight = 60
    }
    
    private func setupDiffCalculator(){
        diffCalculator = TableViewDiffCalculator(tableView: tableView)
        diffCalculator?.deletionAnimation = .fade
        diffCalculator?.insertionAnimation = .fade
    }
    
    private func setupDataSourceCallbacks(){
        dataSource?.dataUpdate = {[weak self] (viewModels) in
            self?.diffCalculator?.sectionedValues = SectionedValues(
                values: viewModels,
                valueToSection: {$0.cellType},
                sortSections: {$0.rawValue < $1.rawValue},
                sortValues: {$0.number < $1.number}
            )
            
            self?.updateFooterView()
        }
        
        dataSource?.enableNext = {[weak self] (enable) in
            self?.bottomButtonView.enablePrimaryBtn(isEnabled: enable)
            self?.bottomButtonView.enableSecondaryBtn(isEnabled: !enable)
        }
    }
    
    private func updateFooterView(){
        if let numberOfSections = diffCalculator?.numberOfSections(), numberOfSections > 0 {
            if let footerView = tableView.footerView(forSection: numberOfSections - 1) as? CheckFooterView {
                tableView.beginUpdates()
                footerView.footerDescription = dataSource?.getFooterText()
                tableView.endUpdates()
            }
        }
    }
    
    private func showPicker(for viewModel: CheckViewModel){
        guard let values = viewModel.pickerOptions else {return}
        let alert = UIAlertController(title: "Choose from the below options:", message: nil, preferredStyle: .actionSheet)
        var selectedIndex: Int? = nil
        
        alert.addPickerView(values: values, initialSelection: viewModel.pickerVal) { index in
            selectedIndex = index
        }
        
        alert.addAction(UIAlertAction(title: "Clear", style: .cancel, handler: {[weak self] (action) in
            self?.dataSource?.resetPicker(checkNumber: viewModel.number)
        }))
        
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: {[weak self] (action) in
            if let selectedIndex = selectedIndex {
               self?.dataSource?.setPickerValue(checkNumber: viewModel.number, value: selectedIndex)
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension CheckVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = diffCalculator?.value(atIndexPath: indexPath) else {return UITableViewCell()}
        
        switch viewModel.cellType {
        case .instruction:
            let cell = tableView.dequeueReusableCell(withIdentifier: instructionCellName, for: indexPath) as? CheckInstructionCell ?? CheckInstructionCell()
            cell.viewModel = viewModel
            return cell
        case .check:
            
            switch viewModel.checkType {
            case .simpleBoolean:
                let cell = tableView.dequeueReusableCell(withIdentifier: checkCellName, for: indexPath) as? CheckCell ?? CheckCell()
                cell.viewModel = viewModel
                cell.delegate = self
                return cell
                
            case .slider:
                let cell = tableView.dequeueReusableCell(withIdentifier: checkSliderCellName, for: indexPath) as? CheckSliderCell ?? CheckSliderCell()
                cell.viewModel = viewModel
                cell.delegate = self
                return cell
                
            case .picker:
                let cell = tableView.dequeueReusableCell(withIdentifier: pickerCellName, for: indexPath) as? CheckPickerCell ?? CheckPickerCell()
                cell.viewModel = viewModel
                return cell
            }
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return diffCalculator?.numberOfObjects(inSection: section) ?? 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return diffCalculator?.numberOfSections() ?? 0
    }

}


extension CheckVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        footerHeights[section] = view.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        headerHeights[section] = view.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath] ?? 60.0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        return footerHeights[section] ?? 60.0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return headerHeights[section] ?? 60.0
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        //check if it's the last section
        guard let numberOfSections = diffCalculator?.numberOfSections(),
            section == numberOfSections - 1 else {return nil}

        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: footerCellName)
            as? CheckFooterView ?? CheckFooterView()
        cell.footerDescription = dataSource?.getFooterText()
        return cell
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let viewModel = diffCalculator?.value(atIndexPath: indexPath) else {return}
        
        if viewModel.checkType == .picker {
            showPicker(for: viewModel)
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return diffCalculator?.value(forSection: section).sectionTitle
    }
}

extension CheckVC: CheckCellDelegate {
    func checkBoxValueChanged(checkNumber: Int, value: Bool) {
        dataSource?.setCheckValue(checkNumber: checkNumber, value: value)
    }
}

extension CheckVC: CheckSliderCellDelegate {
    func checkSliderValueChanged(checkNumber: Int, value: Float) {
        dataSource?.setSliderValue(checkNumber: checkNumber, value: value)
    }
    
    func checkResetTapped(checkNumber: Int) {
        dataSource?.resetSlider(checkNumber: checkNumber)
    }
    
}

