//
//  CheckViewModel.swift
//  Boot
//
//  Created by Chris James on 06/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation

struct CheckViewModel {
    let checkType: CheckType
    let number: Int
    let description: String
    let pass: Bool
    let cellType: CellType
    
    let sliderMin: Float?
    let sliderMax: Float?
    let sliderUnits: String?
    let sliderDecimals: Int?
    var sliderVal: Float?
    
    let pickerOptions: [String]?
    let pickerVal: Int?
    let pickerString: String?
}

extension CheckViewModel: Equatable {
    
    init(check: Check){
        self.checkType = check.type
        self.number = check.number
        self.description = check.description
        self.pass = check.getPass()
        self.cellType = .check
        
        self.sliderMin = check.sliderMin
        self.sliderMax = check.sliderMax
        self.sliderUnits = check.sliderUnits
        self.sliderDecimals = check.sliderDecimals
        self.sliderVal = check.getSliderVal()
        
        self.pickerOptions = check.pickerOptions?.map({$0.description})
        self.pickerVal = check.getPickerVal()
        self.pickerString = check.pickerOptions?
            .first(where: {$0.number == check.getPickerVal()})?
            .description 
    }
    
    init(instruction: String){
        self.description = instruction
        self.cellType = .instruction
        
        //Values are unimportant
        self.checkType = .simpleBoolean
        self.number = 0
        self.pass = true
        self.sliderMin = nil
        self.sliderMax = nil
        self.sliderUnits = nil
        self.sliderVal = nil
        self.sliderDecimals = nil
        self.pickerOptions = nil
        self.pickerVal = nil
        self.pickerString = nil
    }
    
    enum CellType: Int, CaseIterable {
        case instruction = 0
        case check = 1
        
        var sectionTitle: String {
            get {
                switch self {
                case .instruction: return "INSTRUCTION"
                case .check: return "CHECKS"
                }
            }
        }
    }
    
}
