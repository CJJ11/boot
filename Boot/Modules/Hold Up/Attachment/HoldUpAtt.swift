//
//  HoldUpAttachment.swift
//  Boot
//
//  Created by Chris James on 15/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import Firebase

struct HoldUpAtt {
    let uid: String
    let mediaType: MediaType
    let storageUrl: URL
    let fileName: String
    
    let iPhoneThumbnailUrl: URL?
    let iPadThumbnailUrl: URL?
    let videoStreamUrl: URL?
    
    static let mediaTypeKey = "mediaType"
    static let storageUrlKey = "storageUrl"
    static let fileNameKey = "fileName"
    static let iPhoneThumbnailUrlKey = "iPhoneThumb"
    static let iPadThumbnailUrlKey = "iPadThumb"
    static let videoStreamUrlKey = "videoUrl"

}

extension HoldUpAtt {

    init(uid: String, dictionary: [String: Any]) throws {
        guard let mediaTypeRawValue = dictionary[HoldUpAtt.mediaTypeKey] as? Int,
            let mediaType = MediaType(rawValue: mediaTypeRawValue),
            let storageUrlString = dictionary[HoldUpAtt.storageUrlKey] as? String,
            let storageUrl = URL(string: storageUrlString),
            let fileName = dictionary[HoldUpAtt.fileNameKey] as? String
            else {
                throw(HoldUpAttError.invalidData)
        }
        
        var iPhoneThumbnailUrl: URL?
        if let iPhoneThumbnailUrlString = dictionary[HoldUpAtt.iPhoneThumbnailUrlKey] as? String {
            iPhoneThumbnailUrl = URL(string: iPhoneThumbnailUrlString)
        }
        var iPadThumbnailUrl: URL?
        if let iPadThumbnailUrlString = dictionary[HoldUpAtt.iPadThumbnailUrlKey] as? String {
            iPadThumbnailUrl = URL(string: iPadThumbnailUrlString)
        }
        
        var videoStreamUrl: URL?
        if let videoStreamUrlString = dictionary[HoldUpAtt.videoStreamUrlKey] as? String {
            videoStreamUrl = URL(string: videoStreamUrlString)
        }
        
        if mediaType == .video && (videoStreamUrl == nil || iPhoneThumbnailUrl == nil || iPadThumbnailUrl == nil) {
            throw(HoldUpAttError.videoNotReady)
        }
        
        self.init(
            uid: uid,
            mediaType: mediaType,
            storageUrl: storageUrl,
            fileName: fileName,
            iPhoneThumbnailUrl: iPhoneThumbnailUrl,
            iPadThumbnailUrl: iPadThumbnailUrl,
            videoStreamUrl: videoStreamUrl
        )
    }
    
    func toDictionary() -> [String: Any] {
        return [HoldUpAtt.mediaTypeKey: mediaType.rawValue as Any,
                HoldUpAtt.storageUrlKey: storageUrl.absoluteString as Any,
                HoldUpAtt.iPhoneThumbnailUrlKey: iPhoneThumbnailUrl?.absoluteString as Any,
                HoldUpAtt.iPadThumbnailUrlKey: iPadThumbnailUrl?.absoluteString as Any,
                HoldUpAtt.videoStreamUrlKey: videoStreamUrl?.absoluteString as Any
        ]
    }
    
}

enum HoldUpAttError: Error {
    case invalidData
    case videoNotReady
}
