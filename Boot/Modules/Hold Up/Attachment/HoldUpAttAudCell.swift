//
//  HoldUpAttAudCell.swift
//  Boot
//
//  Created by Chris James on 22/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit


protocol HoldUpAttAudCellDelegate: class {
    func audioTapped(url: URL)
}

class HoldUpAttAudCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    
    weak var delegate: HoldUpAttAudCellDelegate?
    var imgTapGesture: UITapGestureRecognizer!
    var viewModel: HoldUpAttViewModel? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgTapGesture = UITapGestureRecognizer(target: self, action: #selector(imgTapped))
        imgView.addGestureRecognizer(imgTapGesture)
    }
    
    @objc func imgTapped() {
        if let url = viewModel?.storageUrl {
            delegate?.audioTapped(url: url)
        }
    }
    
}
