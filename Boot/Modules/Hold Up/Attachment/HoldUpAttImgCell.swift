//
//  HoldUpCollectionCell.swift
//  Boot
//
//  Created by Chris James on 20/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit
import Kingfisher

protocol HoldUpAttImgCellDelegate: class {
    func imageTapped(url: URL)
}

class HoldUpAttImgCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    
    weak var delegate: HoldUpAttImgCellDelegate?
    var imgTapGesture: UITapGestureRecognizer!
    
    var viewModel: HoldUpAttViewModel? = nil{
        didSet {
            imgView.kf.indicatorType = .activity
            
            if let viewModel = viewModel {
                switch UIDevice.current.userInterfaceIdiom {
                case .phone:
                    imgView.kf.setImage(with: viewModel.iPhoneThumbnailUrl ?? viewModel.storageUrl)
                case .pad:
                    imgView.kf.setImage(with: viewModel.iPadThumbnailUrl ?? viewModel.storageUrl)
                default:
                    imgView.kf.setImage(with: viewModel.storageUrl)
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgTapGesture = UITapGestureRecognizer(target: self, action: #selector(imgTapped))
        imgView.addGestureRecognizer(imgTapGesture)
    }
    
    
    @objc func imgTapped() {
        if let url = viewModel?.storageUrl {
            delegate?.imageTapped(url: url)
        }
    }
    
}



