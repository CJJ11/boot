//
//  HoldUpAttVidCell.swift
//  Boot
//
//  Created by Chris James on 22/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit


protocol HoldUpAttVidCellDelegate: class {
    func videoTapped(url: URL)
}

class HoldUpAttVidCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    
    weak var delegate: HoldUpAttVidCellDelegate?
    var imgTapGesture: UITapGestureRecognizer!
    
    var viewModel: HoldUpAttViewModel? = nil {
        didSet {
            imgView.kf.indicatorType = .activity
            if let viewModel = viewModel {
            switch UIDevice.current.userInterfaceIdiom {
            case .phone:
                imgView.kf.setImage(with: viewModel.iPhoneThumbnailUrl ?? viewModel.storageUrl)
            case .pad:
                imgView.kf.setImage(with: viewModel.iPadThumbnailUrl ?? viewModel.storageUrl)
            default:
                imgView.kf.setImage(with: viewModel.iPadThumbnailUrl)
            }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgTapGesture = UITapGestureRecognizer(target: self, action: #selector(imgTapped))
        imgView.addGestureRecognizer(imgTapGesture)
    }
    
    @objc func imgTapped() {
        if let url = viewModel?.videoStreamUrl {
            delegate?.videoTapped(url: url)
        }
    }
    
}
