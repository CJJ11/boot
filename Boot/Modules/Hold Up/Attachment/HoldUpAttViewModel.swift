//
//  HoldUpAttachmentViewModel.swift
//  Boot
//
//  Created by Chris James on 20/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation

struct HoldUpAttViewModel {
    let uid: String
    let mediaType: MediaType
    let iPhoneThumbnailUrl: URL?
    let iPadThumbnailUrl: URL?
    let videoStreamUrl: URL?
    let storageUrl: URL?
}

extension HoldUpAttViewModel: Equatable {
    
    init(holdUpAttachment: HoldUpAtt){
        self.uid = holdUpAttachment.uid
        self.mediaType = holdUpAttachment.mediaType
        self.iPhoneThumbnailUrl = holdUpAttachment.iPhoneThumbnailUrl
        self.iPadThumbnailUrl = holdUpAttachment.iPadThumbnailUrl
        self.videoStreamUrl = holdUpAttachment.videoStreamUrl
        self.storageUrl = holdUpAttachment.storageUrl
    }
    
    //Ignore the thumbnail URLs when checking if the viewModels are equal to prevent
    //reloading the image when the thumbnail is generated
    static func == (lhs: HoldUpAttViewModel, rhs: HoldUpAttViewModel) -> Bool {
        return lhs.uid == rhs.uid &&
        lhs.mediaType == rhs.mediaType &&
        lhs.storageUrl == rhs.storageUrl &&
        lhs.videoStreamUrl == rhs.videoStreamUrl
    }
    
}
