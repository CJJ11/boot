//
//  Hazard.swift
//  Boot
//
//  Created by Chris James on 12/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import FirebaseFirestore


struct HoldUp: Document {
    
    let uid: String
    let status: HoldUpStatus
    let description: String
    let reporterUsername: String
    let reporterUid: String
    let reporterProfileUrl: URL
    let reportedTimestamp: Timestamp
    let attachments: [HoldUpAtt]
    
    let inspectionUid: String?
    let stepUid: String?
    let editorUsername: String?
    let editorUid: String?
    let editedTimestamp: Timestamp?
    
    static func getUploadDictionary(
        description: String,
        reporterUsername: String,
        reporterUid: String,
        reporterProfileUrl: URL,
        attachments: [AddHoldUpAtt],
        inspectionUid: String? = nil,
        stepUid: String? = nil) -> [String: Any] {
        
        var attachmentsDict = [String: Any]()
        
        for (index, attachment) in attachments.enumerated() {
            attachmentsDict[String(index)] = attachment.toDictionary()
        }
        
        return [HoldUp.statusKey: HoldUpStatus.open.rawValue as Any,
                HoldUp.descriptionKey: description as Any,
                HoldUp.reporterUsernameKey: reporterUsername as Any,
                HoldUp.reporterUidKey: reporterUid as Any,
                HoldUp.reporterProfileUrl: reporterProfileUrl.absoluteString as Any,
                HoldUp.reportedTimestampKey: FieldValue.serverTimestamp() as Any,
                HoldUp.attachmentsKey: attachmentsDict as Any,
                HoldUp.inspectionUidKey: inspectionUid as Any,
                HoldUp.stepUidKey: stepUid as Any
        ]
    }
    
    //DB keys
    static let statusKey = "status"
    static let descriptionKey = "desc"
    static let reporterUsernameKey = "repUser"
    static let reporterUidKey = "repUid"
    static let reportedTimestampKey = "repTs"
    static let reporterProfileUrl = "repImgUrl"
    static let attachmentsKey = "att"
    
    static let inspectionUidKey = "inspUid"
    static let stepUidKey = "stepUid"
    
    static let editorUsernameKey = "editUser"
    static let editorUidKey = "editUid"
    static let editedTimestampKey = "editTs"
    
    static func getTimestampKey() -> String {
        return reportedTimestampKey
    }
    
    static func getStatusKey() -> String {
        return statusKey
    }
    
}

extension HoldUp {
    
    init?(uid: String, dictionary: [String: Any]){
        guard let statusRawValue = dictionary[HoldUp.statusKey] as? Int,
            let status = HoldUpStatus(rawValue: statusRawValue),
            let description = dictionary[HoldUp.descriptionKey] as? String,
            let reporterUsername = dictionary[HoldUp.reporterUsernameKey] as? String,
            let reporterUid = dictionary[HoldUp.reporterUidKey] as? String,
            let reporterProfileUrlString = dictionary[HoldUp.reporterProfileUrl] as? String,
            let reporterProfileUrl = URL(string: reporterProfileUrlString),
            let reportedTimestamp = dictionary[HoldUp.reportedTimestampKey] as? Timestamp
            else {return nil}
        
        var attachments = [HoldUpAtt]()
        if let attachmentsDict = dictionary[HoldUp.attachmentsKey] as? [String: Any] {
            for (key, value) in attachmentsDict {
                if let attachmentDict = value as? [String: Any]{
                    do {
                        let attachment = try HoldUpAtt(uid: key, dictionary: attachmentDict)
                        attachments.append(attachment)
                    }
                    catch HoldUpAttError.invalidData {
                        print("Invalid hold up attachment data")
                        return nil
                    } catch HoldUpAttError.videoNotReady {
                        print("Video file still processing")
                        return nil
                    } catch {
                        print("Unexpected error: \(error).")
                        return nil
                    }
                }
            }
            attachments.sort(by: {$0.mediaType.sortValue < $1.mediaType.sortValue})
        }
        
        let inspectionUid = dictionary[HoldUp.inspectionUidKey] as? String
        let stepUid = dictionary[HoldUp.stepUidKey] as? String
        
        let editorUsername = dictionary[HoldUp.editorUsernameKey] as? String
        let editorUid = dictionary[HoldUp.editorUidKey] as? String
        let editedTimestamp = dictionary[HoldUp.editedTimestampKey] as? Timestamp
        
        self.init(
            uid: uid,
            status: status,
            description: description,
            reporterUsername: reporterUsername,
            reporterUid: reporterUid,
            reporterProfileUrl: reporterProfileUrl,
            reportedTimestamp: reportedTimestamp,
            attachments: attachments,
            inspectionUid: inspectionUid,
            stepUid: stepUid,
            editorUsername: editorUsername,
            editorUid: editorUid,
            editedTimestamp: editedTimestamp
        )
    }
    
}
