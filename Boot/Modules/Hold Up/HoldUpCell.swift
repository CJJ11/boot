//
//  HazardImageCell.swift
//  Boot
//
//  Created by Chris James on 12/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit
import Kingfisher

protocol HoldUpCellDelegate: AnyObject {
    func imageTapped(url: URL)
    func videoTapped(url: URL)
    func audioTapped(url: URL)
    func optionsTapped(viewModel: HoldUpViewModel)
}

class HoldUpCell: UITableViewCell {

    @IBOutlet weak var reportedTimestampLbl: UILabel!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var optionsBtn: UIButton!
    
    private let attachmentHeight: CGFloat = 200
    weak var delegate: HoldUpCellDelegate?
    private var attachments = [HoldUpAttViewModel]()
    private let imgCellName = String(describing: HoldUpAttImgCell.self)
    private let vidCellName = String(describing: HoldUpAttVidCell.self)
    private let audCellName = String(describing: HoldUpAttAudCell.self)
    
    var viewModel: HoldUpViewModel? = nil {
        didSet {
            profileImgView.kf.indicatorType = .activity
            profileImgView.kf.setImage(with: viewModel?.profileImg)
            reportedTimestampLbl.text = viewModel?.reportedTimestamp
            usernameLbl.text = viewModel?.username
            descriptionLbl.text = viewModel?.description
            attachments = viewModel?.attachments ?? []
            
            collectionView.isHidden = attachments.isEmpty
            pageControl.isHidden = attachments.count < 2
            
            if attachments.isEmpty {
                collectionViewHeight.constant = 0
            } else {
                collectionViewHeight.constant = attachmentHeight
            }
            collectionView.layoutIfNeeded()
            collectionView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        pageControl.pageIndicatorTintColor = .lightGray
        pageControl.currentPageIndicatorTintColor = UIApplication.shared.keyWindow?.tintColor
        
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.prefetchDataSource = self
        collectionView.register(UINib(nibName: imgCellName, bundle: nil), forCellWithReuseIdentifier: imgCellName)
        collectionView.register(UINib(nibName: vidCellName, bundle: nil), forCellWithReuseIdentifier: vidCellName)
        collectionView.register(UINib(nibName: audCellName, bundle: nil), forCellWithReuseIdentifier: audCellName)
    }
    
    @IBAction func optionsBtnTapped(_ sender: Any) {
        if let viewModel = viewModel {
            delegate?.optionsTapped(viewModel: viewModel)
        }
    }
    

}

extension HoldUpCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if attachments.isEmpty {
            return CGSize.zero
        } else {
            let width = UIScreen.main.bounds.width
            let height = attachmentHeight
            return CGSize(width: width, height: height)
        }
    }
    
}

extension HoldUpCell : UICollectionViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let visibleRect = CGRect(origin: self.collectionView.contentOffset, size: self.collectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        if let visibleIndexPath = self.collectionView.indexPathForItem(at: visiblePoint) {
            self.pageControl.currentPage = visibleIndexPath.row
        }
    }

}

extension HoldUpCell : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        pageControl.numberOfPages = attachments.count
        return attachments.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let viewModel = attachments[indexPath.row]
        let mediaType = viewModel.mediaType
        
        switch(mediaType){
        case .image:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: imgCellName,
                                                          for: indexPath) as? HoldUpAttImgCell ?? HoldUpAttImgCell()
            cell.viewModel = viewModel
            cell.delegate = self
            cell.layoutIfNeeded()
            return cell
            
        case .video:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: vidCellName,
                                                          for: indexPath) as? HoldUpAttVidCell ?? HoldUpAttVidCell()
            cell.viewModel = viewModel
            cell.delegate = self
            cell.layoutIfNeeded()
            return cell
            
        case .audio:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: audCellName,
                                                          for: indexPath) as? HoldUpAttAudCell ?? HoldUpAttAudCell()
            cell.viewModel = viewModel
            cell.delegate = self
            cell.layoutIfNeeded()
            return cell
        }
    }
    
}

extension HoldUpCell: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        let urls = indexPaths.compactMap({attachments[$0.row].iPhoneThumbnailUrl})
        ImagePrefetcher(urls: urls).start()
    }
    
}

extension HoldUpCell: HoldUpAttImgCellDelegate {
    func imageTapped(url: URL) {
        delegate?.imageTapped(url: url)
    }

}

extension HoldUpCell: HoldUpAttVidCellDelegate {
    func videoTapped(url: URL) {
        delegate?.videoTapped(url: url)
    }
}

extension HoldUpCell: HoldUpAttAudCellDelegate {
    func audioTapped(url: URL) {
        delegate?.audioTapped(url: url)
    }
}
