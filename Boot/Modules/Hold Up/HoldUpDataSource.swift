//
//  HoldUpDataSource.swift
//  Boot
//
//  Created by Chris James on 12/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import FirebaseFirestore


protocol HoldUpDataSource: DataSource, AnyObject {
    var dataUpdate: (([HoldUpViewModel]) -> Void)? {get set}
    var count: ((HoldUpStatus, Int) -> Void)? {get set}
    func toggleViewModels(type: HoldUpStatus)
    func updateHoldUpStatus(holdUpUid: String, status: HoldUpStatus)
    func getViewModels() -> [HoldUpViewModel]
}

class HoldUpFirebaseDataSource: DataSourceAggregate<HoldUp, HoldUpViewModel, HoldUpStatus>, HoldUpDataSource {

    init(reference: CollectionReference, authSource: AuthSource) {
        let types: [HoldUpStatus] = [.open, .closed]
        super.init(reference: reference, authSource: authSource, types: types, firstSelectedType: .open, filterDatabaseKey: HoldUp.getStatusKey(), sortDatabaseKey: HoldUp.getTimestampKey(), sortDescending: true)
    }
    
    func updateHoldUpStatus(holdUpUid: String, status: HoldUpStatus){
        reference.document(holdUpUid).updateData([HoldUp.statusKey: status.rawValue as Any])
    }
    
}
