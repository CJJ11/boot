//
//  HazardViewController.swift
//  Boot
//
//  Created by Chris James on 12/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit
import AVKit
import Agrume
import Dwifft

protocol HoldUpView: BaseView, RefreshingList {
    var onAddHoldUpBtnTapped: (() -> ())? {get set}
    var onProfileTapped: (() -> ())? {get set}
}

class HoldUpVC: UIViewController, HoldUpView {
    
    weak var refreshControl: UIRefreshControl!
    weak var emptyView: UIView!
    weak var tableView: UITableView!
    weak var segmentedControl: UISegmentedControl!
    weak var addHoldUpBtn: UIBarButtonItem!
    weak var profileBtn: UIBarButtonItem!
    
    private let segments: [HoldUpStatus] = [.open, .closed]
    private let holdUpCellName = String(describing: HoldUpCell.self)
    private let role: RoleType
    
    weak var dataSource: HoldUpDataSource?
    private var firstLoad: Bool = true
    private var diffCalculator: SingleSectionTableViewDiffCalculator<HoldUpViewModel>?
    
    var onAddHoldUpBtnTapped: (() -> ())?
    var onProfileTapped: (() -> ())?
    
    
    // MARK: Initialisers
    
    init(role: RoleType, dataSource: HoldUpDataSource){
        self.dataSource = dataSource
        self.role = role
        super.init(nibName: nil, bundle: nil)
        initAudioSession()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Not implemented")
    }
    
    
    // MARK: Lifecycle Functions
    
    override func loadView() {
        super.loadView()
        addViews(estimatedRowHeight: 400,
                 emptyImage: segments[0].emptyImage,
                 emptyText: segments[0].emptyText,
                 style: .plain)
        layoutViews()
        
        addSegmentedControl()
        addRightBarButton()
        if role == .technician {
            addProfileBarButton()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        setupTableView()
        setupDiffCalculator()
        setupDataSourceCallbacks()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if firstLoad {
            dataSource?.startListening()
            firstLoad = false
        } else {
            dataSource?.resumeListening()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        dataSource?.stopListening()
    }
    
    private func initAudioSession(){
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
        } catch {
            print("Error setting audio session")
        }
    }
    
    // MARK: UI Setup Functions
    
    private func addSegmentedControl() {
        let segmentedControl = UISegmentedControl(items: segments.map({segmentTitle(status: $0)}))
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.addTarget(self, action: #selector(segmentedControlChanged(_:)), for: .valueChanged)
        navigationItem.titleView = segmentedControl
        self.segmentedControl = segmentedControl
    }
    
    private func addRightBarButton(){
        let addHoldUpBtn = UIBarButtonItem(barButtonSystemItem: .add,
                                           target: self,
                                           action: #selector(addHoldUpBtnTapped))
        addHoldUpBtn.isEnabled = true
        navigationItem.rightBarButtonItem = addHoldUpBtn
        self.addHoldUpBtn = addHoldUpBtn
    }
    
    private func addProfileBarButton(){
        //guard permission == .supervisor else {return}
        let profileBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "Profile"), style: .plain, target: self, action: #selector(profileBtnTapped))
        navigationItem.leftBarButtonItem = profileBtn
        self.profileBtn = profileBtn
    }
    
    @objc private func profileBtnTapped(){
        onProfileTapped?()
    }
    
    private func setupNavBar(){
        title = "Hold Ups"
        extendedLayoutIncludesOpaqueBars = true
        view.backgroundColor = .white
    }
    
    private func setupTableView(){
        tableView.allowsSelection = false
        tableView.dataSource = self
        tableView.register(UINib(nibName: holdUpCellName, bundle: nil),
                           forCellReuseIdentifier: holdUpCellName)
        tableView.tableFooterView = UIView()
        tableView.sectionHeaderHeight = 0
        tableView.showsHorizontalScrollIndicator = false
        tableView.backgroundColor = UIColor.groupTableViewBackground
        tableView.allowsSelection = false
    }
    
    private func setupDiffCalculator(){
        diffCalculator = SingleSectionTableViewDiffCalculator(tableView: tableView)
        diffCalculator?.deletionAnimation = .none
        diffCalculator?.insertionAnimation = .none
    }
    
    private func setupDataSourceCallbacks(){
        dataSource?.dataUpdate = {[weak self] (viewModels) in
            self?.diffCalculator?.rows = viewModels
        }
        
        dataSource?.count = {[weak self] (status, count) in
            if let index = self?.segments.firstIndex(of: status),
                let title = self?.segmentTitle(status: status, count: count){
                self?.segmentedControl.setTitle(title, forSegmentAt: index)
            }
        }
        
        dataSource?.refreshing = updateRefreshing
        dataSource?.empty = updateEmpty
    }
    
    private func segmentTitle(status: HoldUpStatus, count: Int = 0) -> String {
        return status.title + " (\(count))"
    }
    

    // MARK: UI Response Functions
    
    func pullToRefresh() {
        dataSource?.pullToRefresh()
    }
    
    @objc func segmentedControlChanged(_ sender: UISegmentedControl){
        let status = segments[sender.selectedSegmentIndex]
        updateEmptyView(status: status)        
        dataSource?.toggleViewModels(type: status)

    }
    
    @objc private func addHoldUpBtnTapped(){
        onAddHoldUpBtnTapped?()
    }
    
    private func updateEmptyView(status: HoldUpStatus){
        (emptyView as! EmptyView).emptyLbl.text = status.emptyText
        (emptyView as! EmptyView).emptyImg.image = status.emptyImage
    }
    
}

extension HoldUpVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = diffCalculator?.rows[indexPath.row] else {return UITableViewCell()}
        let cell = tableView.dequeueReusableCell(withIdentifier: holdUpCellName, for: indexPath)
            as? HoldUpCell ?? HoldUpCell()
        cell.viewModel = viewModel
        cell.delegate = self
        cell.optionsBtn.isHidden = role != .supervisor
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return diffCalculator?.rows.count ?? 0
    }
    
}

extension HoldUpVC: HoldUpCellDelegate {
    
    func imageTapped(url: URL) {
        let agrume = Agrume(
            url: url,
            background: Background.blurred(.dark),
            dismissal: Dismissal.withPhysics
        )
        agrume.show(from: self)
    }
    
    func videoTapped(url: URL) {
        let player = AVPlayer(url: url)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        
        present(playerViewController, animated: true) {
            player.play()
        }
    }
    
    func audioTapped(url: URL){
        let player = AVPlayer(url: url)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        
        present(playerViewController, animated: true) {
            player.play()
        }
    }
    
    func optionsTapped(viewModel: HoldUpViewModel) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        switch viewModel.status {
        case .open:
            alert.addAction(UIAlertAction(title: "Mark as Closed", style: .default, handler: {[weak self] (action) in
                self?.dataSource?.updateHoldUpStatus(holdUpUid: viewModel.uid, status: HoldUpStatus.closed)
            }))
            
        case .closed:
            alert.addAction(UIAlertAction(title: "Mark as Open", style: .default, handler: {[weak self] (action) in
                self?.dataSource?.updateHoldUpStatus(holdUpUid: viewModel.uid, status: HoldUpStatus.open)
            }))
        }
        

        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}

