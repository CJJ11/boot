//
//  HazardViewModel.swift
//  Boot
//
//  Created by Chris James on 12/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

struct HoldUpViewModel: ViewModel {
    let uid: String
    let reportedTimestamp: String
    let step: String
    let username: String
    let status: HoldUpStatus
    let description: String
    let profileImg: URL
    let attachments: [HoldUpAttViewModel]
}

extension HoldUpViewModel: Equatable {
    init?(document: Document, authSource: AuthSource){
        guard let holdUp = document as? HoldUp else {return nil}
        self.uid = holdUp.uid
        self.reportedTimestamp = dateService.ds.timeSince(from: holdUp.reportedTimestamp.dateValue())
        self.step = "This is the step"
        self.username = holdUp.reporterUsername
        self.status = holdUp.status
        self.description = holdUp.description
        self.profileImg = holdUp.reporterProfileUrl
        self.attachments = holdUp.attachments
            .compactMap({HoldUpAttViewModel(holdUpAttachment: $0)})
    }
    
    static func == (lhs: HoldUpViewModel, rhs: HoldUpViewModel) -> Bool {
        return lhs.uid == rhs.uid &&
            lhs.step == rhs.step &&
            lhs.username == rhs.username &&
            lhs.status == rhs.status &&
            lhs.description == rhs.description &&
            lhs.profileImg == rhs.profileImg &&
            lhs.attachments == rhs.attachments
    }
    
    
}
