//
//  InspDetailBtnCell.swift
//  Boot
//
//  Created by Chris James on 02/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

class InspDetailBtnCell: UITableViewCell {

    @IBOutlet weak var buttonView: BottomButtonView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
