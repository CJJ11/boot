//
//  InspDetailHazardCell.swift
//  Boot
//
//  Created by Chris James on 18/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

class InspDetailImgCell: UITableViewCell {
    
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var hazardImg: UIImageView!
    
    var hazard: Hazard? {
        didSet {
            descriptionLbl.text = hazard?.description
            hazardImg.image = hazard?.image
        }
    }
    
    var hazardOther: String? {
        didSet {
            descriptionLbl.text = hazardOther
            hazardImg.image = #imageLiteral(resourceName: "Exclamation")
        }
    }
    
}
