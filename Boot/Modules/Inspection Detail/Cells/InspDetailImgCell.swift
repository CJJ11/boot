//
//  InspDetailHazardCell.swift
//  Boot
//
//  Created by Chris James on 18/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

class InspDetailImgCell: UITableViewCell {
    
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var iconImg: UIImageView!
    
    var icon: UIImage? {
        didSet {
            iconImg.image = icon
        }
    }
    
    var desc: String? {
        didSet {
            descriptionLbl.text = desc
        }
    }
    
    var type: String? {
        didSet {
            typeLbl.text = type
        }
    }
}
