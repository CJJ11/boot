//
//  InspDetailLongCell.swift
//  Boot
//
//  Created by Chris James on 18/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

class InspDetailTxtCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    
    func configure(title: String, description: String){
        titleLbl.text = title
        descriptionLbl.text = description
    }
    
}
