//
//  InspectionDetailVC.swift
//  Boot
//
//  Created by Chris James on 18/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

protocol InspectionDetailView: BaseView, List, BottomButtons {
    var startInspection: (() -> Void)? {get set}
}

class InspectionDetailVC: UIViewController, InspectionDetailView {

    weak var tableView: UITableView!
    weak var bottomButtonView: BottomButtonView!
    
    var viewModel: InspectionDetailViewModel
    var startInspection: (() -> Void)?
    let imgCellName = String(describing: InspDetailImgCell.self)
    let txtCellName = String(describing: InspDetailTxtCell.self)
    
    var sections: [InspDetailSection] = [.summary, .requirements]
    
    enum InspDetailSection {
        case summary
        case requirements
        case hazards
        case ppe
        
        var title: String {
            switch (self){
            case .summary: return ""
            case .requirements: return "REQUIREMENTS"
            case .hazards: return "HAZARDS PRESENT"
            case .ppe: return "PPE REQUIRED"
            }
        }
    }
    
    enum SummaryRows: Int {
        case scope = 0
        case time = 1
        
        static let count = 2
    }
    
    enum RequirementRows: Int {
        case permits = 0
        case isolations = 1
        case competencies = 2
        case equipment = 3
        
        static let count = 4
    }
    
    init(viewModel: InspectionDetailViewModel){
        self.viewModel = viewModel
        if !viewModel.hazardDescriptions.isEmpty {
            sections.append(.hazards)
        }
        if !viewModel.ppeDescriptions.isEmpty {
            sections.append(.ppe)
        }
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Not implemented")
    }
    
    override func loadView() {
        super.loadView()
        
        addTableView(estimatedRowHeight: 60.0)
        addBottomButtons(primaryBtnTitle: viewModel.primaryBtnText, secondaryBtnTitle: nil)
        
        layoutTableView(bottomView: bottomButtonView)
        layoutButtomButons()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupNavBar()

        setupTableView()

        enablePrimaryBtn(false)
        bottomButtonView.primaryBtnTapped = {[weak self] in
            self?.startInspection?()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.flashScrollIndicators()
    }
    
    private func setupNavBar(){
        title = viewModel.title
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.view.backgroundColor = .white
        extendedLayoutIncludesOpaqueBars = true
    }
    
    private func setupTableView(){
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: imgCellName, bundle: nil), forCellReuseIdentifier: imgCellName)
        tableView.register(UINib(nibName: txtCellName, bundle: nil), forCellReuseIdentifier: txtCellName)
        tableView.allowsSelection = false
    }
    
}

extension InspectionDetailVC: UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == tableView.numberOfSections - 1 &&
            indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
            bottomButtonView.enablePrimaryBtn(isEnabled: true)
        }
    }
}

extension InspectionDetailVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch sections[section] {
        case .summary: return SummaryRows.count
        case .requirements: return RequirementRows.count
        case .hazards: return viewModel.hazardDescriptions.count
        case .ppe: return viewModel.ppeDescriptions.count
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].title
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch sections[indexPath.section] {
            
        case .summary:
            let cell = tableView.dequeueReusableCell(withIdentifier: txtCellName, for: indexPath)
                as? InspDetailTxtCell ?? InspDetailTxtCell()
            
            switch SummaryRows(rawValue: indexPath.row)! {
            case .scope: cell.configure(title: "Scope", description: viewModel.scope)
            case .time: cell.configure(title: "Time Required", description: viewModel.timeRequired)
            }
            return cell
            
        case .requirements:
            let cell = tableView.dequeueReusableCell(withIdentifier: txtCellName, for: indexPath)
                as? InspDetailTxtCell ?? InspDetailTxtCell()
            
            switch RequirementRows(rawValue: indexPath.row)! {
            case .equipment: cell.configure(title: "Tools & Equipment", description: viewModel.tools)
            case .competencies: cell.configure(title: "Competencies", description: viewModel.competencies)
            case .isolations: cell.configure(title: "Isolations", description: viewModel.isolations)
            case .permits: cell.configure(title: "Permits", description: viewModel.permits)
            }
            return cell
            
        case .hazards:
            let cell = tableView.dequeueReusableCell(withIdentifier: imgCellName, for: indexPath)
                as? InspDetailImgCell ?? InspDetailImgCell()

            cell.type = viewModel.hazardTypes[indexPath.row]
            cell.desc = viewModel.hazardDescriptions[indexPath.row]
            cell.icon = viewModel.hazardImages[indexPath.row]
            return cell
            
        case .ppe:
            let cell = tableView.dequeueReusableCell(withIdentifier: imgCellName, for: indexPath)
                as? InspDetailImgCell ?? InspDetailImgCell()
            
            cell.type = viewModel.ppeTypes[indexPath.row]
            cell.desc = viewModel.ppeDescriptions[indexPath.row]
            cell.icon = viewModel.ppeImages[indexPath.row]
            return cell
        }
    }
    
}
