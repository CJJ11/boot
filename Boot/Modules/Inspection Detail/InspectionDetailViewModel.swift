//
//  InspectionDetailViewModel.swift
//  Boot
//
//  Created by Chris James on 18/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

struct InspectionDetailViewModel {
    let title: String
    let location: String
    let scope: String
    let timeRequired: String
    let primaryBtnText: String
    
    let tools: String
    let isolations: String
    let competencies: String
    let permits: String
    
    let hazardTypes: [String]
    let hazardDescriptions: [String]
    let hazardImages: [UIImage]
    
    let ppeTypes: [String]
    let ppeDescriptions: [String]
    let ppeImages: [UIImage]
}

extension InspectionDetailViewModel {
    init(inspection: Inspection){
        self.title = inspection.equipment
        
        self.location = inspection.location
        self.scope = inspection.scope
        self.timeRequired = inspection.timeRequired

        switch inspection.status {
        case .notStarted:
            self.primaryBtnText = "Start Inspection"
        case .underway:
            self.primaryBtnText = "Continue Inspection"
        default:
            self.primaryBtnText = "View Inspection"
        }
        
        self.tools = inspection.tools ?? "N/A"
        self.isolations = inspection.isolations ?? "N/A"
        self.competencies = inspection.competencies ?? "N/A"
        self.permits = inspection.permits ?? "N/A"
        
        self.hazardTypes = inspection.hazards.map({$0.type.description})
        self.hazardDescriptions = inspection.hazards.map({$0.description})
        self.hazardImages = inspection.hazards.map({$0.type.image})
        
        self.ppeTypes = inspection.ppe.map({$0.type.description})
        self.ppeDescriptions = inspection.ppe.map({$0.description})
        self.ppeImages = inspection.ppe.map({$0.type.image})
    }
}
