//
//  LoginVC.swift
//  Boot
//
//  Created by Chris James on 10/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit
import TransitionButton
import FirebaseAuth
import CoreGraphics


protocol LoginView: BaseView {
    var onTechnicianSelect: (() -> Void)? { get set }
    var onSupervisorSelect: (() -> Void)? {get set}
    
    func triggerAnimation()
}


class LoginVC: UIViewController, LoginView {
    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var supervisorBtn: TransitionButton!
    @IBOutlet weak var technicianBtn: TransitionButton!
    
    @IBOutlet weak var iconVerticalCenter: NSLayoutConstraint!
    @IBOutlet weak var titleVertical: NSLayoutConstraint!
    @IBOutlet weak var supervisorBtnVertical: NSLayoutConstraint!
    @IBOutlet weak var technicanBtnVertical: NSLayoutConstraint!
    
    var onSupervisorSelect: (() -> Void)?
    var onTechnicianSelect: (() -> Void)?
    
    private var animationEnabled: Bool = false
    private var alreadyAnimated: Bool = false
    private let animationTiming: TimeInterval = 1.5
    
    private lazy var start: CGFloat = (UIScreen.main.bounds.height / 2) + 50
    private let iconStart: CGFloat = 0
    
    private let iconFinish: CGFloat = -120.0
    private let titleFinish: CGFloat = 70.0
    private let supervisorBtnFinish: CGFloat = 150.0
    private let technicianBtnFinish: CGFloat = 230.0
    
    private let iconDelay: TimeInterval = 1.3
    private let titleDelay: TimeInterval = 1.5
    private let supervisorBtnDelay: TimeInterval = 1.7
    private let technicianBtnDelay: TimeInterval = 1.9
    
    private let supervisorEmail = "terry@boot.com"
    private let supervisorPassword = "aaaaaa"


    override func viewDidLoad() {
        super.viewDidLoad()

        iconVerticalCenter.constant = iconStart
        titleVertical.constant = start
        supervisorBtnVertical.constant = start
        technicanBtnVertical.constant = start
        
        
        //self.view.layoutIfNeeded()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        playAnimation()
    }
    
    func triggerAnimation(){
        animationEnabled = true
        guard self.isViewLoaded && (self.view.window != nil) else {return}
        playAnimation()
    }
    
    private func playAnimation() {
        guard animationEnabled, !alreadyAnimated else {return}
        alreadyAnimated = true
        
        UIView.animate(withDuration: 1.0,
                       delay: 0.1,
                       usingSpringWithDamping: 1.0, initialSpringVelocity: 0,
                       options: [.curveEaseInOut],
                       animations: {
                        self.icon.transform = CGAffineTransform(rotationAngle: .pi * 0.3)
        },
                       completion: nil
        )
        
        
        UIView.animate(withDuration: 1.2,
                       delay: 1.2,
                       usingSpringWithDamping: 0.5, initialSpringVelocity: 0,
                       options: [.curveEaseInOut , .allowUserInteraction],
                       animations: {
                        self.icon.transform = CGAffineTransform(rotationAngle: 0)
        },
                       completion: nil
        )
        
        
        UIView.animate(withDuration: 1.0, delay: iconDelay,
                       usingSpringWithDamping: 0.3, initialSpringVelocity: 0,
                       options: [.curveEaseIn],
                       animations: {
                        self.iconVerticalCenter.constant = self.iconFinish
                        self.view.layoutIfNeeded()
        },
                       completion: nil
        )
        
        
        UIView.animate(withDuration: animationTiming, delay: titleDelay,
                       usingSpringWithDamping: 0.5, initialSpringVelocity: 0,
                       options: [.curveEaseIn],
                       animations: {
                        self.titleVertical.constant = self.titleFinish
                        self.view.layoutIfNeeded()
        },
                       completion: nil
        )
        
        
        
        UIView.animate(withDuration: animationTiming, delay: supervisorBtnDelay,
                       usingSpringWithDamping: 0.5, initialSpringVelocity: 0,
                       options: [.curveEaseIn],
                       animations: {
                        self.supervisorBtnVertical.constant = self.supervisorBtnFinish
                        self.view.layoutIfNeeded()
        },
                       completion: nil
        )
        
        UIView.animate(withDuration: animationTiming, delay: technicianBtnDelay,
                       usingSpringWithDamping: 0.5, initialSpringVelocity: 0,
                       options: [.curveEaseIn],
                       animations: {
                        self.technicanBtnVertical.constant = self.technicianBtnFinish
                        self.view.layoutIfNeeded()
        },
                       completion: nil
        )
    }
    
    
    @IBAction func supervisorBtnTapped(_ sender: Any) {
        enableBtns(false)
        supervisorBtn.startAnimation()
        Auth.auth().signIn(withEmail: supervisorEmail, password: supervisorPassword, completion: { [weak self] (result, error)  in
            if let error = error {
                let alert = UIAlertController(title: "Sign in Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak self] (action) in
                    self?.supervisorSignIn(success: false)
                }))
                self?.present(alert, animated: true, completion: nil)
            } else {
                self?.supervisorSignIn(success: true)
            }
        })
    }
    
    
    @IBAction func technicianBtnTapped(_ sender: Any) {
        enableBtns(false)
        technicianBtn.startAnimation()
        
        let profile = ProfileTemplates().getRandomProfile()
        
        Auth.auth().signIn(withEmail: profile.email, password: "aaaaaa", completion: { [weak self] (result, error)  in
            if let error = error {
                let alert = UIAlertController(title: "Sign in Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak self] (action) in
                    self?.technicianSignIn(success: false)
                }))
                self?.present(alert, animated: true, completion: nil)
            } else {
                self?.technicianSignIn(success: true)
            }
        })
    }
    
    
    private func supervisorSignIn(success: Bool){
        if success {
            self.view.bringSubviewToFront(supervisorBtn)
            supervisorBtn.stopAnimation(animationStyle: .expand, completion: { [weak self] in
                self?.onSupervisorSelect?()
            })
        } else {
            supervisorBtn.stopAnimation()
        }
        enableBtns(true)
    }
    
    
    private func technicianSignIn(success: Bool){
        if success {
            self.view.bringSubviewToFront(technicianBtn)
            technicianBtn.stopAnimation(animationStyle: .expand, completion: { [weak self] in
                self?.onTechnicianSelect?()
            })
        } else {
            technicianBtn.stopAnimation()
        }
        enableBtns(true)
    }
    
    
    private func enableBtns(_ enable: Bool){
        technicianBtn.isEnabled = enable
        supervisorBtn.isEnabled = enable
    }
    
    
}
