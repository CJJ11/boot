//
//  ImagePickerModule.swift
//  Boot
//
//  Created by Chris James on 13/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import CoreServices
import Photos

protocol MediaPicker {
    func recordVideo()
    func recordAudio()
    func takePhoto()
    func getMediaFromLibrary()
    
    var mediaPicked: ((URL, MediaType) -> ())? {get set}
    var presentImagePicker: ((UIImagePickerController) -> ())? {get set}
    var presentAudioRecorder: ((AudioRecorder) -> ())? {get set}
    var presentAlertController: ((UIAlertController) -> ())? {get set}
}

class MediaPickerImp: NSObject, MediaPicker {
    
    var imagePicker: UIImagePickerController?
    var audioRecorder: AudioRecorder?
    
    var mediaPicked: ((URL, MediaType) -> ())?
    var presentImagePicker: ((UIImagePickerController) -> ())?
    var presentAudioRecorder: ((AudioRecorder) -> ())?
    var presentAlertController: ((UIAlertController) -> ())?
    
    private enum ImagePickerSource {
        case library
        case photo
        case video
    }
    
    func recordVideo(){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            createImagePicker(for: .video)
            checkCameraAccess()
        }
    }
    
    func takePhoto(){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            createImagePicker(for: .photo)
            checkCameraAccess()
        }
    }
    
    func getMediaFromLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            createImagePicker(for: .library)
            checkLibraryAccess()
        }
    }
    
    func recordAudio(){
        createAudioRecorder()
        checkMicrophoneAccess()
    }
    
    private func createImagePicker(for source: ImagePickerSource) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.videoQuality = .typeHigh
        
        switch(source){
        case .library:
            imagePicker.sourceType = .photoLibrary
            imagePicker.mediaTypes = [kUTTypeMovie as String, kUTTypeImage as String]
            
        case .photo:
            imagePicker.sourceType = .camera
            imagePicker.mediaTypes = [kUTTypeImage as String]
            imagePicker.cameraCaptureMode = .photo
            
        case .video:
            imagePicker.sourceType = .camera
            imagePicker.mediaTypes = [kUTTypeMovie as String]
            imagePicker.cameraCaptureMode = .video
        }
        
        self.imagePicker = imagePicker
    }
    
    private func createAudioRecorder(){
        audioRecorder = AudioRecorderViewController()
        audioRecorder?.audioCaptureCompleted = {[weak self] url in
            DispatchQueue.main.async{
                self?.mediaPicked?(url, .audio)
                self?.audioRecorder?.toPresent()?.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    
    private func checkCameraAccess() {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .denied:
            promptToUpdatePermissions(
                prompt: "Camera access is currently denied. Please enable camera access in the app settings."
            )
        case .restricted:
            print("Camera access restricted")
            
        case .authorized:
            if let imagePicker = imagePicker {
                presentImagePicker?(imagePicker)
            }
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) {[weak self] success in
                if success, let imagePicker = self?.imagePicker {
                    DispatchQueue.main.async {
                        self?.presentImagePicker?(imagePicker)
                    }
                }
            }
        }
    }
    
    private func checkLibraryAccess() {
        switch(PHPhotoLibrary.authorizationStatus()){
        case .authorized:
            if let imagePicker = imagePicker {
                presentImagePicker?(imagePicker)
            }
            
        case .denied:
            promptToUpdatePermissions(
                prompt: "Library access is currently denied. Please enable library access in the app settings.")
            
        case .restricted:
            print("Photo library access restricted")
            
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({[weak self] (status) in
                if status == .authorized, let imagePicker = self?.imagePicker {
                    DispatchQueue.main.async {self?.presentImagePicker?(imagePicker)}
                }
            })
        }
    }
    
    
    private func checkMicrophoneAccess() {
        switch AVAudioSession.sharedInstance().recordPermission {
        case .granted:
            if let audioRecorder = audioRecorder {
                presentAudioRecorder?(audioRecorder)
            }
            
        case .denied:
            promptToUpdatePermissions(
                prompt: "Microphone access is currently denied. Please enable microphone access in the app settings.")
            
        case .undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission({[weak self] (granted) in
                if granted, let audioRecorder = self?.audioRecorder {
                    DispatchQueue.main.async {self?.presentAudioRecorder?(audioRecorder)}
                }
            })
        }
    }

    private func promptToUpdatePermissions(prompt: String){
        let alertController = UIAlertController(
            title: "Update Permission Settings",
            message: prompt,
            preferredStyle: .alert
        )
        alertController.addAction(
            UIAlertAction(title: "Cancel", style: .cancel)
        )
        alertController.addAction(
            UIAlertAction(title: "Open App Settings", style: .default) { _ in
                if let url = URL(string: UIApplication.openSettingsURLString) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            }
        )
        presentAlertController?(alertController)
    }
    
}

extension MediaPickerImp: UIImagePickerControllerDelegate {
    
    func imagePickerController(
        _ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[.originalImage] as? UIImage {
            if let imageUrl = MediaService.storeToDocumentDirectory(image: image){
                mediaPicked?(imageUrl, .image)
            }
        } else if let videoUrl = info[.mediaURL] as? NSURL {
            mediaPicked?(videoUrl as URL, .video)
        } else if let imageUrl = info[.imageURL] as? NSURL {
            mediaPicked?(imageUrl as URL, .image)
        }

        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}

extension MediaPickerImp: UINavigationControllerDelegate {}

protocol MediaPickerFactory {
    func make() -> MediaPicker
}

class MediaPickerFactoryImp: MediaPickerFactory {
    func make() -> MediaPicker {
        return MediaPickerImp()
    }
}
