//
//  Notification.swift
//  Boot
//
//  Created by Chris James on 05/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import FirebaseFirestore


struct Noti: Document, Equatable {
    let uid: String
    let type: NotificationType
    let inspectionUid: String?
    let holdUpUid: String?
    let title: String
    let message: String
    let imgUrl: URL?
//    let user: String!
//    let username: String!
//    let profileUrl: String?

    let timestamp: Timestamp
    let seen: Bool
    
    //DB keys
    static let typeKey = "type"
    static let inspectionUidKey = "inspectionUid"
    static let holdUpUidKey = "holdUpUid"
    static let titleKey = "title"
    static let messageKey = "msg"
    static let timestampKey = "ts"
    static let seenKey = "seen"
    static let imgUrlKey = "imgUrl"
}

extension Noti {
    
    init?(uid: String, dictionary: [String: Any]){
        guard let typeRawValue = dictionary[Noti.typeKey] as? Int,
            let type = NotificationType(rawValue: typeRawValue),
            let title = dictionary[Noti.titleKey] as? String,
            let message = dictionary[Noti.messageKey] as? String,
            let timestamp = dictionary[Noti.timestampKey] as? Timestamp,
            let seen = dictionary[Noti.seenKey] as? Bool
            else {return nil}
        
        let inspectionUid = dictionary[Noti.inspectionUidKey] as? String
        let holdUpUid = dictionary[Noti.holdUpUidKey] as? String
        
        if type == .holdUpCreated && holdUpUid == nil {
            return nil
        }
        
        if (type == .inspectionStarted || type == .inspectionFinished) && inspectionUid == nil {
            return nil
        }
        
        var imgUrl: URL? = nil
        if let imgUrlString = dictionary[Noti.imgUrlKey] as? String {
            imgUrl = URL(string: imgUrlString)
        }
        
        self.init(
            uid: uid,
            type: type,
            inspectionUid: inspectionUid,
            holdUpUid: holdUpUid,
            title: title,
            message: message,
            imgUrl: imgUrl,
            timestamp: timestamp,
            seen: seen
        )
    }
    
    func toDictionary() -> [String: Any] {
        return [
            Noti.typeKey: type as Any,
            Noti.inspectionUidKey: inspectionUid as Any,
            Noti.holdUpUidKey: holdUpUid as Any,
            Noti.titleKey: title as Any,
            Noti.messageKey: message as Any,
            Noti.timestampKey: timestamp as Any,
            Noti.seenKey: seen as Any,
            Noti.imgUrlKey: imgUrl?.absoluteString as Any
        ]
    }
    
}
