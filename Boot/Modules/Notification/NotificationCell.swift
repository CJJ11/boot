//
//  NotificationCell.swift
//  Boot
//
//  Created by Chris James on 05/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var timestampLbl: UILabel!
    @IBOutlet weak var seenView: UIView!
    
    var viewModel: NotificationViewModel? = nil {
        didSet{
            imgView.kf.indicatorType = .activity
            imgView.kf.setImage(with: viewModel?.imgUrl)
            titleLbl.text = viewModel?.title
            messageLbl.text = viewModel?.message
            timestampLbl.text = viewModel?.timestamp
            seenView.backgroundColor = UIApplication.shared.keyWindow?.tintColor
            seenView.isHidden = viewModel?.seen == true
        }
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        let color = seenView.backgroundColor
        super.setSelected(selected, animated: animated)
        seenView.backgroundColor = color
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        let color = seenView.backgroundColor
        super.setHighlighted(highlighted, animated: animated)
        seenView.backgroundColor = color
    }
}
