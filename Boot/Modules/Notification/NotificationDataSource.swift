//
//  NotificationDataSource.swift
//  Boot
//
//  Created by Chris James on 05/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import FirebaseFirestore


protocol NotificationDataSource: DataSource, AnyObject {
    var dataUpdate: (([NotificationViewModel]) -> Void)? {get set}
    func markAsSeen()
    func deleteNotification(viewModel: NotificationViewModel)
    func deleteAllNotifications()
}

class NotificationFirebaseDataSource: FirebaseCollectionDataSource<Noti, NotificationViewModel>, NotificationDataSource {
    
    private let reference: CollectionReference
    
    init?(authSource: AuthSource){
        guard let uid = authSource.getUid() else {return nil}
        reference = Firestore.firestore()
                .collection("minesites")
                .document("cadia")
                .collection("users")
                .document(uid)
                .collection("notifications")
        
        let query = reference.order(by: Noti.timestampKey, descending: true)
        super.init(query: query, authSource: authSource)
    }
    
    func markAsSeen(){
        viewModels.forEach { (viewModel) in
            reference.document(viewModel.uid).updateData(["seen": true])
        }
    }
    
    func deleteNotification(viewModel: NotificationViewModel) {
        reference.document(viewModel.uid).delete()
    }
    
    func deleteAllNotifications() {
        let db = Firestore.firestore()
        let batch = db.batch()
        
        viewModels.forEach({batch.deleteDocument(reference.document($0.uid))})
        
        batch.commit() {err in
            if let err = err {
                print("Notification delete error: \(err.localizedDescription)")
            } else {
                print("Notifications deleted successfully")
            }
        }
    }
    
}
