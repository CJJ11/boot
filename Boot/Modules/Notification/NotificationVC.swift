//
//  NotificationVC.swift
//  Boot
//
//  Created by Chris James on 05/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit
import Dwifft

protocol NotificationView: BaseView, RefreshingList {
    var onNotificationSelect: ((NotificationViewModel) -> Void)? {get set}
    var onProfileTapped: (() -> Void)? {get set}
}

class NotificationVC: UIViewController, NotificationView {

    weak var tableView: UITableView!
    weak var emptyView: UIView!
    weak var refreshControl: UIRefreshControl!
    weak var profileBtn: UIBarButtonItem!
    weak var deleteBtn: UIBarButtonItem!
    
    var onNotificationSelect: ((NotificationViewModel) -> ())?
    var onProfileTapped: (() -> Void)?

    private let cellName = String(describing: NotificationCell.self)
    private var segmentedControl: UISegmentedControl!
    
    private var firstLoad: Bool = true
    weak var dataSource: NotificationDataSource?
    var diffCalculator: SingleSectionTableViewDiffCalculator<NotificationViewModel>?
    
    
    init(dataSource: NotificationDataSource){
        super.init(nibName: nil, bundle: nil)
        self.dataSource = dataSource
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Not implemented")
    }
    
    override func loadView(){
        super.loadView()
        addViews(estimatedRowHeight: 80.0, emptyImage: #imageLiteral(resourceName: "Mail"), emptyText: "No notifications to show", style: .plain)
        layoutViews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Notifications"
        extendedLayoutIncludesOpaqueBars = true
        view.backgroundColor = .white
        
        addProfileBarButton()
        addDeleteBarButton()
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: cellName, bundle: nil), forCellReuseIdentifier: cellName)
        tableView.tableFooterView = UIView()
        tableView.sectionHeaderHeight = 0
        tableView.showsHorizontalScrollIndicator = false
        tableView.backgroundColor = UIColor.groupTableViewBackground
        tableView.allowsSelection = true
        
        diffCalculator = SingleSectionTableViewDiffCalculator(tableView: tableView)
        diffCalculator?.deletionAnimation = .none
        diffCalculator?.insertionAnimation = .none
        
        dataSource?.empty = updateEmpty
        dataSource?.refreshing = updateRefreshing
        
        dataSource?.dataUpdate = {[weak self] (viewModels) in
            self?.diffCalculator?.rows = viewModels
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if firstLoad {
            dataSource?.startListening()
            firstLoad = false
        } else {
            dataSource?.resumeListening()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        dataSource?.markAsSeen()
        dataSource?.stopListening()
    }
    
    func pullToRefresh() {
        dataSource?.pullToRefresh()
    }
    
    private func addProfileBarButton(){
        //guard permission == .supervisor else {return}
        let profileBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "Profile"), style: .plain, target: self, action: #selector(profileBtnTapped))
        navigationItem.leftBarButtonItem = profileBtn
        self.profileBtn = profileBtn
    }
    
    @objc private func profileBtnTapped(){
        onProfileTapped?()
    }
    
    private func addDeleteBarButton(){
        let deleteBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "Delete"), style: .plain, target: self, action: #selector(deleteBtnTapped))
        navigationItem.rightBarButtonItem = deleteBtn
        self.deleteBtn = deleteBtn
    }
    
    @objc private func deleteBtnTapped(){
        let alert = UIAlertController(title: "Delete Notifications", message: "Are you sure you want to delete all notifications?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Delete All", style: .destructive, handler: {[weak self] (_) in
            self?.dataSource?.deleteAllNotifications()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
}

extension NotificationVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = diffCalculator?.rows[indexPath.row] else {return UITableViewCell()}
        let cell = tableView.dequeueReusableCell(withIdentifier: cellName, for: indexPath)
            as? NotificationCell ?? NotificationCell()
        cell.viewModel = viewModel
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return diffCalculator?.rows.count ?? 0
    }
    
}

extension NotificationVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let viewModel = diffCalculator?.rows[indexPath.row] {
            onNotificationSelect?(viewModel)
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        return [UITableViewRowAction(style: .destructive, title: "Delete", handler:{[weak self] action, indexpath in
            if let viewModel = self?.diffCalculator?.rows[indexPath.row] {
                self?.dataSource?.deleteNotification(viewModel: viewModel)
            }
        })]
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}


