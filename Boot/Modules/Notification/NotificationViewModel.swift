//
//  NotificationViewModel.swift
//  Boot
//
//  Created by Chris James on 05/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

struct NotificationViewModel: ViewModel {
    let uid: String
    let notification: Noti
    let title: String
    let message: String
    let timestamp: String
    let seen: Bool
    let imgUrl: URL?
}

extension NotificationViewModel: Equatable {

    init?(document: Document, authSource: AuthSource) {
        guard let notification = document as? Noti else {return nil}
        self.notification = notification
        self.uid = notification.uid
        self.title = notification.title
        self.message = notification.message
        self.timestamp = dateService.ds.timeSince(from: notification.timestamp.dateValue())
        self.seen = notification.seen
        self.imgUrl = notification.imgUrl
    }
    
}
