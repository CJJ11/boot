//
//  Profile.swift
//  Boot
//
//  Created by Chris James on 13/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation

struct Profile: Equatable {
    let name: String
    let email: String
    let phone: String
    let role: RoleType
    let minesite: String
    let profileImgUrl: URL
    let profileImgId: String
    let unseenNotiCount: Int
    
    //DB keys
    private static let nameKey = "name"
    private static let emailKey = "email"
    private static let phoneKey = "phone"
    private static let roleKey = "role"
    private static let minesiteKey = "site"
    private static let profileImgUrlKey = "profileImgUrl"
    private static let profileImgIdKey = "profileImgId"
    private static let unseenNotiCountKey = "notiCount"
    static let notificationTokenKey = "notiTokens"
}

extension Profile {
    init?(uid: String, dictionary: [String: Any]){
        guard let name = dictionary[Profile.nameKey] as? String,
            let email = dictionary[Profile.emailKey] as? String,
        let phone = dictionary[Profile.phoneKey] as? String,
            let unseenNotiCount = dictionary[Profile.unseenNotiCountKey] as? Int,
            let roleRawValue = dictionary[Profile.roleKey] as? Int,
            let role = RoleType(rawValue: roleRawValue),
            let minesite = dictionary[Profile.minesiteKey] as? String,
        let profileImgUrlString = dictionary[Profile.profileImgUrlKey] as? String,
            let profileImgUrl = URL(string: profileImgUrlString),
            let profileImgId = dictionary[Profile.profileImgIdKey] as? String
    else {return nil}
        
        self.init(
            name: name,
            email: email,
            phone: phone,
            role: role,
            minesite: minesite,
            profileImgUrl: profileImgUrl,
            profileImgId: profileImgId,
            unseenNotiCount: unseenNotiCount)
    }
    
    func toDictionary() -> [String: Any] {
        return [
            Profile.nameKey: name as Any,
            Profile.emailKey: email as Any,
            Profile.phoneKey: phone as Any,
            Profile.roleKey: role.rawValue as Any,
            Profile.minesiteKey: minesite as Any,
            Profile.profileImgUrlKey: profileImgUrl.absoluteString as Any,
            Profile.profileImgIdKey: profileImgId as Any,
            Profile.unseenNotiCountKey: unseenNotiCount as Any
        ]
    }
}
