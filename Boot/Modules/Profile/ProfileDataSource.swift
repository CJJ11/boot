//
//  ProfileDataSource.swift
//  Boot
//
//  Created by Chris James on 12/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import FirebaseFirestore

protocol ProfileDataSource: class {
    var dataUpdate: ((ProfileViewModel) -> Void)? {get set}
    func startListening()
    func stopListening()
}


class ProfileFirebaseDataSource: ProfileDataSource {
    var refreshing: ((Bool) -> Void)?
    var empty: ((Bool, Bool) -> Void)?
    
    var observer: ListenerRegistration?
    var reference: DocumentReference
    var dataUpdate: ((ProfileViewModel) -> Void)?
    
    init?(authSource: AuthSource){
        guard let uid = authSource.getUid() else {return nil}
        
        reference = Firestore.firestore()
            .collection("minesites")
            .document("cadia")
            .collection("users")
            .document(uid)
    }
    
    
    func startListening(){
        observer = reference.addSnapshotListener {[weak self] snapshot, error in
            guard let snapshot = snapshot else {
                print("error adding snapshot" + error.debugDescription)
                return
            }
            
            if let data = snapshot.data(),
                let profile = Profile(uid: snapshot.documentID, dictionary: data) {
                let profileViewModel = ProfileViewModel(profile: profile)
                self?.dataUpdate?(profileViewModel)
            }
        }
    }
    
    func stopListening(){
        observer?.remove()
    }
    
    deinit {
        stopListening()
    }
    
}
