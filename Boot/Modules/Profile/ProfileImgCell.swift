//
//  ProfileImgCell.swift
//  Boot
//
//  Created by Chris James on 17/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit
import Kingfisher

class ProfileImgCell: UITableViewHeaderFooterView {

    @IBOutlet weak var profileImgView: UIImageView!
    
    var profileImgUrl: URL? {
        didSet {
            profileImgView.kf.indicatorType = .activity
            profileImgView.kf.setImage(with: profileImgUrl)
        }
    }
    
}
