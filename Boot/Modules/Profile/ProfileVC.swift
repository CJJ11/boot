//
//  ProfileVC.swift
//  Boot
//
//  Created by Chris James on 12/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

protocol ProfileView: BaseView, List {
    var onCloseTapped: (() -> Void)? {get set}
    var onLogoutTapped: (() -> Void)? {get set}
}

class ProfileVC: UIViewController, ProfileView {
    
    var tableView: UITableView!
    let profileImgCellName = String(describing: ProfileImgCell.self)
    private var closeBtn: UIBarButtonItem!
    private var viewModel: ProfileViewModel?
    private var sections: [ProfileSection] = [.info, .logout]
    
    private var dataSource: ProfileDataSource?
    
    var onCloseTapped: (() -> Void)?
    var onLogoutTapped: (() -> Void)?
    
    enum ProfileSection {
        case info
        case logout
    }
    
    enum InfoRows: Int {
        case name = 0
        case email = 1
        case phone = 2
        case role = 3
        case minesite = 4
        
        var title: String {
            get {
                switch(self){
                case .name: return "Name"
                case .email: return "Email"
                case .phone: return "Phone"
                case .role: return "Role"
                case .minesite: return "Minesite"
                }
            }
        }
        
        static let count = 5
    }
    
    init(dataSource: ProfileDataSource){
        self.dataSource = dataSource
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Not implemeneted")
    }
    
    override func loadView(){
        super.loadView()
        addTableView(estimatedRowHeight: 44.0, style: .grouped)
        layoutTableView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        setupNavBar()
        setupTableView()
        addCloseBarButton()
        dataSource?.dataUpdate = {[weak self] viewModel in
            self?.viewModel = viewModel
            self?.tableView.reloadData()
//            self?.profileImg.kf.indicatorType = .activity
//            self?.profileImg.kf.setImage(with: viewModel.profileUrl)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.flashScrollIndicators()
        dataSource?.startListening()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        dataSource?.stopListening()
    }
    
    private func setupNavBar(){
        title = "Profile"
        navigationController?.navigationBar.isTranslucent = true
        extendedLayoutIncludesOpaqueBars = true
        view.backgroundColor = .groupTableViewBackground    
    }
    
    private func setupTableView(){
        tableView.dataSource = self
        tableView.delegate = self
        tableView.allowsSelection = true
        tableView.register(UINib(nibName: profileImgCellName, bundle: nil), forHeaderFooterViewReuseIdentifier: profileImgCellName)
        tableView.sectionHeaderHeight = UITableView.automaticDimension
    }
    
    private func addCloseBarButton(){
        let closeBtn = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(closeBtnTapped))
        navigationItem.rightBarButtonItem = closeBtn
        self.closeBtn = closeBtn
    }
    
    @objc private func closeBtnTapped(){
        onCloseTapped?()
    }
}

extension ProfileVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch (sections[indexPath.section]){
        case .info:
            tableView.deselectRow(at: indexPath, animated: true)
        case .logout:
            tableView.deselectRow(at: indexPath, animated: true)
            onLogoutTapped?()
        }
    }

}

extension ProfileVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch sections[section] {
        case .info: return InfoRows.count
        case .logout: return 1
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if sections[section] == .info {
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: profileImgCellName)
                as? ProfileImgCell ?? ProfileImgCell()
            header.profileImgUrl = viewModel?.profileUrl
            return header
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch sections[indexPath.section] {
            
        case .info:
            let cell = UITableViewCell(style: .value1, reuseIdentifier: "info")
            guard let infoRow = InfoRows(rawValue: indexPath.row) else {return cell}
            cell.textLabel?.text = infoRow.title
            
            switch infoRow {
            case .name:
                cell.detailTextLabel?.text =  viewModel?.name
            case .email:
                cell.detailTextLabel?.text = viewModel?.email
            case .phone:
                cell.detailTextLabel?.text =  viewModel?.phone
            case .role:
                cell.detailTextLabel?.text =  viewModel?.role
            case .minesite:
                cell.detailTextLabel?.text =  viewModel?.minesite
            }
            return cell
            
        case .logout:
            let cell = UITableViewCell(style: .default, reuseIdentifier: "logout")
            cell.accessoryType = .disclosureIndicator
            cell.textLabel?.text = "Sign out"
            cell.textLabel?.textColor = .red
            return cell
        }
    }
    
}

