//
//  ProfileViewModel.swift
//  Boot
//
//  Created by Chris James on 13/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation

struct ProfileViewModel {
    let name: String
    let email: String
    let phone: String
    let role: String
    let minesite: String
    let profileUrl: URL
}

extension ProfileViewModel: Equatable {
    
    init(profile: Profile){
        self.email = profile.email
        self.name = profile.name
        self.phone = profile.phone
        self.role = profile.role.description
        self.minesite = profile.minesite
        self.profileUrl = profile.profileImgUrl
    }
    
}
