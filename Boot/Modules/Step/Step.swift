//
//  Step.swift
//  Boot
//
//  Created by Chris James on 19/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import FirebaseFirestore.FIRTimestamp

struct Step: Equatable {
    let number: Int
    let description: String
    var checks: [Check]
    
    init?(number: Int, description: String, checks: [Check]){
        guard !checks.isEmpty else {return nil}
        self.number = number
        self.description = description
        self.checks = checks
    }

    //DB keys
    private static let checksKey = "checks"
    private static let descriptionKey = "desc"
}

extension Step {
    init?(uid: String, dictionary: [String: Any]){
        guard let number = Int(uid),
            let checksDict = dictionary[Step.checksKey] as? [String: Any],
            let description = dictionary[Step.descriptionKey] as? String else {return nil}
        
        var checks = [Check]()
        for (key, value) in checksDict {
            if let checkDict = value as? [String: Any],
                let check = Check(uid: key, dictionary: checkDict){
                checks.append(check)
            }
        }
        checks.sort(by: {$0.number < $1.number})

        self.init(number: number, description: description, checks: checks)
    }

    func toDictionary() -> [String: Any] {
        var checksDict = [String: Any]()
        
        for check in checks {
            checksDict[String(check.number)] = check.toDictionary()
        }
        
        return [
            Step.checksKey : checksDict as Any,
            Step.descriptionKey: description as Any
        ]
    }
    
    
    //Functions to alter the state of the step
    mutating func setCheckValue(checkNumber: Int, pass: Bool, userId: String, username: String){
        if checks.indices.contains(checkNumber){
            checks[checkNumber].setPass(pass: pass, userId: userId, username: username)
            //update step status???
        }
    }
    
    mutating func setCheckSliderValue(checkNumber: Int, sliderVal: Float?, userId: String, username: String){
        if checks.indices.contains(checkNumber){
            checks[checkNumber].setSliderVal(sliderVal: sliderVal, userId: userId, username: username)
        }
    }
    
    mutating func setCheckPickerValue(checkNumber: Int, pickerVal: Int?, userId: String, username: String){
        if checks.indices.contains(checkNumber){
            checks[checkNumber].setPickerVal(pickerVal: pickerVal, userId: userId, username: username)
        }
    }
    
    //Functions to get the status of the step
    func checksPass() -> Bool {
        return checks.filter({!$0.getPass()}).isEmpty
    }
    
    func noOfChecksPass() -> Int {
        return checks.filter({$0.getPass()}).count
    }
    
    func noOfChecks() -> Int {
        return checks.count
    }
    
    func checksStarted() -> Bool {
        return !checks.filter({$0.getStarted()}).isEmpty
    }
    
    func getStatus() -> StepStatus {
        if checksPass(){
            return .complete
        } else if checksStarted() {
            return .underway
        } else {
            return .notStarted
        }
    }
    
    func earliest() -> NamedTimestamp? {
        let timestamps = checks.compactMap({$0.getTimestamp()})
        
        guard let earliest = timestamps.min(by: {$0.dateValue() < $1.dateValue()}) else {
            return nil
        }
        
        guard let username = checks.first(where: {$0.getTimestamp() == earliest})?.getUsername() else {
            return nil
        }
        
        guard let uid = checks.first(where: {$0.getTimestamp() == earliest})?.getUserId() else {
            return nil
        }
        
        return NamedTimestamp(username: username, uid: uid, timestamp: earliest)
    }
    
    
    func latest() -> NamedTimestamp? {
        let timestamps = checks.compactMap({$0.getTimestamp()})
        
        guard let latest = timestamps.max(by: {$0.dateValue() < $1.dateValue()}) else {
            return nil
        }
        guard let username = checks.first(where: {$0.getTimestamp() == latest})?.getUsername() else {
            return nil
        }
        guard let uid = checks.first(where: {$0.getTimestamp() == latest})?.getUserId() else {
            return nil
        }
        
        return  NamedTimestamp(username: username, uid: uid, timestamp: latest)
    }
    
}
