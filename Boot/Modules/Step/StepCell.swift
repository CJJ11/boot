//
//  StepCell.swift
//  Boot
//
//  Created by Chris James on 19/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

class StepCell: UITableViewCell {

    @IBOutlet weak var iconImg: UIImageView!
    @IBOutlet weak var numberLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    
    var viewModel: StepViewModel? = nil {
        didSet {
            numberLbl.text = viewModel?.number
            titleLbl.text = viewModel?.title
            descriptionLbl.text = viewModel?.description
            iconImg.image = viewModel?.iconImg
        }
    }
}
