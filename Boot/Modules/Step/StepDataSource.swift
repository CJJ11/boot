//
//  StepDataSource.swift
//  Boot
//
//  Created by Chris James on 19/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import FirebaseFirestore

protocol StepDataSource: AnyObject {
    var dataUpdate: (([StepViewModel]) -> Void)? {get set}
    
    func startListening()
    func stopListening()
    
    func cancelInspection()
    func signOffInspection()
    
    func getFooterText(status: InspectionStatus) -> String?
}

class StepFirebaseDataSource: StepDataSource {
    var refreshing: ((Bool) -> Void)?
    var empty: ((Bool, Bool) -> Void)?
    
    var observer: ListenerRegistration?
    var reference: DocumentReference
    var dataUpdate: (([StepViewModel]) -> Void)?
    
    private let authSource: AuthSource
    var inspection: Inspection?
    
    
    init(authSource: AuthSource, inspection: Inspection){
        self.authSource = authSource
        self.inspection = inspection
        reference = Firestore.firestore()
            .collection("minesites")
            .document("cadia")
            .collection("inspections")
            .document(inspection.uid)
    }

    
    func startListening(){
        observer = reference.addSnapshotListener {[weak self] snapshot, error in
            guard let snapshot = snapshot else {
                print("error adding snapshot" + error.debugDescription)
                return
            }
            
            if let data = snapshot.data(),
                let inspection = Inspection(uid: snapshot.documentID, dictionary: data) {
                self?.inspection = inspection
                if let authSource = self?.authSource {
                    let viewModels = inspection.steps.map({StepViewModel(step: $0, authSource: authSource)})
                    self?.dataUpdate?(viewModels)
                }
            }
        }
    }
    
    func stopListening(){
        observer?.remove()
    }
    
    func cancelInspection() {
        guard let inspection = inspection,
            let uid = authSource.getUid(),
            let username = authSource.getName() else {return}
        
        let uploadDict = Inspection.cancel(
            inspection: inspection,
            uid: uid,
            username: username
        )
        reference.updateData(uploadDict)
    }
    
    func signOffInspection() {
        guard let inspection = inspection,
            let uid = authSource.getUid(),
            let username = authSource.getName() else {return}
        
        let uploadDict = Inspection.signOff(
            inspection: inspection,
            uid: uid,
            username: username
        )
        reference.updateData(uploadDict)
    }
    
    deinit {
        stopListening()
    }
    
    func getFooterText(status: InspectionStatus) -> String? {
        switch status {
        case .boot:
            return nil
            
        case .underway:
            return "Underway"
            
        case .cancelled:
            let timeSince = dateService.ds.timeSince(from: inspection?.cancelledTimestamp?.dateValue())
            let username = authSource.getName(
                uid: inspection?.cancelledSupervisorUid,
                username: inspection?.cancelledSupervisorUsername
            )
            return "Cancelled by \(username ?? "supervisor") \(timeSince)"
            
        case .complete:
            let timeSince = dateService.ds.timeSince(from: inspection?.signedOffTimestamp?.dateValue())
            let username = authSource.getName(
                uid: inspection?.signedOffSupervisorUid,
                username: inspection?.signedOffSupervisorUsername
            )
            return "Signed off by \(username ?? "supervisor") \(timeSince)"
            
        case .notStarted:
            let timeSince = dateService.ds.timeSince(from: inspection?.addedTimestamp?.dateValue())
            let username = authSource.getName(
                uid: inspection?.addedSupervisorUid,
                username: inspection?.addedSupervisorUsername
            )
            return "Added by \(username ?? "supervisor") \(timeSince)"
            
        case .signOff:
            return "Awaiting supervisor sign-off"
        }
    }
    
}
