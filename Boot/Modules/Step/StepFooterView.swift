//
//  StepFooterView.swift
//  Boot
//
//  Created by Chris James on 18/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

class StepFooterView: UITableViewHeaderFooterView {

    @IBOutlet weak var footerLbl: UILabel!
    
    var footerDescription: String? {
        didSet {
            footerLbl.text = footerDescription
        }
    }
    
}
