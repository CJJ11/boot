//
//  StepVC.swift
//  Boot
//
//  Created by Chris James on 19/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit
import Dwifft
import LocalAuthentication

protocol StepView: BaseView, List, BottomButtons {
    var onInspectionCancelled: (() -> Void)? {get set}
    var onInspectionSignedOff: (() -> Void)? {get set}
}

class StepVC: UIViewController, StepView {
    
    weak var tableView: UITableView!
    weak var emptyView: UIView!
    weak var refreshControl: UIRefreshControl!
    weak var cancelBtn: UIBarButtonItem?
    weak var bottomButtonView: BottomButtonView!
    
    private var dataSource: StepDataSource
    var onInspectionCancelled: (() -> Void)?
    var onInspectionSignedOff: (() -> Void)?
    
    private let inspection: Inspection
    private var diffCalculator: SingleSectionTableViewDiffCalculator<StepViewModel>?
    private let stepCellName = String(describing: StepCell.self)
    private let footerName = String(describing: StepFooterView.self)
    private let roleType: RoleType
    
    
    init(inspection: Inspection, dataSource: StepDataSource, roleType: RoleType){
        self.inspection = inspection
        self.dataSource = dataSource
        self.roleType = roleType
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Not implemented")
    }
    
    override func loadView() {
        super.loadView()
        
        addTableView(estimatedRowHeight: 50.0, style: .plain)
        
        if inspection.status == .signOff && roleType == .supervisor {
            addBottomButtons(primaryBtnTitle: "Sign Off Inspection", secondaryBtnTitle: nil)
            layoutTableView(bottomView: bottomButtonView)
            layoutButtomButons()
        } else {
            layoutTableView(bottomView: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = inspection.equipment
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.view.backgroundColor = .white
        extendedLayoutIncludesOpaqueBars = true
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: stepCellName, bundle: nil), forCellReuseIdentifier: stepCellName)
        tableView.register(UINib(nibName: footerName, bundle: nil), forHeaderFooterViewReuseIdentifier: footerName)
        tableView.allowsSelection = false
        tableView.tableFooterView = UIView()
        tableView.sectionHeaderHeight = 0
        tableView.showsHorizontalScrollIndicator = false
        tableView.backgroundColor = UIColor.groupTableViewBackground
        
        diffCalculator = SingleSectionTableViewDiffCalculator(tableView: tableView)
        diffCalculator?.deletionAnimation = .fade
        diffCalculator?.insertionAnimation = .fade
        
        dataSource.dataUpdate = {[weak self] (viewModels) in
            self?.diffCalculator?.rows = viewModels
        }
        
        if inspection.status == .signOff && roleType == .supervisor {
            bottomButtonView.primaryBtnTapped = {[weak self] in
                self?.signOffBtnTapped()
            }
        }
        
        if inspection.status != .cancelled && inspection.status != .complete && roleType == .supervisor {
            addRightBarButton()
        }
    }
    
    private func signOffBtnTapped(){
        let context = LAContext()
        
        // First check if we have the needed hardware support.
        var error: NSError?
        if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
            let reason = "Sign off the inspection"
            
            context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason ) { success, error in
                
                DispatchQueue.main.async { [weak self] in
                    if success {
                        self?.dataSource.signOffInspection()
                        self?.onInspectionSignedOff?()
                    } else {
                        print(error?.localizedDescription ?? "Failed to authenticate")
                        let alert = UIAlertController(
                            title: "Authentication error",
                            message: error?.localizedDescription,
                            preferredStyle: .alert
                        )
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self?.present(alert, animated: true, completion: nil)
                    }
                }
            }
        } else {
            print(error?.localizedDescription ?? "Can't evaluate policy")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        dataSource.startListening()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        dataSource.stopListening()
    }
    
    private func addRightBarButton(){
        let cancelBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "Delete"), style: .plain, target: self, action: #selector(cancelBtnTapped))
        cancelBtn.isEnabled = true
        navigationItem.rightBarButtonItem = cancelBtn
        self.cancelBtn = cancelBtn
    }
    
    @objc private func cancelBtnTapped(){
        let alert = UIAlertController(
            title: "Cancel Inspection",
            message: "Are you sure you want to set this inspection as cancelled?",
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "Back", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Set as Cancelled", style: .destructive, handler: {[weak self] (action) in
            self?.dataSource.cancelInspection()
            self?.onInspectionCancelled?()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension StepVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return diffCalculator?.rows.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = diffCalculator?.rows[indexPath.row] else {return UITableViewCell()}
        let cell = tableView.dequeueReusableCell(withIdentifier: stepCellName, for: indexPath)
            as? StepCell ?? StepCell()
        cell.viewModel = viewModel
        return cell
    }
    
}

extension StepVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard section == 0 else {return nil}
        
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: footerName) as? StepFooterView ?? StepFooterView()
        cell.footerDescription = dataSource.getFooterText(status: inspection.status)
        return cell
    }

}
