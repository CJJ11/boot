//
//  StepViewModel.swift
//  Boot
//
//  Created by Chris James on 19/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit
import FirebaseFirestore.FIRTimestamp

struct StepViewModel {
    let number: String
    let title: String
    let description: String
    let status: StepStatus
    let iconImg: UIImage
}
    
extension StepViewModel: Equatable {
    
    init(step: Step, authSource: AuthSource){
        self.number = String(step.number + 1) 
        self.title = step.description
        let status = step.getStatus()
        self.status = status
        self.iconImg = status.image

        switch status {
        
        case .notStarted:
            self.description = "Not started"
            
        case .underway:
            if let namedTimestamp = step.latest() {
                let timeSince = dateService.ds.timeSince(from: namedTimestamp.timestamp.dateValue())
                let username = authSource.getName(uid: namedTimestamp.uid, username: namedTimestamp.username)
                self.description = "Last edited by \(username ?? "") \(timeSince)"
            } else {
                self.description = "Underway"
            }
           
        case .complete:
            if let namedTimestamp = step.latest() {
                let timeSince = dateService.ds.timeSince(from: namedTimestamp.timestamp.dateValue())
                let username = authSource.getName(uid: namedTimestamp.uid, username: namedTimestamp.username)
                self.description = "Completed by \(username ?? "") \(timeSince)"
            } else {
                self.description = "Completed"
            }
        }
    }
    
    
}
