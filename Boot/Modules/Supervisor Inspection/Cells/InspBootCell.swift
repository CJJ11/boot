//
//  InspBootCell.swift
//  Boot
//
//  Created by Chris James on 27/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

protocol InspBootCellDelegate: class {
    func archiveBtnTapped()
}

class InspBootCell: UITableViewCell {
    
    weak var delegate: InspBootCellDelegate?
    
    @IBAction func archiveBtnTapped(_ sender: Any) {
        delegate?.archiveBtnTapped()    
    }
    
}
