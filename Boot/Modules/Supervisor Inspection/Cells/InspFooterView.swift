//
//  InspFooterView.swift
//  Boot
//
//  Created by Chris James on 26/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

protocol InspFooterViewDelegate: class {
    func changeBtnTapped()
}

class InspFooterView: UITableViewHeaderFooterView {

    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var changeBtn: UIButton!
    
    weak var delegate: InspFooterViewDelegate?
    
    
    var descriptionText: String? {
        didSet {
            descriptionLbl.text = descriptionText
        }
    }
    
    var buttonImage: UIImage? {
        didSet {
            changeBtn.setImage(buttonImage, for: .normal)
        }
    }

    @IBAction func changeBtnTapped(_ sender: UIButton) {
        delegate?.changeBtnTapped()
    }
    
}
