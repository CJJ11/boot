//
//  InspHoldUpCell.swift
//  Boot
//
//  Created by Chris James on 16/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

class InspHoldUpCell: UITableViewCell, InspectionCellProtocol {

    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    
    
    var viewModel: InspectionViewModel? = nil {
        didSet {
            locationLbl.text = viewModel?.equipment
            descriptionLbl.text = viewModel?.description
        }
    }
    
}
