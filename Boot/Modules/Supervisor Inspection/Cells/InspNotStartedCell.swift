//
//  InspNotStartedCell.swift
//  Boot
//
//  Created by Chris James on 16/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

class InspNotStartedCell: UITableViewCell, InspectionCellProtocol {

    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var timeEstimateLbl: UILabel!
    
    var viewModel: InspectionViewModel? {
        didSet {
            locationLbl.text = viewModel?.equipment
            timeEstimateLbl.text = viewModel?.timeRequired
        }
    }
    
}
