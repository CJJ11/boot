//
//  InspSignOff.swift
//  Boot
//
//  Created by Chris James on 17/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

class InspSignOffCell: UITableViewCell, InspectionCellProtocol {

    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    
    
    var viewModel: InspectionViewModel? {
        didSet {
            locationLbl.text = viewModel?.equipment
            descriptionLbl.text = viewModel?.description
        }
    }
    
}
