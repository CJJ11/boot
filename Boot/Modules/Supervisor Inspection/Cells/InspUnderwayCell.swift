//
//  InspUnderwayCell.swift
//  Boot
//
//  Created by Chris James on 16/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

class InspUnderwayCell: UITableViewCell, InspectionCellProtocol {

    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    
    var viewModel: InspectionViewModel? = nil {
        didSet {
            locationLbl.text = viewModel?.equipment
            descriptionLbl.text = viewModel?.description
            progressView.progress = viewModel?.progress ?? 0
        }
    }
    
    
}
