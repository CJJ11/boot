//
//  InspectionCellProtocol.swift
//  Boot
//
//  Created by Chris James on 16/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

protocol InspectionCellProtocol: UITableViewCell  {
    var viewModel: InspectionViewModel? {get set}
}
