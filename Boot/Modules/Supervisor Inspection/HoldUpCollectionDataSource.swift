//
//  HoldUpCollectionDataSource.swift
//  Boot
//
//  Created by Chris James on 28/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import FirebaseFirestore

protocol HoldUpCollectionDataSource: DataSource, AnyObject {
    var dataUpdate: (([HoldUpViewModel]) -> Void)? {get set}
    var viewModels: [HoldUpViewModel] {get}
    var reference: CollectionReference {get}
}

class HoldUpCollectionFirebaseDataSource: FirebaseCollectionDataSource<HoldUp, HoldUpViewModel>, HoldUpCollectionDataSource {
    
    let reference: CollectionReference
    
    init(authSource: AuthSource){
        reference = Firestore.firestore()
            .collection("minesites")
            .document("cadia")
            .collection("holdUps")
        super.init(query: reference, authSource: authSource)
    }
}
