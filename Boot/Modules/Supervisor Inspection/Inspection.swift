//
//  Inspection.swift
//  Boot
//
//  Created by Chris James on 07/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import FirebaseFirestore

struct Inspection: Document, Equatable {
    let uid: String
    
    //Inspection Info
    let equipment: String
    let location: String
    let locationIconUrl: URL
    let scope: String
    let timeRequired: String
    
    //Requirements
    let tools: String?
    let isolations: String?
    let competencies: String?
    let permits: String?
    let hazards: [Hazard]
    let ppe: [PPE]
    var status: InspectionStatus = .notStarted
    var steps: [Step]
    
    //Version Control Info
    let templateUid: String
    let version: Int
    let minAppVersion: Int
    let lastEditedBy: String
    let lastEditedDate: Timestamp
    
    //Status info
    var addedTimestamp: Timestamp?
    var addedSupervisorUid: String?
    var addedSupervisorUsername: String?
    
    var signedOffTimestamp: Timestamp?
    var signedOffSupervisorUid: String?
    var signedOffSupervisorUsername: String?
    
    var cancelledTimestamp: Timestamp?
    var cancelledSupervisorUid: String?
    var cancelledSupervisorUsername: String?
    
    var firstActionedTimestamp: Timestamp?
    var firstActionedUid: String?
    var firstActionedUsername: String?
    var firstActionedProfUrl: URL?
    
    var lastActionedTimestamp: Timestamp?
    var lastActionedUid: String?
    var lastActionedUsername: String?
    var lastActionedProfUrl: URL?
    
    //DB keys
    private static let equipmentKey = "equip"
    private static let locationKey = "loc"
    private static let locationIconKey = "locIcon"
    private static let statusKey = "status"
    private static let scopeKey = "scope"
    private static let timeRequiredKey = "time"
    private static let templateUidKey = "template"
    private static let lastEditedByKey = "editBy"
    private static let lastEditedDateKey = "editDate"
    private static let minAppVersionKey = "minApp"
    private static let versionKey = "ver"
    private static let uidKey = "uid"
    private static let toolsKey = "tools"
    private static let hazardsKey = "haz"
    private static let ppeKey = "ppe"
    private static let isolationsKey = "isol"
    private static let competenciesKey = "comp"
    private static let permitsKey = "permit"
    private static let stepsKey = "steps"
    
    private static let addedTimestampKey = "addedTs"
    private static let addedSupervisorUidKey = "addedUid"
    private static let addedSupervisorUsernameKey = "addedName"
    
    private static let signedOffTimestampKey = "signedOffTs"
    private static let signedOffSupervisorUidKey = "signedOffUid"
    private static let signedOffSupervisorUsernameKey = "signedOffName"
    
    private static let cancelledTimestampKey = "cancelTs"
    private static let cancelledSupervisorUidKey = "cancelUid"
    private static let cancelledSupervisorUsernameKey = "cancelName"
    
    private static let firstActionedTimestampKey = "firstActionedTs"
    private static let firstActionedUidKey = "firstActionedUid"
    private static let firstActionedUsernameKey = "firstActionedName"
    private static let firstActionedProfUrlKey = "firstActionedProfUrl"
    
    private static let lastActionedTimestampKey = "lastActionedTs"
    private static let lastActionedUidKey = "lastActionedUid"
    private static let lastActionedUsernameKey = "lastActionedName"
    private static let lastActionedProfUrlKey = "lastActionedProfUrl"
    
    
    static func getStatusKey() -> String {
        return Inspection.statusKey
    }
    
    static func getEquipmentKey() -> String {
        return Inspection.equipmentKey
    }
    
}

extension Inspection {
    init?(uid: String, dictionary: [String : Any]) {
        
        guard let statusRawValue = dictionary[Inspection.statusKey] as? Int,
            let status = InspectionStatus(rawValue: statusRawValue),
            let location = dictionary[Inspection.locationKey] as? String,
            let locationIconString = dictionary[Inspection.locationIconKey] as? String,
            let equipment = dictionary[Inspection.equipmentKey] as? String,
            let locationIconUrl = URL(string: locationIconString),
            let scope = dictionary[Inspection.scopeKey] as? String,
            let timeRequired = dictionary[Inspection.timeRequiredKey] as? String,
            let templateUid = dictionary[Inspection.templateUidKey] as? String,
            let lastEditedBy = dictionary[Inspection.lastEditedByKey] as? String,
            let lastEditedDate = dictionary[Inspection.lastEditedDateKey] as? Timestamp,
            let version = dictionary[Inspection.versionKey] as? Int,
            let minAppVersion = dictionary[Inspection.minAppVersionKey] as? Int,
            let stepsDict = dictionary[Inspection.stepsKey] as? [String : Any]
            else {return nil}
        
        var steps = [Step]()
        for (key, value) in stepsDict {
            if let stepDict = value as? [String: Any],
                let step = Step(uid: key, dictionary: stepDict) {
                steps.append(step)
            }
        }
        steps.sort(by: {$0.number < $1.number})
        guard !steps.isEmpty else {return nil}
        
        let tools = dictionary[Inspection.toolsKey] as? String
        let isolations = dictionary[Inspection.isolationsKey] as? String
        let competencies = dictionary[Inspection.competenciesKey] as? String
        let permits = dictionary[Inspection.permitsKey] as? String
        
        var hazards = [Hazard]()
        if let hazardsDict = dictionary[Inspection.hazardsKey] as? [String: Any] {
            for (key, value) in hazardsDict {
                if let hazardDict = value as? [String: Any],
                    let hazard = Hazard(uid: key, dictionary: hazardDict) {
                    hazards.append(hazard)
                }
            }
            hazards.sort(by: {$0.type.rawValue < $1.type.rawValue})
        }
        
        var ppeList = [PPE]()
        if let ppeDict = dictionary[Inspection.ppeKey] as? [String: Any] {
            for (key, value) in ppeDict {
                if let ppeDict = value as? [String: Any],
                    let ppe = PPE(uid: key, dictionary: ppeDict) {
                    ppeList.append(ppe)
                }
            }
            ppeList.sort(by: {$0.type.rawValue < $1.type.rawValue})
        }
        
        let addedTimestamp = dictionary[Inspection.addedTimestampKey] as? Timestamp
        let addedSupervisorUid = dictionary[Inspection.addedSupervisorUidKey] as? String
        let addedSupervisorUsername = dictionary[Inspection.addedSupervisorUsernameKey] as? String
        
        let signedOffTimestamp = dictionary[Inspection.signedOffTimestampKey] as? Timestamp
        let signedOffSupervisorUid = dictionary[Inspection.signedOffSupervisorUidKey] as? String
        let signedOffSupervisorUsername = dictionary[Inspection.signedOffSupervisorUsernameKey] as? String
        
        let cancelledTimestamp = dictionary[Inspection.cancelledTimestampKey] as? Timestamp
        let cancelledSupervisorUid = dictionary[Inspection.cancelledSupervisorUidKey] as? String
        let cancelledSupervisorUsername = dictionary[Inspection.cancelledSupervisorUsernameKey] as? String
        
        let firstActionedTimestamp = dictionary[Inspection.firstActionedTimestampKey] as? Timestamp
        let firstActionedUid = dictionary[Inspection.firstActionedUidKey] as? String
        let firstActionedUsername = dictionary[Inspection.firstActionedUsernameKey] as? String
        var firstActionedProfUrl: URL? = nil
        if let firstActionedProfUrlString = dictionary[Inspection.firstActionedProfUrlKey] as? String {
            firstActionedProfUrl = URL(string: firstActionedProfUrlString)
        }
        
        let lastActionedTimestamp = dictionary[Inspection.lastActionedTimestampKey] as? Timestamp
        let lastActionedUid = dictionary[Inspection.lastActionedUidKey] as? String
        let lastActionedUsername = dictionary[Inspection.lastActionedUsernameKey] as? String
        var lastActionedProfUrl: URL? = nil
        if let lastActionedProfUrlString = dictionary[Inspection.lastActionedProfUrlKey] as? String {
            lastActionedProfUrl = URL(string: lastActionedProfUrlString)
        }
        
        self.init(uid: uid,
                  equipment: equipment,
                  location: location,
                  locationIconUrl: locationIconUrl,
                  scope: scope,
                  timeRequired: timeRequired,
                  tools: tools,
                  isolations: isolations,
                  competencies: competencies,
                  permits: permits,
                  hazards: hazards,
                  ppe: ppeList,
                  status: status,
                  steps: steps,
                  
                  templateUid: templateUid,
                  version: version,
                  minAppVersion: minAppVersion,
                  lastEditedBy: lastEditedBy,
                  lastEditedDate: lastEditedDate,
                  
                  addedTimestamp: addedTimestamp,
                  addedSupervisorUid: addedSupervisorUid,
                  addedSupervisorUsername: addedSupervisorUsername,
                  
                  signedOffTimestamp: signedOffTimestamp,
                  signedOffSupervisorUid: signedOffSupervisorUid,
                  signedOffSupervisorUsername: signedOffSupervisorUsername,
                  
                  cancelledTimestamp: cancelledTimestamp,
                  cancelledSupervisorUid: cancelledSupervisorUid,
                  cancelledSupervisorUsername: cancelledSupervisorUsername,
                  
                  firstActionedTimestamp: firstActionedTimestamp,
                  firstActionedUid: firstActionedUid,
                  firstActionedUsername: firstActionedUsername,
                  firstActionedProfUrl: firstActionedProfUrl,
                  
                  lastActionedTimestamp: lastActionedTimestamp,
                  lastActionedUid: lastActionedUid,
                  lastActionedUsername: lastActionedUsername,
                  lastActionedProfUrl: lastActionedProfUrl
        )
    }
    
    init(
        equipment: String, location: String, locationIconUrl: URL,
        scope: String, timeRequired: String, tools: String,
        isolations: String, competencies: String, permits: String,
        hazards: [Hazard], ppeList: [PPE], steps: [Step]){
        
        let uid = NSUUID().uuidString
        self.init(uid: uid,
                  equipment: equipment,
                  location: location,
                  locationIconUrl: locationIconUrl,
                  scope: scope,
                  timeRequired: timeRequired,
                  tools: tools,
                  isolations: isolations,
                  competencies: competencies,
                  permits: permits,
                  hazards: hazards,
                  ppe: ppeList,
                  status: .notStarted,
                  steps: steps,
                  
                  templateUid: uid,
                  version: 1,
                  minAppVersion: 1,
                  lastEditedBy: "Admin",
                  lastEditedDate: Timestamp(date: Date()),
                  
                  addedTimestamp: nil,
                  addedSupervisorUid: nil,
                  addedSupervisorUsername: nil,
                  
                  signedOffTimestamp: nil,
                  signedOffSupervisorUid: nil,
                  signedOffSupervisorUsername: nil,
                  
                  cancelledTimestamp: nil,
                  cancelledSupervisorUid: nil,
                  cancelledSupervisorUsername: nil,
                  
                  firstActionedTimestamp: nil,
                  firstActionedUid: nil,
                  firstActionedUsername: nil,
                  firstActionedProfUrl: nil,
                  
                  lastActionedTimestamp: nil,
                  lastActionedUid: nil,
                  lastActionedUsername: nil,
                  lastActionedProfUrl: nil
        )
    }
    
    
    static func toDictionary(inspection: Inspection) -> [String: Any] {
        var stepsDict = [String: Any]()
        var ppeDict = [String: Any]()
        var hazardsDict = [String: Any]()
        
        for step in inspection.steps {
            stepsDict[String(step.number)] = step.toDictionary() as Any
        }
        
        for item in inspection.ppe {
            ppeDict[item.uid] = item.toDictionary() as Any
        }
        
        for hazard in inspection.hazards {
            hazardsDict[hazard.uid] = hazard.toDictionary() as Any
        }
        
        return
            [Inspection.hazardsKey: hazardsDict as Any,
             Inspection.ppeKey: ppeDict,
             Inspection.stepsKey : stepsDict as Any,
             
             Inspection.uidKey: inspection.uid as Any,
             Inspection.equipmentKey: inspection.equipment as Any,
             Inspection.statusKey: inspection.status.rawValue as Any,
             Inspection.locationKey: inspection.location as Any,
             Inspection.locationIconKey: inspection.locationIconUrl.absoluteString as Any,
             Inspection.timeRequiredKey: inspection.timeRequired as Any,
             Inspection.scopeKey: inspection.scope as Any,
             Inspection.lastEditedByKey: inspection.lastEditedBy as Any,
             Inspection.lastEditedDateKey: inspection.lastEditedDate as Any,
             Inspection.templateUidKey: inspection.templateUid as Any,
             Inspection.versionKey: inspection.version as Any,
             Inspection.minAppVersionKey: inspection.minAppVersion as Any,
             Inspection.toolsKey: inspection.tools as Any,
             Inspection.isolationsKey : inspection.isolations as Any,
             Inspection.competenciesKey: inspection.competencies as Any,
             Inspection.permitsKey: inspection.permits as Any,
             
             Inspection.addedTimestampKey: inspection.addedTimestamp as Any,
             Inspection.addedSupervisorUidKey: inspection.addedSupervisorUid as Any,
             Inspection.addedSupervisorUsernameKey: inspection.addedSupervisorUsername as Any,
             
             Inspection.signedOffTimestampKey: inspection.signedOffTimestamp as Any,
             Inspection.signedOffSupervisorUidKey: inspection.signedOffSupervisorUid as Any,
             Inspection.signedOffSupervisorUsernameKey: inspection.signedOffSupervisorUsername as Any,
             
             Inspection.cancelledTimestampKey: inspection.cancelledTimestamp as Any,
             Inspection.cancelledSupervisorUidKey: inspection.cancelledSupervisorUid as Any,
             Inspection.cancelledSupervisorUsernameKey: inspection.cancelledSupervisorUsername as Any,
             
             Inspection.firstActionedTimestampKey: inspection.firstActionedTimestamp as Any,
             Inspection.firstActionedUidKey: inspection.firstActionedUid as Any,
             Inspection.firstActionedUsernameKey: inspection.firstActionedUsername as Any,
             Inspection.firstActionedProfUrlKey: inspection.firstActionedProfUrl?.absoluteString as Any,
             
             Inspection.lastActionedTimestampKey: inspection.lastActionedTimestamp as Any,
             Inspection.lastActionedUidKey: inspection.lastActionedUid as Any,
             Inspection.lastActionedUsernameKey: inspection.lastActionedUsername as Any,
             Inspection.lastActionedProfUrlKey: inspection.lastActionedProfUrl?.absoluteString as Any
        ]
    }
    
    static func add(inspection: Inspection, uid: String, username: String) -> [String: Any] {
        var inspection = inspection
        inspection.addedSupervisorUid = uid
        inspection.addedSupervisorUsername = username
        inspection.addedTimestamp = Timestamp(date: Date())
        return Inspection.toDictionary(inspection: inspection)
    }
    
    static func setCheckValue(
        inspection: Inspection,
        stepNumber: Int,
        checkNumber: Int,
        pass: Bool,
        userId: String,
        username: String,
        profileUrl: URL) -> [String: Any]? {
        
        var inspection = inspection
        
        if inspection.steps.indices.contains(stepNumber){
            
            inspection.steps[stepNumber].setCheckValue(
                checkNumber: checkNumber,
                pass: pass,
                userId: userId,
                username: username)
            
            return updatedInspection(
                inspection: inspection,
                userId: userId,
                username: username,
                profileUrl: profileUrl)
        }
        return nil
    }
    
    static func setCheckSliderValue(
        inspection: Inspection,
        stepNumber: Int,
        checkNumber: Int,
        sliderVal: Float?,
        userId: String,
        username: String,
        profileUrl: URL) -> [String: Any]? {
        
        var inspection = inspection
        
        if inspection.steps.indices.contains(stepNumber){
            
            inspection.steps[stepNumber].setCheckSliderValue(
                checkNumber: checkNumber,
                sliderVal: sliderVal,
                userId: userId,
                username: username
            )
            
            return updatedInspection(
                inspection: inspection,
                userId: userId,
                username: username,
                profileUrl: profileUrl
            )
        }
        return nil
    }
    
    
    static func setCheckPickerValue(
        inspection: Inspection,
        stepNumber: Int,
        checkNumber: Int,
        pickerVal: Int?,
        userId: String,
        username: String,
        profileUrl: URL) -> [String: Any]? {
        
        var inspection = inspection
        
        if inspection.steps.indices.contains(stepNumber){
            
            inspection.steps[stepNumber].setCheckPickerValue(
                checkNumber: checkNumber,
                pickerVal: pickerVal,
                userId: userId,
                username: username
            )
            return updatedInspection(
                inspection: inspection,
                userId: userId,
                username: username,
                profileUrl: profileUrl
            )
        }
        return nil
    }
    
    private static func updatedInspection(inspection: Inspection, userId: String,
                                          username: String, profileUrl: URL) -> [String: Any] {
        
        var inspection = inspection
        inspection.lastActionedTimestamp = Timestamp(date: Date())
        inspection.lastActionedUid = userId
        inspection.lastActionedUsername = username
        inspection.lastActionedProfUrl = profileUrl
        
        if inspection.steps.filter({!$0.checksPass()}).isEmpty {
            inspection.status = .signOff
        } else {
            if inspection.status == .notStarted {
                inspection.firstActionedTimestamp = Timestamp(date: Date())
                inspection.firstActionedUid = userId
                inspection.firstActionedUsername = username
                inspection.firstActionedProfUrl = profileUrl
            }
            inspection.status = .underway
        }
        return Inspection.toDictionary(inspection: inspection)
    }
    
    
    static func signOff(inspection: Inspection, uid: String, username: String)  -> [String: Any] {
        //guard status == .signOff else {return}
        var inspection = inspection
        inspection.status = .complete
        inspection.signedOffSupervisorUid = uid
        inspection.signedOffSupervisorUsername = username
        inspection.signedOffTimestamp = Timestamp(date: Date())
        return Inspection.toDictionary(inspection: inspection)
    }
    
    static func cancel(inspection: Inspection, uid: String, username: String) -> [String: Any] {
        var inspection = inspection
        inspection.status = .cancelled
        inspection.cancelledSupervisorUid = uid
        inspection.cancelledSupervisorUsername = username
        inspection.cancelledTimestamp = Timestamp(date: Date())
        return Inspection.toDictionary(inspection: inspection)
    }
    
    
    func latest() -> NamedTimestamp? {
        let namedTimestamps = steps.compactMap({$0.latest()})
        return namedTimestamps.max(by: {$0.timestamp.dateValue() < $1.timestamp.dateValue()})
    }
    
    func earliest() -> NamedTimestamp? {
        let namedTimestamps = steps.compactMap({$0.earliest()})
        return namedTimestamps.min(by: {$0.timestamp.dateValue() < $1.timestamp.dateValue()})
    }
    
    func progress() -> Float {
        if steps.isEmpty {return 0.0}
        let numberOfChecksPass = Float(steps.map({$0.noOfChecksPass()}).reduce(0, +))
        let numberOfChecks = Float(steps.map({$0.noOfChecks()}).reduce(0, +))
        return numberOfChecksPass / numberOfChecks
    }
    
}
