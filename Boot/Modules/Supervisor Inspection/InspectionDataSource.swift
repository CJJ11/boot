//
//  StepModelController.swift
//  Boot
//
//  Created by Chris James on 07/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import FirebaseFirestore


protocol InspectionDataSource: DataSource, AnyObject {
    var dataUpdate: (([InspectionViewModel]) -> Void)? {get set}
    var viewModels: [InspectionViewModel] {get}
    var reference: CollectionReference {get}
}

class InspectionFirebaseDataSource: FirebaseCollectionDataSource<Inspection, InspectionViewModel>, InspectionDataSource {

    let reference: CollectionReference
    
    init(authSource: AuthSource){
        reference = Firestore.firestore()
            .collection("minesites")
            .document("cadia")
            .collection("inspections")
        super.init(query: reference, authSource: authSource)
    }
}
