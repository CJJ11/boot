//
//  StepViewController.swift
//  Boot
//
//  Created by Chris James on 07/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit
import Dwifft
import TransitionButton

protocol InspectionView: BaseView, RefreshingList {
    var onInspectionSelect: ((InspectionViewModel) -> ())? { get set }
    var onAddInspectionTapped: (() -> Void)? {get set}
}


class InspectionVC: CustomTransitionViewController, InspectionView {
    
    weak var tableView: UITableView!
    weak var emptyView: UIView!
    weak var refreshControl: UIRefreshControl!
    weak var addInspectionBtn: UIBarButtonItem?
    
    private lazy var headerView: TableViewHeader = initHeader()
    private lazy var footerView: TableViewFooter = initFooter()
    
    var dataSource: SupervisorInspDataSource
    private var firstLoad: Bool = true
    private let footerViewName = String(describing: InspFooterView.self)
    
    private var diffCalculator: TableViewDiffCalculator<InspectionStatus, InspectionViewModel>?
    private var cellHeights = [IndexPath : CGFloat]()
    private var headerHeights = [Int : CGFloat]()
    private var footerHeights = [Int : CGFloat]()
    
    var onInspectionSelect: ((InspectionViewModel) -> ())?
    var onAddInspectionTapped: (() -> Void)?
    
    
    init(dataSource: SupervisorInspDataSource){
        self.dataSource = dataSource
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Not Implemented")
    }
    
    override func loadView(){
        super.loadView()
        
        addViews(estimatedRowHeight: 60.0,
                 emptyImage: #imageLiteral(resourceName: "ListAdd"),
                 emptyText: "Hit the + button in the top corner to add an inspection")
        layoutViews()
        addRightBarButton()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Inspections"
        
        tableView.delegate = self
        tableView.dataSource = self
        //tableView.sectionHeaderHeight = UITableView.automaticDimension
        
        InspectionStatus.allCases.forEach {
            tableView.register(
                UINib(nibName: $0.cell, bundle: nil),
                forCellReuseIdentifier: $0.cell
            )
        }
        
        tableView.register(
            UINib(nibName: footerViewName, bundle: nil),
            forHeaderFooterViewReuseIdentifier: footerViewName
        )
        
        diffCalculator = TableViewDiffCalculator(tableView: tableView)
        diffCalculator?.deletionAnimation = .fade
        diffCalculator?.insertionAnimation = .fade
        
        dataSource.empty = updateEmpty
        dataSource.refreshing = updateRefreshing
        
        dataSource.dataUpdate = {[weak self] (viewModels) in
                self?.diffCalculator?.sectionedValues = SectionedValues(
                    values: viewModels,
                    valueToSection: {$0.status},
                    sortSections: {$0.rawValue < $1.rawValue},
                    sortValues: {$0.location < $1.location})
            
        }
        
        dataSource.showBootHeader = {[weak self] (show) in
            self?.showHeaderView(show)
        }
        
        dataSource.showFooter = {[weak self] (show) in
            self?.showFooterView(show)
        }
    }
    
    private func initHeader() -> TableViewHeader{
        let headerView = TableViewHeader()
        headerView.archiveBtnTapped = {[weak self] in
            let alert = UIAlertController(
                title: "Archive Start-Up",
                message: "This will archive and clear all inspections and hold-ups from this plant start-up.",
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "Archive", style: .default, handler: { (action) in
                self?.dataSource.archive()
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self?.present(alert, animated: true, completion: nil)
        }
        return headerView
    }
    
    private func initFooter() -> TableViewFooter {
        let footerView = TableViewFooter(description: "Current supervisor is you", btnImage: #imageLiteral(resourceName: "Edit"))
        footerView.footerBtnTapped = {[weak self] in
            let alert = UIAlertController(
                title: "Supervisor Change",
                message: "This would change the supervisor in charge of the plant start-up.  Feature locked in demo mode.",
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self?.present(alert, animated: true, completion: nil)
        }
        return footerView
    }
    
    private func showHeaderView(_ show: Bool) {
        DispatchQueue.main.async {
            self.tableView.beginUpdates()
            let currentlyShowing = self.tableView.tableHeaderView != nil
            
            if show && !currentlyShowing {
                self.tableView.tableHeaderView = self.headerView
                self.sizeHeaderToFit()
                
            } else if !show && currentlyShowing {
                self.tableView.tableHeaderView = nil
                self.sizeHeaderToFit()
            }
            
            self.tableView.endUpdates()
        }
    }
    
    
    private func showFooterView(_ show: Bool) {
        DispatchQueue.main.async {
            self.tableView.beginUpdates()
            let currentlyShowing = self.tableView.tableFooterView != nil
            
            if show && !currentlyShowing {
                self.tableView.tableFooterView = self.footerView
                self.sizeFooterToFit()
                
            } else if !show && currentlyShowing {
                self.tableView.tableFooterView = nil
                self.sizeFooterToFit()
            }
            
            self.tableView.endUpdates()
        }
    }
    
    private func sizeHeaderToFit() {
        if let headerView = tableView.tableHeaderView {
            headerView.setNeedsLayout()
            headerView.layoutIfNeeded()
            
            let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            var frame = headerView.frame
            frame.size.height = height
            headerView.frame = frame
            
            tableView.tableHeaderView = headerView
        }
    }
    
    private func sizeFooterToFit() {
        if let footerView = tableView.tableFooterView {
            footerView.setNeedsLayout()
            footerView.layoutIfNeeded()
            
            let height = footerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            var frame = footerView.frame
            frame.size.height = height
            footerView.frame = frame
            
            tableView.tableFooterView = footerView
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if firstLoad {
            dataSource.startListening()
            firstLoad = false
        } else {
            dataSource.resumeListening()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        dataSource.stopListening()
    }
    
    func pullToRefresh(){
        dataSource.pullToRefresh()
        print("Refresh called")
    }
    
    private func addRightBarButton(){
        let addInspectionBtn = UIBarButtonItem(
            barButtonSystemItem: .add,
            target: self,
            action: #selector(addInspectionBtnTapped)
        )
        addInspectionBtn.isEnabled = true
        navigationItem.rightBarButtonItem = addInspectionBtn
        self.addInspectionBtn = addInspectionBtn
    }
    
    @objc private func addInspectionBtnTapped(){
        onAddInspectionTapped?()
    }

}


extension InspectionVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let viewModel = diffCalculator?.value(atIndexPath: indexPath){
            if viewModel.status != .boot {
                onInspectionSelect?(viewModel)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.size.height
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath] ?? 60
    }
    
}


extension InspectionVC: UITableViewDataSource {
    
    func numberOfSections(in: UITableView) -> Int {
        return diffCalculator?.numberOfSections() ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return diffCalculator?.numberOfObjects(inSection: section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = diffCalculator?.value(atIndexPath: indexPath) else {return UITableViewCell()}
        
        let reuseIdentifier = viewModel.status.cell
        let cell: InspectionCellProtocol
        
        switch viewModel.status {
        case .boot:
            let bootCell = tableView.dequeueReusableCell(
                withIdentifier: reuseIdentifier,
                for: indexPath) as? InspBootCell ?? InspBootCell()
            bootCell.delegate = self
            return bootCell
            
        case .notStarted:
            cell = tableView.dequeueReusableCell(
                withIdentifier: reuseIdentifier,
                for: indexPath) as? InspNotStartedCell ?? InspNotStartedCell()
            
        case .underway:
            cell = tableView.dequeueReusableCell(
                withIdentifier: reuseIdentifier,
                for: indexPath) as? InspUnderwayCell ?? InspUnderwayCell()
            
        case .complete:
            cell = tableView.dequeueReusableCell(
                withIdentifier: reuseIdentifier,
                for: indexPath) as? InspCompleteCell ?? InspCompleteCell()
            
        case .cancelled:
            cell = tableView.dequeueReusableCell(
                withIdentifier: reuseIdentifier,
                for: indexPath) as? InspCancelledCell ?? InspCancelledCell()
            
        case .signOff:
            cell = tableView.dequeueReusableCell(
                withIdentifier: reuseIdentifier,
                for: indexPath) as? InspSignOffCell ?? InspSignOffCell()
            
        }
        cell.viewModel = viewModel
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return diffCalculator?.value(forSection: section).title
    }
    
}


extension InspectionVC: InspFooterViewDelegate {
    
    func changeBtnTapped() {
        let alert = UIAlertController(
            title: "Supervisor Change",
            message: "This would change the supervisor in charge of the plant start-up.  Feature locked in demo mode.",
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension InspectionVC: InspBootCellDelegate {
    
    func archiveBtnTapped() {
        dataSource.archive()
    }
    
}

