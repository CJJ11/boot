//
//  InspectionViewModel.swift
//  Boot
//
//  Created by Chris James on 07/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

struct InspectionViewModel: ViewModel {
    let inspection: Inspection?
    let uid: String
    let equipment: String
    let location: String
    let locationIconUrl: URL?
    let timeRequired: String
    let status: InspectionStatus
    let description: String
    let progress: Float
}

extension InspectionViewModel: Equatable {
    
    static func == (lhs: InspectionViewModel, rhs: InspectionViewModel) -> Bool {
        return lhs.uid == rhs.uid &&
            lhs.status == rhs.status &&
            lhs.description == rhs.description &&
            lhs.locationIconUrl == rhs.locationIconUrl &&
            lhs.progress == rhs.progress
    }
    
    init?(document: Document, authSource: AuthSource){
        guard let inspection = document as? Inspection else {return nil}
        self.inspection = inspection
        self.uid = inspection.uid
        self.equipment = inspection.equipment
        self.location = inspection.location
        self.locationIconUrl = inspection.locationIconUrl
        self.status = inspection.status
        self.timeRequired = "Est. time to complete " + inspection.timeRequired
        self.progress = inspection.progress()
        
        var description: String = ""
        
        switch(inspection.status){
        case .cancelled:
            let timeSince = dateService.ds.timeSince(from: inspection.cancelledTimestamp?.dateValue())
            let username = authSource.getName(
                uid: inspection.cancelledSupervisorUid,
                username: inspection.cancelledSupervisorUsername
            )
            description = "Cancelled by \(username ?? "supervisor") \(timeSince)"
            
        case .complete:
            let timeSince = dateService.ds.timeSince(from: inspection.signedOffTimestamp?.dateValue())
            let username = authSource.getName(
                uid: inspection.signedOffSupervisorUid,
                username: inspection.signedOffSupervisorUsername
            )
            description = "Signed off by \(username ?? "supervisor") \(timeSince)"
            
        case .boot:
            description = ""
            
        case .notStarted:
            description = "Est. time required is " + inspection.timeRequired
            
        case .signOff:
            if let namedTimestamp = inspection.latest() {
                let timeSince = dateService.ds.timeSince(from: namedTimestamp.timestamp.dateValue())
                let username = authSource.getName(uid: namedTimestamp.uid, username: namedTimestamp.username)
                description = "Completed by \(username ?? "") \(timeSince)"
            }
        case .underway:
            if let namedTimestamp = inspection.latest() {
                let timeSince = dateService.ds.timeSince(from: namedTimestamp.timestamp.dateValue())
                let username = authSource.getName(uid: namedTimestamp.uid, username: namedTimestamp.username)
                description = "Actioned by \(username ?? "") \(timeSince)"
            }
        }
        
        self.description = description
    }
    
    static func getBoot() -> InspectionViewModel {
        return InspectionViewModel(
            inspection: nil,
            uid: "",
            equipment: "",
            location: "",
            locationIconUrl: nil,
            timeRequired: "",
            status: .boot,
            description: "",
            progress: 0.0
        )
    }
    

    
}


