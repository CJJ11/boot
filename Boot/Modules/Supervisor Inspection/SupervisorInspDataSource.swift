//
//  SupervisorInspectionDataSource.swift
//  Boot
//
//  Created by Chris James on 28/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import FirebaseFirestore


protocol SupervisorInspDataSource: DataSource, AnyObject {
    var dataUpdate: (([InspectionViewModel]) -> Void)? {get set}
    var showBootHeader: ((Bool) -> Void)? {get set}
    var showFooter: ((Bool) -> Void)? {get set}
    func archive()
}

class SupervisorInspFirebaseDataSource: SupervisorInspDataSource {

    private var inspectionDataSource: InspectionDataSource
    private var holdUpDataSource: HoldUpCollectionDataSource
    
    private var holdUpsComplete: Bool = false
    private var inspectionsComplete: Bool = false
    private let bootViewModel = InspectionViewModel.getBoot()
    
    var dataUpdate: (([InspectionViewModel]) -> Void)?
    var showBootHeader: ((Bool) -> Void)?
    var showFooter: ((Bool) -> Void)?
    var refreshing: ((Bool) -> Void)?
    var empty: ((Bool, Bool) -> Void)?
    
    
    init(authSource: AuthSource){
        inspectionDataSource = InspectionFirebaseDataSource(authSource: authSource)
        holdUpDataSource = HoldUpCollectionFirebaseDataSource(authSource: authSource)
        setupCallbacks()
    }
    
    private func setupCallbacks(){
        inspectionDataSource.empty = {[weak self] (isEmpty, animate) in
            self?.empty?(isEmpty, animate)
            self?.showFooter?(!isEmpty)
        }
        
        inspectionDataSource.refreshing = {[weak self] (isRefreshing) in
            self?.refreshing?(isRefreshing)
        }
        
        inspectionDataSource.dataUpdate = {[weak self] (viewModels) in
            self?.inspectionsComplete = !viewModels.isEmpty && viewModels.filter({$0.status != .complete && $0.status != .cancelled}).isEmpty
            self?.sendDataUpdate()
        }
        
        holdUpDataSource.dataUpdate = {[weak self] (viewModels) in
            self?.holdUpsComplete = viewModels.filter({$0.status == .open}).isEmpty
            self?.sendDataUpdate()
        }
    }
    
    private func sendDataUpdate(){
        showBootHeader?(holdUpsComplete && inspectionsComplete)
        dataUpdate?(inspectionDataSource.viewModels)
    }
    
    func startListening() {
        inspectionDataSource.startListening()
        holdUpDataSource.startListening()
    }
    
    func stopListening() {
        inspectionDataSource.stopListening()
        holdUpDataSource.stopListening()
    }
    
    func resumeListening() {
        inspectionDataSource.resumeListening()
        holdUpDataSource.resumeListening()
    }
    
    func pullToRefresh() {
        inspectionDataSource.pullToRefresh()
        holdUpDataSource.pullToRefresh()
    }
    
    func archive() {
        //Just a hack for the demo, delete for now instead of archive
        
        let db = Firestore.firestore()
        let batch = db.batch()
        
        inspectionDataSource.viewModels.forEach({
            batch.deleteDocument(inspectionDataSource.reference.document($0.uid))
        })
        
        batch.commit() {err in
            if let err = err {
                print("Delete error: \(err.localizedDescription)")
            } else {
                print("Deleted successfully")
            }
        }
    }
    
}
