//
//  SupervisorTabBarController.swift
//  Boot
//
//  Created by Chris James on 11/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

protocol SupervisorTabBarView: BaseView {
    var onInspectionFlowSelect: ((UINavigationController) -> ())? { get set }
    var onHoldUpFlowSelect: ((UINavigationController) -> ())? { get set }
    var onNotificationFlowSelect: ((UINavigationController) -> ())? { get set }
    
    func showTab(tab: SupervisorTab)
}

enum SupervisorTab: Int {
    case inspections = 0
    case holdUps = 1
    case notifications = 2
}

final class SupervisorTabBarController: UITabBarController, SupervisorTabBarView {
    
    var onInspectionFlowSelect: ((UINavigationController) -> ())?
    var onHoldUpFlowSelect: ((UINavigationController) -> ())?
    var onNotificationFlowSelect: ((UINavigationController) -> ())?

    weak var dataSource: TabBarDataSource?
    
    private let tabs: [SupervisorTab] = [.inspections, .holdUps, .notifications]

    
    init(viewControllers: [UIViewController], dataSource: TabBarDataSource){
        self.dataSource = dataSource
        super.init(nibName: nil, bundle: nil)
        self.viewControllers = viewControllers
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Not implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        setBadgeValueListener()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        dataSource?.startListening()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        dataSource?.stopListening()
    }
    
    private func setBadgeValueListener(){
        dataSource?.unseenNotiCount = {[weak self] (count) in
            var badgeValue: String? = nil
            if count != 0 {
                badgeValue = String(count)
            }
            
            UIApplication.shared.applicationIconBadgeNumber = count
            
            self?.tabBar.items?[SupervisorTab.notifications.rawValue].badgeValue = badgeValue
        }
    }
    
    func showTab(tab: SupervisorTab) {
        if let index = tabs.index(of: tab){
            self.selectedIndex = index
            runFlow()
        }
    }
    
    private func runFlow(){
        guard let controller = viewControllers?[selectedIndex] as? UINavigationController,
            let tab = SupervisorTab(rawValue: selectedIndex) else {return}
        switch tab {
        case .inspections:
            onInspectionFlowSelect?(controller)
        case .holdUps:
            onHoldUpFlowSelect?(controller)
        case .notifications:
            onNotificationFlowSelect?(controller)
        }
    }
    
}

extension SupervisorTabBarController: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        runFlow()
    }
    
}
