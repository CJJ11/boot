//
//  TechInspectionCell.swift
//  Boot
//
//  Created by Chris James on 29/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit
import Kingfisher

class TechInspectionCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    
    
    var viewModel: InspectionViewModel? = nil {
        didSet {
            imgView.kf.indicatorType = .activity
            imgView.kf.setImage(with: viewModel?.locationIconUrl)
            titleLbl.text = viewModel?.equipment
            descriptionLbl.text = viewModel?.description
        }
    }

    
}
