//
//  TechInspectionDataSource.swift
//  Boot
//
//  Created by Chris James on 29/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import FirebaseFirestore


protocol TechInspectionDataSource: DataSource, AnyObject {
    var dataUpdate: (([InspectionViewModel]) -> Void)? {get set}
    var count: ((InspectionStatus, Int) -> Void)? {get set}
    func toggleViewModels(type: InspectionStatus)
}

class TechInspectionFirebaseDataSource:
    DataSourceAggregate<Inspection, InspectionViewModel, InspectionStatus>, TechInspectionDataSource {
    
    init(reference: CollectionReference, authSource: AuthSource) {
        let types: [InspectionStatus] = [.notStarted, .underway, .signOff]
        
        super.init(reference: reference,
                   authSource: authSource,
                   types: types,
                   firstSelectedType: .notStarted,
                   filterDatabaseKey: Inspection.getStatusKey(),
                   sortDatabaseKey: Inspection.getEquipmentKey(),
                   sortDescending: false)
    }
    
}
