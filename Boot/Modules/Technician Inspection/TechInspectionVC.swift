//
//  TechInspectionVC.swift
//  Boot
//
//  Created by Chris James on 29/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit
import Dwifft
import TransitionButton

protocol TechInspectionView: BaseView, RefreshingList {
    var onInspectionSelect: ((InspectionViewModel) -> ())? {get set}
}

class TechInspectionVC: CustomTransitionViewController, TechInspectionView {

    weak var tableView: UITableView!
    weak var emptyView: UIView!
    weak var refreshControl: UIRefreshControl!
    
    var onInspectionSelect: ((InspectionViewModel) -> ())?
    
    private let segments: [InspectionStatus] = [.notStarted, .underway, .signOff]
    private let cellName = String(describing: TechInspectionCell.self)
    private let footerViewName = String(describing: InspFooterView.self)
    private var segmentedControl: UISegmentedControl!
    
    private var dataSource: TechInspectionDataSource
    private var firstLoad: Bool = true
    var diffCalculator: SingleSectionTableViewDiffCalculator<InspectionViewModel>?
    
    init(dataSource: TechInspectionDataSource){
        self.dataSource = dataSource
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Not implemented")
    }
    
    override func loadView(){
        super.loadView()
        addViews(estimatedRowHeight: 70.0,
                 emptyImage: segments[0].techEmptyImage,
                 emptyText: segments[0].techEmptyText,
                 style: .plain)
        layoutViews()
        addSegmentedControl()
    }
    
    private func addSegmentedControl(){
        let segmentedControl = UISegmentedControl(items: segments.map({segmentTitle(status: $0)}))
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.addTarget(self, action: #selector(segmentedControlChanged(_:)), for: .valueChanged)
        navigationItem.titleView = segmentedControl
        self.segmentedControl = segmentedControl
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Inspections"
        extendedLayoutIncludesOpaqueBars = true
        view.backgroundColor = .white

        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: cellName, bundle: nil), forCellReuseIdentifier: cellName)
        tableView.register(UINib(nibName: footerViewName, bundle: nil),
                           forHeaderFooterViewReuseIdentifier: footerViewName)
        tableView.backgroundColor = UIColor.groupTableViewBackground
        tableView.tableFooterView = UIView()
        
        diffCalculator = SingleSectionTableViewDiffCalculator(tableView: tableView)
        diffCalculator?.deletionAnimation = .top
        diffCalculator?.insertionAnimation = .top
        
        dataSource.empty = updateEmpty
        dataSource.refreshing = updateRefreshing
        
        dataSource.dataUpdate = {[weak self] (viewModels) in
            self?.diffCalculator?.rows = viewModels
        }
        
        dataSource.count = {[weak self] (status, count) in
            if let index = self?.segments.firstIndex(of: status),
                let title = self?.segmentTitle(status: status, count: count){
                self?.segmentedControl.setTitle(title, forSegmentAt: index)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if firstLoad {
            dataSource.startListening()
            firstLoad = false
        } else {
            dataSource.resumeListening()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        dataSource.stopListening()
    }
    
    func pullToRefresh() {
        dataSource.pullToRefresh()
        print("Pull to refresh")
    }
    
    @objc func segmentedControlChanged(_ sender: UISegmentedControl){
        let status = segments[sender.selectedSegmentIndex]
        updateEmptyView(status: status)
        dataSource.toggleViewModels(type: status)
    }
    
    private func updateEmptyView(status: InspectionStatus){
        (emptyView as! EmptyView).emptyLbl.text = status.techEmptyText
        (emptyView as! EmptyView).emptyImg.image = status.techEmptyImage
    }

    private func segmentTitle(status: InspectionStatus, count: Int = 0) -> String {
        return status.techDesc + " (\(count))"
    }
    
}

extension TechInspectionVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = diffCalculator?.rows[indexPath.row] else {return UITableViewCell()}
        let cell = tableView.dequeueReusableCell(withIdentifier: cellName, for: indexPath)
            as? TechInspectionCell ?? TechInspectionCell()
        cell.viewModel = viewModel
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return diffCalculator?.rows.count ?? 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
}

extension TechInspectionVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let viewModel = diffCalculator?.rows[indexPath.row] {
            onInspectionSelect?(viewModel)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: footerViewName)
            as? InspFooterView ?? InspFooterView()
        cell.descriptionText = "Current supervisor is Terry Pratchett"
        cell.buttonImage = #imageLiteral(resourceName: "Phone")
        cell.delegate = self
        return cell
    }
    
}

extension TechInspectionVC: InspFooterViewDelegate {
    
    func changeBtnTapped() {
        let alert = UIAlertController(
            title: "Call Supervisor",
            message: "This would call the supervisor in charge of the plant start-up.  Feature locked in demo mode.",
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}

