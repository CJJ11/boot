//
//  TechnicianTabBarDataSource.swift
//  Boot
//
//  Created by Chris James on 06/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import FirebaseFirestore

protocol TabBarDataSource: class {
    func startListening()
    func stopListening()
    var unseenNotiCount: ((Int) -> Void)? {get set}
}

class TabBarFirebaseDataSource: TabBarDataSource{
    
    private var observer: ListenerRegistration?
    private let reference: DocumentReference
    
    var unseenNotiCount: ((Int) -> Void)?
    
    init?(authSource: AuthSource){
        guard let uid = authSource.getUid() else {return nil}
        reference = Firestore.firestore()
            .collection("minesites")
            .document("cadia")
            .collection("users")
            .document(uid)
    }
    
    func startListening(){
        stopListening()
        observer = reference.addSnapshotListener {[weak self] snapshot, error in
            guard let snapshot = snapshot else {
                print("error adding snapshot" + error.debugDescription)
                return
            }
            
            if let data = snapshot.data(),
                let user = Profile(uid: snapshot.documentID, dictionary: data) {
                self?.unseenNotiCount?(user.unseenNotiCount)
            }
        }
    }
    
    func stopListening(){
        observer?.remove()
    }
    
    deinit {
        stopListening()
    }
    
}
