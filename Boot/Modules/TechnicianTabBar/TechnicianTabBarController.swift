//
//  TechnicianTabBarController.swift
//  Boot
//
//  Created by Chris James on 24/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

protocol TechnicianTabBarView: BaseView {
    var onInspectionFlowSelect: ((UINavigationController) -> ())? { get set }
    var onHoldUpFlowSelect: ((UINavigationController) -> ())? { get set }
    var onNotificationFlowSelect: ((UINavigationController) -> ())? { get set }
}

final class TechnicianTabBarController: UITabBarController, TechnicianTabBarView {
    
    var onInspectionFlowSelect: ((UINavigationController) -> ())?
    var onHoldUpFlowSelect: ((UINavigationController) -> ())?
    var onNotificationFlowSelect: ((UINavigationController) -> ())?
    
    weak var dataSource: TabBarDataSource?
    
    enum TechnicianTab: Int {
        case inspections = 0
        case holdUps = 1
        case notifications = 2
    }
    
    private let tabs: [TechnicianTab] = [.inspections, .holdUps]

    
    init(viewControllers: [UIViewController], dataSource: TabBarDataSource){
        self.dataSource = dataSource
        super.init(nibName: nil, bundle: nil)
        self.viewControllers = viewControllers
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Not implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        //setBadgeValueListener()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        dataSource?.startListening()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        dataSource?.stopListening()
    }
    
    private func setBadgeValueListener(){
        dataSource?.unseenNotiCount = {[weak self] (count) in
            var badgeValue: String? = nil
            if count != 0 {
                badgeValue = String(count)
            }
            self?.tabBar.items?[2].badgeValue = badgeValue
        }
    }
    
}

extension TechnicianTabBarController: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        guard let controller = viewControllers?[selectedIndex] as? UINavigationController,
        let tab = TechnicianTab(rawValue: selectedIndex) else { return }
        
        switch tab {
        case .inspections:
            onInspectionFlowSelect?(controller)
        case .holdUps:
            onHoldUpFlowSelect?(controller)
        case .notifications:
            onNotificationFlowSelect?(controller)
        }
    }
    
}
