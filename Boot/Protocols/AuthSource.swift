//
//  AuthSource.swift
//  Boot
//
//  Created by Chris James on 17/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseFirestore

protocol AuthSource {
    typealias AuthStatus = (_ signedIn: Bool, _ supervisor: Bool) -> Void
    typealias SignOutCompletion = (_ success: Bool) -> Void
    
    func getUid() -> String?
    func getName() -> String?
    func getName(uid: String?, username: String?) -> String?
    func getProfileUrl() -> URL?
    func signedIn(completion: @escaping AuthStatus)
    func signOut(completion: @escaping SignOutCompletion)
}

class FirebaseAuthSource: AuthSource {

    func signedIn(completion: @escaping AuthStatus){
        guard let uid = getUid() else {return completion(false, false)}
        
        let ref = Firestore.firestore().collection("minesites").document("cadia")
            .collection("users").document(uid)
        
        ref.getDocument { (document, error) in
            if let error = error {
                print(error.localizedDescription)
                completion(false, false)
            } else if let document = document, let dict = document.data(),
                let profile = Profile(uid: document.documentID, dictionary: dict) {
                if profile.role == .supervisor {
                    completion(true, true)
                } else {
                    completion(true, false)
                }
            } else {
                completion(false, false)
            }
        }
    }
    
    func getUid() -> String? {
        return Auth.auth().currentUser?.uid
    }
    
    func getName() -> String? {
        return Auth.auth().currentUser?.displayName
    }
    
    func getProfileUrl() -> URL? {
        return Auth.auth().currentUser?.photoURL
    }
    
    func getName(uid: String?, username: String?) -> String? {
        guard let uid = uid, let username = username else {return nil}
        
        if uid == Auth.auth().currentUser?.uid {
            return "you"
        } else {
            return username
        }
    }
    
    func signOut(completion: @escaping SignOutCompletion) {
        do {
            try Auth.auth().signOut()
            completion(true)
        } catch {
            completion(false)
        }
    }
    
}
