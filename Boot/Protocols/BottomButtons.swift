//
//  ListBottomButtonView.swift
//  Boot
//
//  Created by Chris James on 30/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

protocol BottomButtons: class {
    var view: UIView! {get set}
    var bottomButtonView: BottomButtonView! {get set}
}

extension BottomButtons {
    
    func addBottomButtons(primaryBtnTitle: String, secondaryBtnTitle: String?){
        let bottomButtonView = BottomButtonView(primaryBtnTitle: primaryBtnTitle, secondaryBtnTitle: secondaryBtnTitle)
        bottomButtonView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bottomButtonView)
        self.bottomButtonView = bottomButtonView
    }
    
    func layoutButtomButons(){
        NSLayoutConstraint.activate([
            bottomButtonView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            bottomButtonView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            bottomButtonView.trailingAnchor.constraint(equalTo: view.trailingAnchor)])
    }
    
    func enablePrimaryBtn(_ isEnabled: Bool){
        bottomButtonView.enablePrimaryBtn(isEnabled: isEnabled)
    }
    
    func enableSecondaryBtn(_ isEnabled: Bool){
        bottomButtonView.enableSecondaryBtn(isEnabled: isEnabled)
    }
    
}
