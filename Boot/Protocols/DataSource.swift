//
//  DataSource.swift
//  Boot
//
//  Created by Chris James on 19/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import FirebaseFirestore

protocol DataSource {
    func startListening()
    func stopListening()
    func resumeListening()
    func pullToRefresh()
    
    var refreshing: ((Bool) -> Void)? {get set}
    var empty: ((Bool, Bool) -> Void)? {get set}
}

class FirebaseCollectionDataSource<T: Document, U: ViewModel>: DataSource {
    var authSource: AuthSource
    var observer: ListenerRegistration?
    var query: Query
    
    private var refreshTimer: Timer?
    private var uiDelayTimer: Timer?
    
    //If no items are found during this time, show the empty screen
    let refreshTime: Double = 3.0
    
    //When listeners are reattached, give the cache time to repopulate the table.
    //Prevent UI updating so the view doesn't flicker
    let uiDelay: Double = 1.0
    
    var viewModels = [U]()
    var dataUpdate: (([U]) -> Void)?
    var refreshing: ((Bool) -> Void)?
    var empty: ((Bool, Bool) -> Void)?
    
    
    init(query: Query, authSource: AuthSource){
        self.authSource = authSource
        self.query = query
    }
    
    func startListening(){
        stopListening()
        startRefreshing()
        attachListener()
    }

    func resumeListening(){
        stopListening()
        uiDelayTimer = Timer.scheduledTimer(withTimeInterval: uiDelay,
                                            repeats: false,
                                            block: {[weak self] (timer) in
            timer.invalidate()
            self?.sendDataUpdate()
        })
        attachListener()
    }
    
    func pullToRefresh(){
        stopListening()
        startRefreshing()
        
        uiDelayTimer = Timer.scheduledTimer(withTimeInterval: uiDelay, repeats: false, block: {[weak self] (timer) in
            timer.invalidate()
            self?.sendDataUpdate(endRefreshing: false)
        })
        attachListener()
    }
    
    
    private func attachListener(){
        observer = query.addSnapshotListener {[weak self] snapshot, error in
            self?.respondToSnapshot(snapshot: snapshot, error: error)
        }
    }
    
    func stopListening(){
        uiDelayTimer?.invalidate()
        refreshTimer?.invalidate()
        observer?.remove()
        viewModels.removeAll()
    }
    
    private func respondToSnapshot(snapshot: QuerySnapshot?, error: Error?){
        guard let snapshot = snapshot else {
            print("error adding snapshot" + error.debugDescription)
            return
        }
        snapshot.documentChanges.forEach { diff in
            
            switch (diff.type) {
            case .added:
                if let viewModel = getViewModel(diff: diff) {
                    add(viewModel, newIndex: Int(diff.newIndex))
                }
                
            case .modified:
                if let viewModel = getViewModel(diff: diff){
                    modify(viewModel, oldIndex: Int(diff.oldIndex), newIndex: Int(diff.newIndex))
                } else {
                    remove(diff.document.documentID, oldIndex: Int(diff.oldIndex))
                }
                
            case .removed:
                self.remove(diff.document.documentID, oldIndex: Int(diff.oldIndex))
            }
            
        }
    }
    
    private func startRefreshing(){
        refreshing?(true)
        empty?(false, true)
        refreshTimer?.invalidate()
        refreshTimer = Timer.scheduledTimer(withTimeInterval: refreshTime, repeats: false, block: {[weak self] (timer) in
            timer.invalidate()
            self?.sendDataUpdate(endRefreshing: true, animated: true)
        })
    }
    
    private func endRefreshing(animated: Bool){
        refreshTimer?.invalidate()
        refreshing?(false)
        empty?(viewModels.isEmpty, animated)
    }
    
    private func sendDataUpdate(endRefreshing: Bool = true, animated: Bool = false){
        DispatchQueue.main.async {
            if self.uiDelayTimer?.isValid == true {
                return
            }
            if endRefreshing {
                self.endRefreshing(animated: animated)
            }
            self.dataUpdate?(self.viewModels)
        }
    }
    
    private func add(_ viewModel: U, newIndex: Int){
        //Check if the document already exists to avoid duplicates
        if let index = indexOf(viewModel.uid) {
            viewModels.remove(at: index)
        }

        insert(viewModel: viewModel, newIndex: newIndex)
        sendDataUpdate()
    }
    
    private func remove(_ uid: String, oldIndex: Int){
        //First attempt to remove using the index Firebase provides as this will be quicker
        if oldIndexIsCorrect(uid: uid, oldIndex: oldIndex){
            viewModels.remove(at: oldIndex)
            sendDataUpdate()
        //If this index wasn't correct, search for the correct document to remove
        } else if let index = indexOf(uid){
            viewModels.remove(at: index)
            sendDataUpdate()
        }
    }
    
    private func modify(_ viewModel: U, oldIndex: Int, newIndex: Int){
        if viewModels.isEmpty {
            insert(viewModel: viewModel, newIndex: 0)
            
        } else if oldIndexIsCorrect(uid: viewModel.uid, oldIndex: oldIndex){
            //If the viewModels are identical, don't trigger an update
            if viewModels[oldIndex] == viewModel {
                return
            }
            viewModels.remove(at: oldIndex)
            insert(viewModel: viewModel, newIndex: newIndex)
            
        } else if let index = indexOf(viewModel.uid){
            viewModels[index] = viewModel
            
        } else {
            insert(viewModel: viewModel, newIndex: newIndex)
        }
        
        sendDataUpdate()
    }
    
    private func insert(viewModel: U, newIndex: Int){
        if viewModels.count > newIndex {
            viewModels.insert(viewModel, at: newIndex)
        } else {
            viewModels.append(viewModel)
        }
    }
    
    func getViewModel(diff: DocumentChange) -> U? {
        if let document = T.init(uid: diff.document.documentID, dictionary: diff.document.data()){
            return U.init(document: document, authSource: authSource)
        } else {
            return nil
        }
    }
    
    func indexOf(_ uid: String) -> Int? {
        return viewModels.firstIndex(where: {$0.uid == uid})
    }

    private func oldIndexIsCorrect(uid: String, oldIndex: Int) -> Bool {
        guard viewModels.indices.contains(oldIndex) else {return false}
        return viewModels[oldIndex].uid == uid
    }
    
    deinit {
        stopListening()
    }
    
}
