//
//  Document.swift
//  Boot
//
//  Created by Chris James on 19/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation

protocol Document: Unique {
    init?(uid: String, dictionary: [String: Any])
}
