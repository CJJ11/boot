//
//  ListView.swift
//  Boot
//
//  Created by Chris James on 30/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit
import CoreGraphics

@objc protocol List {
    var view: UIView! {get set}
    var tableView: UITableView! {get set}
}

extension List {
    
    func addTableView(estimatedRowHeight: CGFloat, style: UITableView.Style = .grouped){
        let tableView = UITableView(frame: .zero, style: style)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = estimatedRowHeight
        
        view.addSubview(tableView)
        self.tableView = tableView
    }
    
    func layoutTableView(bottomView: UIView? = nil){
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomView?.topAnchor ?? view.bottomAnchor)
            ])
    }

}
