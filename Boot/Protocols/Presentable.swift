//
//  Presentable.swift
//  Boot
//
//  Created by Chris James on 08/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

protocol Presentable {
    func toPresent() -> UIViewController?
}

extension UIViewController: Presentable {
    func toPresent() -> UIViewController? {
        return self
    }
}
