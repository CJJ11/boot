//
//  RefreshListView.swift
//  Boot
//
//  Created by Chris James on 30/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

@objc protocol RefreshingList: List {
    var refreshControl: UIRefreshControl! {get set}
    var emptyView: UIView! {get set}
    
    @objc func pullToRefresh()
}

extension RefreshingList {
    
    func addViews(estimatedRowHeight: CGFloat, emptyImage: UIImage, emptyText: String, style: UITableView.Style = .grouped){
        addTableView(estimatedRowHeight: estimatedRowHeight, style: style)
        addRefreshControl()
        addEmptyView(image: emptyImage, text: emptyText)
    }
    
    func layoutViews(bottomView: UIView? = nil){
        layoutTableView(bottomView: bottomView)
        refreshControl.layoutIfNeeded()
        layoutEmptyView(bottomView: bottomView)
    }
    
    private func addRefreshControl(){
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(pullToRefresh), for: UIControl.Event.valueChanged)
        tableView.refreshControl = refreshControl
        self.refreshControl = refreshControl
    }
    
    private func addEmptyView(image: UIImage, text: String){
        let emptyView = EmptyView(image: image, text: text)
        emptyView.translatesAutoresizingMaskIntoConstraints = false
        emptyView.isHidden = true
        emptyView.isUserInteractionEnabled = false
        tableView.backgroundView = emptyView
        self.emptyView = emptyView
    }
    
    private func layoutEmptyView(bottomView: UIView?){
        NSLayoutConstraint.activate([
            emptyView.topAnchor.constraint(equalTo: view.topAnchor),
            emptyView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            emptyView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            emptyView.bottomAnchor.constraint(equalTo: bottomView?.topAnchor ?? view.bottomAnchor)
            ])
    }
    
    func updateRefreshing(refreshing: Bool){
        if refreshing && !refreshControl.isRefreshing {
            refreshControl.beginRefreshing()
            
        } else if !refreshing && refreshControl.isRefreshing { //Stop refreshing
            Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false, block: {[weak self] (timer) in
                self?.refreshControl.endRefreshing()
            })
        }
    }
    
    func updateEmpty(empty: Bool, animated: Bool){
        emptyView.layer.removeAllAnimations()
        
        if empty && animated {
            emptyView.alpha = 0
            emptyView.isHidden = false
            
            UIView.animate(withDuration: 1.0, delay: 0, options: [], animations: { [weak self] () in
                self?.emptyView.alpha = 1
            })
        } else if empty {
            emptyView.alpha = 1
            emptyView.isHidden = false
        } else {
            emptyView.isHidden = true
        }
    }
    
}
