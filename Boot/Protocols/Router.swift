//
//  Router.swift
//  Boot
//
//  Created by Chris James on 08/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//


import UIKit

protocol Router: Presentable {
    func push(_ module: Presentable?, animated: Bool, hideToolBar: Bool, completion: (() -> Void)?)
    func popModule(animated: Bool)
    
    func present(_ module: Presentable?, animated: Bool, completion: (() -> Void)?)
    func dismissModule(animated: Bool, completion: (() -> Void)?)
    
    func setRootModule(_ module: Presentable?, hideNavBar: Bool)
    func popToRootModule(animated: Bool)
}

final class RouterImp: NSObject, Router {
    
    private var navController: UINavigationController
    private var completions: [UIViewController : () -> Void]
    
    init(navController: UINavigationController = UINavigationController()) {
        self.navController = navController
        completions = [:]
    }
    
    func toPresent() -> UIViewController? {
        return navController
    }
    
    func present(_ module: Presentable?, animated: Bool = true, completion: (() -> Void)? = nil) {
        if let controller = module?.toPresent() {
            navController.present(controller, animated: animated, completion: completion)
        }
    }
    
    func dismissModule(animated: Bool = true, completion: (() -> Void)? = nil) {
        navController.dismiss(animated: animated, completion: completion)
    }
    
    func push(
        _ module: Presentable?,
        animated: Bool = true,
        hideToolBar: Bool = false,
        completion: (() -> Void)? = nil) {
        
        guard let controller = module?.toPresent(),
            (controller is UINavigationController == false)
            else { assertionFailure("Deprecated push UINavigationController."); return }
        
        if let completion = completion {
            completions[controller] = completion
        }
        controller.hidesBottomBarWhenPushed = hideToolBar
        navController.pushViewController(controller, animated: animated)
    }
    
    func popModule(animated: Bool = true)  {
        if let controller = navController.popViewController(animated: animated) {
            runCompletion(for: controller)
        }
    }
    
    func setRootModule(_ module: Presentable?, hideNavBar: Bool = false) {
        guard let controller = module?.toPresent() else { return }
        navController.setViewControllers([controller], animated: false)
        navController.isNavigationBarHidden = hideNavBar
    }
    
    func popToRootModule(animated: Bool) {
        if let controllers = navController.popToRootViewController(animated: animated) {
            controllers.forEach { controller in
                runCompletion(for: controller)
            }
        }
    }
    
    private func runCompletion(for controller: UIViewController) {
        guard let completion = completions[controller] else { return }
        completion()
        completions.removeValue(forKey: controller)
    }
}
