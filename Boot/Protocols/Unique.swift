//
//  Unique.swift
//  Boot
//
//  Created by Chris James on 19/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation

protocol Unique {
    var uid: String {get}
}
