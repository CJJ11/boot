//
//  ViewModel.swift
//  Boot
//
//  Created by Chris James on 07/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation

protocol ViewModel: Unique, Equatable {
    init?(document: Document, authSource: AuthSource)
}
