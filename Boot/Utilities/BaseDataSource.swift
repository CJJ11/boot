//
//  BaseDataSource.swift
//  Boot
//
//  Created by Chris James on 19/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import FirebaseFirestore

protocol Unique {
    var uid: String {get set}
}

protocol Document: Unique {
    init?(uid: String, dictionary: [String: Any])
}

protocol ViewModel: Unique {
    init(document: Document)
}

final class BaseDataSource<T: Document, U: ViewModel> {
    typealias Item = Document
    var observer: ListenerRegistration?
    var reference: CollectionReference?
    var viewModels = [U]()
    var dataUpdate: (([U]) -> Void)?
    
    func startListening(){
        observer = reference?.addSnapshotListener {snapshot, error in
            guard let snapshot = snapshot else {
                print("error adding snapshot" + error.debugDescription)
                return
            }
            
            snapshot.documentChanges.forEach {[weak self] diff in
                if let document = T.init(uid: diff.document.documentID, dictionary: diff.document.data()) {
                    let viewModel = U.init(document: document)
                    switch (diff.type) {
                    case .added: self?.add(viewModel)
                    case .modified: self?.modify(viewModel)
                    case .removed: self?.remove(viewModel)
                    }
                }
            }
        }
    }
    
    func stopListening(){
        observer?.remove()
    }
    
    private func add(_ viewModel: U){
        if indexOf(viewModel) != nil {
            modify(viewModel)
        } else {
            viewModels.append(viewModel)
            dataUpdate?(viewModels)
        }
    }
    
    private func remove(_ viewModel: U){
        if let index = indexOf(viewModel){
            viewModels.remove(at: index)
            dataUpdate?(viewModels)
        }
    }
    
    private func modify(_ viewModel: U){
        if let index = indexOf(viewModel){
            viewModels[index] = viewModel
            dataUpdate?(viewModels)
        } else {
            add(viewModel)
        }
    }
    
    private func indexOf(_ viewModel: U) -> Int? {
        if let index = viewModels.firstIndex(where: {$0.uid == viewModel.uid}){
            return index
        } else {
            return nil
        }
    }
    
    deinit {
        stopListening()
    }
    
}
