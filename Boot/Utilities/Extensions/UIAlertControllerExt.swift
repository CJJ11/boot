//
//  UIAlertController.swift
//  Boot
//
//  Created by Chris James on 24/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation

import UIKit

extension UIAlertController {
    
    func addPickerView(values: [String],  initialSelection: Int? = nil, action: PickerVC.Action?) {
        let pickerView = PickerVC(values: values, initialSelection: initialSelection, action: action)
        set(vc: pickerView, height: 216)
    }
    
    func set(vc: UIViewController?, width: CGFloat? = nil, height: CGFloat? = nil) {
        guard let vc = vc else { return }
        setValue(vc, forKey: "contentViewController")
        if let height = height {
            vc.preferredContentSize.height = height
            preferredContentSize.height = height
        }
    }
    
}

