//
//  UIImageExtensions.swift
//  Boot
//
//  Created by Chris James on 06/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    func resize(scaledTo newSize: CGSize) -> UIImage? {
        if self.size.width <= newSize.width || self.size.height <= newSize.height {
            return self
        }
        
        UIGraphicsBeginImageContextWithOptions(newSize, true, 1.0)
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    
}
