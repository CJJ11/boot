//
//  FirebaseDocumentDataSource.swift
//  Boot
//
//  Created by Chris James on 19/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import FirebaseFirestore

class FirebaseDocumentDataSource<T: Document> {
    var observer: ListenerRegistration?
    var reference: DocumentReference
    var dataUpdate: ((T) -> Void)?
    
    init(reference: DocumentReference){
        self.reference = reference
    }
    
    func startListening(){
        observer = reference.addSnapshotListener {[weak self] snapshot, error in
            guard let snapshot = snapshot else {
                print("error adding snapshot" + error.debugDescription)
                return
            }
            
            if let data = snapshot.data(),
                let document = T.init(uid: snapshot.documentID, dictionary: data){
                self?.dataUpdate?(document)
            }
        }
    }
    
    func stopListening(){
        observer?.remove()
    }
    
    deinit {
        stopListening()
    }
    
}
