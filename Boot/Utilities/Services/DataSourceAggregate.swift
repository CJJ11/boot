//
//  DataSourceAggregate.swift
//  Boot
//
//  Created by Chris James on 30/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import FirebaseFirestore


class DataSourceAggregate<T: Document, U: ViewModel, V: RawRepresentable & Hashable>: DataSource {
    typealias Data = FirebaseCollectionDataSource<T, U>
    
    private let dataSources: [V: Data]
    private var selectedType: V
    
    let reference: CollectionReference
    let authSource: AuthSource
    
    var dataUpdate: (([U]) -> Void)?
    var count: ((V, Int) -> Void)?
    
    var refreshing: ((Bool) -> Void)?
    var empty: ((Bool, Bool) -> Void)?
    
    
    init(reference: CollectionReference, authSource: AuthSource, types: [V], firstSelectedType: V, filterDatabaseKey: String, sortDatabaseKey: String, sortDescending: Bool) {
        self.selectedType = firstSelectedType
        self.reference = reference
        self.authSource = authSource
        
        let typesWithDataSources = types.map { (type) -> (V, Data) in
            let query = reference.whereField(filterDatabaseKey, isEqualTo: type.rawValue).order(by: sortDatabaseKey, descending: sortDescending)
            let dataSource = Data(query: query, authSource: authSource)
            return (type, dataSource)
        }
        dataSources = Dictionary(uniqueKeysWithValues: typesWithDataSources)
        
        for (type, dataSource) in dataSources {
            dataSource.dataUpdate = {[weak self] (viewModels) in
                self?.count?(type, viewModels.count)
                if self?.selectedType == type {
                    self?.dataUpdate?(viewModels)
                }
            }
            
            dataSource.empty = {[weak self] (empty, animating) in
                if self?.selectedType == type {
                    self?.empty?(empty, animating)
                }
            }
            
            dataSource.refreshing = {[weak self] (refreshing) in
                if self?.selectedType == type {
                    self?.refreshing?(refreshing)
                print("Refreshing sent: \(refreshing)")
                }
            }
        }
    }
    
    func startListening() {
        dataSources.values.forEach({$0.startListening()})
    }
    
    func stopListening() {
        dataSources.values.forEach({$0.stopListening()})
    }
    
    func resumeListening() {
        dataSources.values.forEach({$0.resumeListening()})
    }
    
    func pullToRefresh() {
        dataSources.values.forEach({$0.pullToRefresh()})
    }
    
    func toggleViewModels(type: V) {
        self.selectedType = type
        refreshing?(false)
        if let viewModels = dataSources[type]?.viewModels {
            dataUpdate?(viewModels)
            empty?(viewModels.isEmpty, false)
        }
    }
    
    func getViewModels() -> [U] {
        var viewModels = [U]()
        for (_, dataSource) in dataSources {
            viewModels.append(contentsOf: dataSource.viewModels)
        }
        return viewModels
    }
    
}

