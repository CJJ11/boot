//
//  DateService.swift
//  Boot
//
//  Created by Chris James on 25/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation

class dateService {
    
    static let ds = dateService()
    
    let shortTimeFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        formatter.dateStyle = .none
        return formatter
    }()
    
    
    let shortDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeStyle = .none
        formatter.dateStyle = .short
        return formatter
    }()
    
    
    func timeAsString(date: Date?) -> String {
        if let date = date {
            return shortTimeFormatter.string(from: date).removingWhitespaces()
        }else {
            return ""
        }
    }
    
    static func currentTimeBetween(startTime: Date, finishTime: Date) -> Bool{
        let now = Date()
        let calendar = Calendar.current
        
        let startComponents = calendar.dateComponents([.hour, .minute], from: startTime)
        guard let startHour = startComponents.hour, let startMinute = startComponents.minute else {return false}
        guard let start = calendar.date(bySettingHour: startHour, minute: startMinute, second: 0, of: now) else {return false}
        
        let finishComponents = calendar.dateComponents([.hour, .minute], from: finishTime)
        guard let finishHour = finishComponents.hour, let finishMinute = finishComponents.minute else {return false}
        guard let finish = calendar.date(bySettingHour: finishHour, minute: finishMinute, second: 0, of: now) else {return false}
        
        return now > start && now < finish
    }
    
    
    static func dateFrom(hour: Int, minute: Int) -> Date? {
        return Calendar.current.date(bySettingHour: hour, minute: minute, second: 0, of: Date())
    }
    
    
    func timeSince(from: Date?) -> String {
        guard let from = from else {return ""}
        let calendar = Calendar.current
        let now = Date()
        var result = ""
        
        if now >= from {
            let components = calendar.dateComponents([.year, .weekOfYear, .month, .day, .hour, .minute, .second], from: from, to: now)
            
            if components.year! >= 1  {
                result = shortDateFormatter.string(from: from)
            } else if components.month! >= 1 {
                result = shortDateFormatter.string(from: from)
            } else if components.weekOfYear! >= 1 {
                result = "\(components.weekOfYear!)w ago"
            } else if components.day! >= 1 {
                result = "\(components.day!)d ago"
            } else if components.hour! >= 2 {
                result = "\(components.hour!)h ago"
            } else if components.minute! >= 1 {
                result = "\(components.minute!)m ago"
            } else if components.second! >= 3 {
                result = "\(components.second!)s ago"
            } else {
                result = "just now"
            }
        }
        return result
    }
    
    
    static func roundedCurrentDate() -> Date {
        let seconds = ceil(Date().timeIntervalSinceReferenceDate/900.0)*900.0
        let roundedDate = Date(timeIntervalSinceReferenceDate: seconds)
        return roundedDate
    }
    
    
    static func getMidnight() -> Date? {
        let calendar = Calendar.current
        let today = Date()
        if let tomorrow = calendar.date(byAdding: .day, value: 1, to: today){
            return calendar.startOfDay(for: tomorrow)
        } else {
            return nil
        }
    }
    
    
    static func timePickerString(time: Date?, valid: Bool) -> NSAttributedString {
        guard let time = time else {return NSAttributedString(string: "-:--")}
        
        if valid {
            return NSAttributedString(string: ds.timeAsString(date: time))
        } else {
            return ds.timeAsString(date: time).strikeThrough()
        }
        
    }
}
