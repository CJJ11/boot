//
//  StorageService.swift
//  boot
//
//  Created by Chris James on 04/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import FirebaseStorage

//protocol StorageService {
//    static func getS
//    static func uploadImage(image: UIImage, storageType: StorageType, completion: @escaping ImageCompletion)
//}

//typealias ImageCompletion =  (_ url: String?, _ imageID: String?, _ error: Error?) -> Void


class FirebaseStorageService {

    //Firebase Storage bucket keys
    private static let profileImageKey = "profile-images"
    private static let holdUpImageKey = "holdUp-images/full-res"
    private static let holdUpVideoKey = "holdUp-videos"
    private static let holdUpAudioKey = "holdUp-audio"

    static func getStorageBucket(storageType: StorageType, fileType: MediaType) -> StorageReference {
        switch storageType {
        case .profileImage:
            return Storage.storage().reference().child(profileImageKey)
        case .holdUpAttachment:
            switch (fileType){
            case .image: return Storage.storage().reference().child(holdUpImageKey)
            case .audio: return Storage.storage().reference().child(holdUpAudioKey)
            case .video: return Storage.storage().reference().child(holdUpVideoKey)
            }
        }
    }
    
    static func getMetadata(fileType: MediaType) -> StorageMetadata {
        let metadata = StorageMetadata()
        switch(fileType){
        case .image:
            metadata.contentType = "image/jpg"
        case .audio:
            metadata.contentType = "audio/m4a"
        case .video:
            metadata.contentType = "video/mov"
        }
        return metadata
    }
    
     static func getFileName(fileType: MediaType) -> String {
        let fileID = NSUUID().uuidString
        switch(fileType){
        case .image:
            return fileID + ".jpg"
        case .audio:
            return fileID + ".m4a"
        case .video:
            return fileID + ".mov"
        }
    }
    
    static func getUploadTask(fileUrl: URL, storageType: StorageType, fileType: MediaType) -> StorageTask {
        let metadata = FirebaseStorageService.getMetadata(fileType: fileType)
        let fileName = FirebaseStorageService.getFileName(fileType: fileType)
        let storageBucket = FirebaseStorageService.getStorageBucket(storageType: storageType, fileType: fileType)
        let storageRef = storageBucket.child(fileName)
        let uploadTask = storageRef.putFile(from: fileUrl, metadata: metadata)
        return uploadTask
    }
    
}



//class PhotoUploadManager {
//    static var urlSessionIdentifier = "photoUploadsFromMainApp"  // Should be changed in app extensions.
//    static let urlSession: URLSession = {
//        let configuration = URLSessionConfiguration.background(withIdentifier: PhotoUploadManager.urlSessionIdentifier)
//        configuration.sessionSendsLaunchEvents = false
//        configuration.sharedContainerIdentifier = "my-suite-name"
//        return URLSession(configuration: configuration)
//    }()
//
//    // ...
//
//    // Example upload URL: https://firebasestorage.googleapis.com/v0/b/my-bucket-name/o?name=user-photos/someUserId/ios/photoKey.jpg
//    func startUpload(fileUrl: URL, contentType: String, uploadUrl: URL) {
//        Auth.auth().currentUser?.getIDToken() { token, error in
//            if let error = error {
//                print("ID token retrieval error: \(error.localizedDescription)")
//                return
//            }
//            guard let token = token else {
//                print("No token.")
//                return
//            }
//
//            var urlRequest = URLRequest(url: uploadUrl)
//            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
//            urlRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
//            urlRequest.httpMethod = "POST"
//            let uploadTask = PhotoUploadManager.urlSession.uploadTask(with: urlRequest, fromFile: fileUrl)
//            uploadTask.resume()
//        }
//    }
//}
