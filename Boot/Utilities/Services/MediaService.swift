//
//  MediaService.swift
//  Boot
//
//  Created by Chris James on 21/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

class MediaService {
    
    static func getVideoThumbnail(from url: URL) -> UIImage? {
        do {
            let asset = AVURLAsset(url: url)
            let imageGenerator = AVAssetImageGenerator(asset: asset)
            imageGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imageGenerator.copyCGImage(at: .zero,
                                                         actualTime: nil)
            return UIImage(cgImage: cgImage)
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    static func getImage(from imageUrl: URL) -> UIImage? {
        guard FileManager.default.fileExists(atPath: imageUrl.path),
            let data = try? Data(contentsOf: imageUrl) else {return nil}
        return UIImage(data: data)
    }
    
    static func storeToDocumentDirectory(image: UIImage) -> URL? {
        do {
            guard let data = image.jpegData(compressionQuality: 0.7) else {return nil}
            let documentDirectory = try FileManager.default.url(
                for: .documentDirectory,
                in: .userDomainMask,
                appropriateFor: nil,
                create: false
            )
            
            let fileName = NSUUID().uuidString
            let fileUrl = documentDirectory.appendingPathComponent(fileName)
            
            try data.write(to: fileUrl)
            return fileUrl
        } catch {
            return nil
        }
    }
    
    static func deleteFile(at fileUrl: URL){
        do {
            try FileManager.default.removeItem(atPath: fileUrl.path)
            print("File deleted")
        }
        catch let error as NSError {
            print("Error deleting file: \(error)")
        }
    }

    
    static func sizeInMB(for localFileUrl: URL) -> String? {
        do {
            let fileAttributes = try FileManager.default.attributesOfItem(atPath: localFileUrl.path)
            let fileSizeAny = fileAttributes[FileAttributeKey.size]
            guard let fileSize = fileSizeAny as? NSNumber else {return nil}
            
            let unsignedLong = fileSize.uint64Value
            let signedLong = Int64(bitPattern: unsignedLong)
            
            let bcf = ByteCountFormatter()
            bcf.allowedUnits = [.useMB, .useKB]
            bcf.countStyle = .file
            return bcf.string(fromByteCount: signedLong)
        }
        catch let error {
            print(error.localizedDescription)
            return nil
        }
    }
    
}
