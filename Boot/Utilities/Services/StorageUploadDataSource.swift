//
//  StorageUploadDataSource.swift
//  Boot
//
//  Created by Chris James on 16/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//


import Foundation
import FirebaseStorage

protocol StorageUploadModule: AnyObject {
    func startUpload()
    var fileUrl: URL {get}
    var uploadProgress: ((Int) -> Void)? {get set}
    var uploadSuccess: ((URL) -> Void)? {get set}
    var uploadFailure: ((Error) -> Void)? {get set}
}

enum StorageFileType {
    case image
    case audio
    case video
}

class StorageUploadFirebaseModule: StorageUploadModule {

    let fileUrl: URL
    let fileType: StorageFileType

    
    var uploadObserver: StorageTask?
    var uploadResumed: (() -> Void)?
    var uploadPaused: (() -> Void)?
    var uploadProgress: ((Int) -> Void)?
    var uploadSuccess: ((URL) -> Void)?
    var uploadFailure: ((Error) -> Void)?
    
    init(fileUrl: URL, fileType: StorageFileType){
        self.fileUrl = fileUrl
        self.fileType = fileType
    }
    
    func startUpload(){
        let metadata = getMetaData()
        
        
        // Upload file and metadata to the object 'images/mountains.jpg'
        let uploadTask = storageRef.putFile(from: fileUrl, metadata: metadata)
        
        uploadTask.observe(.resume) { snapshot in
            uploadResumed()
        }
        
        uploadTask.observe(.pause) { snapshot in
            // Upload paused
        }
        
        uploadTask.observe(.progress) { snapshot in
            // Upload reported progress
            let percentComplete = 100.0 * Double(snapshot.progress!.completedUnitCount)
                / Double(snapshot.progress!.totalUnitCount)
        }
        
        uploadTask.observe(.success) { snapshot in
            // Upload completed successfully
        }
        
        uploadTask.observe(.failure) { snapshot in
            if let error = snapshot.error as? NSError {
                switch (StorageErrorCode(rawValue: error.code)!) {
                case .objectNotFound:
                    // File doesn't exist
                    break
                case .unauthorized:
                    // User doesn't have permission to access file
                    break
                case .cancelled:
                    // User canceled the upload
                    break
                    
                    /* ... */
                    
                case .unknown:
                    // Unknown error occurred, inspect the server response
                    break
                default:
                    // A separate error occurred. This is a good place to retry the upload.
                    break
                }
            }
        }
    }
    
    
    private func getMetaData() -> StorageMetadata {
        let metaData = StorageMetadata()
        switch(fileType){
        case .image: metaData.contentType = "image/jpeg"
        case .audio: metaData.contentType = "to be fixed"
        case .video: metaData.contentType = "to be fixed"
        }
        return metaData
    }
    
    private func getStorageReference() -> StorageReference {
        
    }

    

}
