//
//  StorageUploadDataSource.swift
//  Boot
//
//  Created by Chris James on 16/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//


import Foundation
import FirebaseStorage

protocol StorageUploader: AnyObject {
    typealias StorageUploadCallback = ((StorageUploadStatus, URL?, String?, Float?, Error?) -> Void)?
    
    var fileUrl: URL {get}
    var storageType: StorageType {get}
    var fileType: MediaType {get}
    var callback: StorageUploadCallback {get set}
    
    func startUpload()
}

class FirebaseStorageUploader: StorageUploader {
    
    let fileUrl: URL
    let fileType: MediaType
    let storageType: StorageType
    
    var uploadObserver: StorageTask?
    var callback: StorageUploadCallback

    init(fileUrl: URL, mediaType: MediaType, storageType: StorageType){
        self.fileUrl = fileUrl
        self.fileType = mediaType
        self.storageType = storageType
    }
    
    func startUpload(){
        let metadata = FirebaseStorageService.getMetadata(fileType: fileType)
        let fileName = FirebaseStorageService.getFileName(fileType: fileType)
        let storageBucket = FirebaseStorageService.getStorageBucket(storageType: storageType, fileType: fileType)
        let storageRef = storageBucket.child(fileName)
        let uploadTask = storageRef.putFile(from: fileUrl, metadata: metadata)
        
        uploadTask.observe(.resume) {[weak self] snapshot in
            self?.callback?(.uploading, nil, nil, nil, nil)
        }
        
        uploadTask.observe(.pause) {[weak self] snapshot in
            self?.callback?(.paused, nil, nil, nil, nil)
        }
        
        uploadTask.observe(.progress) {[weak self] snapshot in
            let progress = Float(snapshot.progress!.completedUnitCount)
                / Float(snapshot.progress!.totalUnitCount)
            self?.callback?(.uploading, nil, nil, progress, nil)
            
        }
        
        uploadTask.observe(.success) {[weak self] snapshot in
            storageRef.downloadURL(completion: { (url, error) in
                if let error = error {
                    self?.callback?(.failure, nil, nil, nil, error)
                } else if let url = url {
                    self?.callback?(.success, url, fileName, nil, nil)
                }
            })
        }
        
        uploadTask.observe(.failure) {[weak self] snapshot in
            self?.callback?(.failure, nil, nil, nil, snapshot.error!)
        }
    }
    
}

protocol StorageUploaderFactory {
    func make(fileUrl: URL, mediaType: MediaType, storageType: StorageType) -> StorageUploader
    func make(attachment: AddHoldUpAtt) -> StorageUploader
}

class FirebaseStorageUploaderFactory: StorageUploaderFactory {
    func make(fileUrl: URL, mediaType: MediaType, storageType: StorageType) -> StorageUploader {
        return FirebaseStorageUploader(fileUrl: fileUrl, mediaType: mediaType, storageType: storageType)
    }
    
    func make(attachment: AddHoldUpAtt) -> StorageUploader {
        return FirebaseStorageUploader(
            fileUrl: attachment.localFileUrl,
            mediaType: attachment.mediaType,
            storageType: StorageType.holdUpAttachment
        )
    }
}
