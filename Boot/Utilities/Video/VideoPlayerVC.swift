//
//  VideoPlayerVC.swift
//  Boot
//
//  Created by Chris James on 10/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit
import AVFoundation

class VideoPlayerVC: UIViewController {
    
    @IBOutlet weak var videoView: UIView!
    
    let avPlayer = AVPlayer()
    var avPlayerLayer: AVPlayerLayer!
    var videoURL: URL!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        avPlayerLayer = AVPlayerLayer(player: avPlayer)
        avPlayerLayer.frame = view.bounds
        avPlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoView.layer.insertSublayer(avPlayerLayer, at: 0)
        
        view.layoutIfNeeded()
        
        let playerItem = AVPlayerItem(url: videoURL as URL)
        avPlayer.replaceCurrentItem(with: playerItem)
        avPlayer.play()
    }
}
