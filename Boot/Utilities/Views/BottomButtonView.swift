//
//  BottomButtonView.swift
//  Boot
//
//  Created by Chris James on 17/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

@IBDesignable
class BottomButtonView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var primaryBtn: LargeBtn!
    @IBOutlet weak var secondaryBtn: UIButton!
    @IBOutlet weak var dividerHeight: NSLayoutConstraint!

    @IBInspectable var primaryBtnTitle: String = "Submit to Supervisor" {
        didSet {
            primaryBtn.setTitle(primaryBtnTitle, for: .normal)
        }
    }
    @IBInspectable var secondaryBtnTitle: String = "Cancel" {
        didSet {
            secondaryBtn.setTitle(secondaryBtnTitle, for: .normal)
        }
    }
    
    @IBInspectable var secondaryBtnRequired: Bool = false {
        didSet {
            secondaryBtn.isHidden = !secondaryBtnRequired
        }
    }
    
    var primaryBtnTapped: (() -> Void)?
    var secondaryBtnTapped: (() -> Void)?
    
    init(primaryBtnTitle: String, secondaryBtnTitle: String? = nil){
        super.init(frame: .zero)
        xibSetup()
        primaryBtn.setTitle(primaryBtnTitle, for: .normal)
        secondaryBtn.isHidden = (secondaryBtnTitle == nil)
        secondaryBtn.setTitle(secondaryBtnTitle, for: .normal)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        guard let view = loadViewFromNib() else {return}
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        contentView = view
        addSubview(contentView)
        dividerHeight.constant = 1/UIScreen.main.scale
    }
    
    func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "BottomButtonView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    func enablePrimaryBtn(isEnabled: Bool){
        primaryBtn.isEnabled = isEnabled
    }
    
    func enableSecondaryBtn(isEnabled: Bool){
        secondaryBtn.isEnabled = isEnabled
    }
    
    @IBAction func primayBtnTapped(_ sender: Any) {
        primaryBtnTapped?()
    }
    
    @IBAction func secondaryBtnTapped(_ sender: Any) {
        secondaryBtnTapped?()
    }
    
}
