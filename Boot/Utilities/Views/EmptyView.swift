//
//  EmptyView.swift
//  Boot
//
//  Created by Chris James on 30/04/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit
@IBDesignable
class EmptyView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var emptyImg: UIImageView!
    @IBOutlet weak var emptyLbl: UILabel!
    
    init(image: UIImage, text: String){
        super.init(frame: .zero)
        xibSetup(image: image, text: text)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup(image: UIImage? = nil, text: String? = nil) {
        guard let view = loadViewFromNib() else {return}
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        contentView = view
        addSubview(contentView)
        emptyImg.image = image
        emptyLbl.text = text
    }
    
    func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "EmptyView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    
}
