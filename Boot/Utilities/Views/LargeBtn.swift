//
//  LargeBtn.swift
//  Boot
//
//  Created by Chris James on 17/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

@IBDesignable
class LargeBtn: UIButton {
    
    lazy var buttonTint = UIApplication.shared.keyWindow?.tintColor
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        self.layer.cornerRadius = 10.0
        self.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        self.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        self.setTitleColor(#colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), for: .disabled)
        self.clipsToBounds = true
        self.isEnabled = true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = buttonTint ?? .blue
    }
    
    override var isEnabled: Bool {
        didSet {
            switch isEnabled {
            case true:
                self.layer.borderWidth = 0.0
                self.backgroundColor = buttonTint
                
            case false:
                self.layer.borderWidth = 1.0
                self.backgroundColor = .clear
            }
        }
    }
    
    override var isHighlighted:  Bool {
        didSet {
            switch isHighlighted {
            case true:
                self.backgroundColor = UIApplication.shared.keyWindow?.tintColor.withAlphaComponent(0.5)
            case false:
                self.backgroundColor = UIApplication.shared.keyWindow?.tintColor
            }
        }
    }
    
}

