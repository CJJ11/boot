//
//  TableViewFooter.swift
//  Boot
//
//  Created by Chris James on 29/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

class TableViewFooter: UIView {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var footerBtn: UIButton!
    @IBOutlet weak var footerLbl: UILabel!
    
    var footerBtnTapped: (() -> Void)?
    
    
    init(description: String? = nil, btnImage: UIImage? = nil){
        super.init(frame: .zero)
        xibSetup()
        footerBtn.setImage(btnImage, for: .normal)
        footerLbl.text = description
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        guard let view = loadViewFromNib() else {return}
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        contentView = view
        addSubview(contentView)
    }
    
    func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "TableViewFooter", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    @IBAction func footerBtnTapped(_ sender: Any) {
        footerBtnTapped?()
    }
    
    
}
