//
//  TableHeaderView.swift
//  Boot
//
//  Created by Chris James on 29/05/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import UIKit

class TableViewHeader: UIView {
    
    @IBOutlet weak var contentView: UIView!
    
    var archiveBtnTapped: (() -> Void)?
    
    init(){
        super.init(frame: .zero)
        xibSetup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        guard let view = loadViewFromNib() else {return}
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        contentView = view
        addSubview(contentView)
    }
    
    func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "TableViewHeader", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    @IBAction func archiveBtnTapped(_ sender: Any) {
        archiveBtnTapped?()
    }
    
    
}
