//
//  Constants.swift
//  Boot
//
//  Created by Chris James on 06/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

let defaultImageSize = CGSize(width: 1080, height: 1080)
