//
//  CheckTests.swift
//  BootTests
//
//  Created by Chris James on 05/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import XCTest
import Firebase
@testable import Boot



class CheckTests: XCTestCase {
    
    func test_init_checkNotNil(){
        let sut = setupCheck()
        XCTAssertNotNil(sut)
    }
    
//    func test_setPass_changePassState(){
//        var sut = setupCheck(pass: false)
//        XCTAssertFalse(sut.getPass())
//        XCTAssertNil(sut.getTimestamp())
//        XCTAssertNil(sut.uid)
//        XCTAssertNil(sut.getName())
//
//        sut.setPass(pass: true, uid: "sjdf", name: "Timbo")
//        let timestampOne = sut.getTimestamp()!
//        XCTAssertTrue(sut.getPass())
//        XCTAssertTrue(timestampOne.isEarlierThan(Timestamp(date: Date())))
//        XCTAssertEqual(sut.getUid(), "sjdf")
//        XCTAssertEqual(sut.getName(), "Timbo")
//
//        sut.setPass(pass: false, uid: "asdf", name: "Jimbo")
//        let timestampTwo = sut.getTimestamp()!
//        XCTAssertFalse(sut.getPass())
//        XCTAssertTrue(timestampTwo.isEarlierThan(Timestamp(date: Date())))
//        XCTAssertTrue(timestampTwo.isLaterThan(timestampOne))
//        XCTAssertEqual(sut.getUid(), "asdf")
//        XCTAssertEqual(sut.getName(), "Jimbo")
//    }
    
//    func test_toDictionary_initAndBack(){
//        let sut = setupCheck()
//        XCTAssertEqual(sut, Check(uid: "0", dictionary: sut.toDictionary()))
//        sut.setPass(pass: true, uid: "", name: "testname")
//        XCTAssertEqual(sut, Check(dictionary: sut.toDictionary()))
//    }
//
//    //MARK: Helper functions
//
//    func setupCheck(description: String = "test", pass: Bool = false) -> Check {
//        return Check(number: 0, type: CheckType.simpleBoolean, description: description)
//    }
    
}

private extension Timestamp {
    
    func isEarlierThan(_ timestamp: Timestamp) -> Bool {
        return self.compare(timestamp) == ComparisonResult.orderedAscending
    }
    
    func isLaterThan(_ timestamp: Timestamp) -> Bool {
        return self.compare(timestamp) == ComparisonResult.orderedDescending
    }
    
}
