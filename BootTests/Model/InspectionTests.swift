//
//  inspectionTests.swift
//  bootTests
//
//  Created by Chris James on 04/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import XCTest
import Firebase
@testable import Boot

class InspectionTests: XCTestCase {

    func test_init_withEmptyDictionary_assertNil(){
        let dict: [String: Any] = [:]
        let uid: String = ""
        XCTAssertNil(Inspection.init(uid: uid, dictionary: dict))
    }
    
    
    func test_init_withInvalidHazardInArray_assertNil(){
        let hazardRawValues = [-1]
        XCTAssertNil(setupInspection(hazardRawValues: hazardRawValues)?.hazards.first)
    }
    
    
//    func test_toDictionaryAndBack_withValidData(){
//        let sut = setupInspection()
//        let dict = sut?.toDictionary()
//        let inspection = Inspection(uid: sut!.uid, dictionary: dict!)
//        XCTAssertEqual(sut, inspection)
//    }
//
//    //MARK: Helper functions
//
//    private func setupInspection(location: String = "Ground floor",
//                                 scope: String = "Just do it",
//                                 editedBy: String = "Mum",
//                                 editedDate: Timestamp = Timestamp(date: Date()),
//                                 minAppVersion: Int = 010101,
//                                 hazardRawValues: [Int]? = [1, 2]) -> Inspection? {
//
//        let dict: [String: Any] = ["loc": location,
//                                   "haz": hazardRawValues as Any,
//                                   "hazOth": ["Greasy steps"] as Any,
//                                   "ppe": [0, 3, 6] as Any,
//                                   "ppeOth": ["Extra underpants"] as Any,
//                                   "scope": scope as Any,
//                                   "time": "3 hours" as Any,
//                                   "ver": 3 as Any,
//                                   "isol": [1] as Any,
//                                   "equip": "Hammer" as Any,
//                                   "editBy": editedBy as Any,
//                                   "editDate": editedDate as Any,
//                                   "minApp": minAppVersion as Any,
//                                   "steps": dummySteps() as Any,
//                                   "status": 1]
//        return Inspection.init(uid: "hjkl", dictionary: dict)
//    }
//
//    private func dummySteps() -> [String: Any]{
//        let check1 = Check(number: 0, type: .simpleBoolean, description: "I'm the first Check")
//        let step1 = Step(number: 0, description: "First step here I am", checks: [check1])
//        return ["0": step1!.toDictionary() as Any]
//    }

}
