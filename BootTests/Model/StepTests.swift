//
//  StepTests.swift
//  BootTests
//
//  Created by Chris James on 05/03/2019.
//  Copyright © 2019 Boot Pty Ltd. All rights reserved.
//

import XCTest
@testable import Boot

class StepTests: XCTestCase {
    
    func test_stepsWithNoChecks_returnsNil(){
        let sut = Step(number: 0, description: "", checks: [])
        XCTAssertNil(sut)
    }
    
    func test_checksPass_allPassing_returnTrue(){
        let sut = setupStep(checks: [Check(number: 0, type: .simpleBoolean, description: "test one")])
        XCTAssertFalse(sut!.checksPass())
    }
    
    func test_checksPass_oneFailing_returnFalse(){
        let sut = setupStep()
        XCTAssertFalse(sut!.checksPass())
    }
    
    func test_toDictAndBack_assertEqual(){
        let sut = setupStep()
        let dict = sut!.toDictionary()
        let step = Step(uid: "0", dictionary: dict)
        XCTAssertEqual(sut, step)
    }
    
    //MARK: Helper functions

    private func setupStep(checks: [Check]? = nil) -> Step? {
        if let checks = checks {
            return Step(number: 0, description: "Step Numero Uno", checks: checks)
        } else {
            let checkOne = Check(number: 0, type: .simpleBoolean, description: "test one")
            let checkTwo = Check(number: 1, type: .simpleBoolean, description: "test two")
            //checkTwo.setPass(pass: true, uid: "testUid", name: "testName")
            return Step(number: 0, description: "Step Numero Uno", checks: [checkOne, checkTwo])
        }
    }
    
    
}
