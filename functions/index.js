'use strict';

const serviceAccount = require('./service-account-credentials.json');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const Mux = require('@mux/mux-node');
const { Storage } = require('@google-cloud/storage');
const sharp = require('sharp');

const storage = new Storage();
const bucket = storage.bucket('boot-dab5b.appspot.com');

admin.initializeApp({
	credential: admin.credential.cert(serviceAccount),
	databaseURL: "https://boot-dab5b.firebaseio.com"
});

const muxClient = new Mux(
	functions.config().mux.dev.token,
	functions.config().mux.dev.secret);

const StorageDir = {
	holdUp: {
		fullRes: 'holdUp-images/full-res/',
		iPhoneThumb: 'holdUp-images/iPhone-thumb/',
		iPadThumb: 'holdUp-images/iPad-thumb/'
	}
};

const ThumbSize = {
	iPhone: {
		width: 800,
		height: 600
	},
	iPad: {
		width: 1600,
		height: 1200
	}
};

const MediaType = {
	image: 0,
	audio: 1,
	video: 2
};


//**************************
// Inspection Functions
//**************************

exports.inspectionUpdated = functions.firestore.document('minesites/cadia/inspections/{inspectionId}').onUpdate(async (change, context) => {
	const inspectionId = context.params.inspectionId;
	const inspection = change.after.data();
	const statusBefore = change.before.data().status;
	const status = inspection.status;
	const equipment = inspection.equip;

	const startedByUsername = inspection.firstActionedName;
	const startedByProfUrl = inspection.firstActionedProfUrl;

	const finishedByUsername = inspection.lastActionedName;
	const finishedByProfUrl = inspection.lastActionedProfUrl;

	var title = "";
	var body = "";
	var largeIcon = null;
	var notificationData = {};

	if (statusBefore === 1 && status === 2) {
		//Inspection started
		title = `Inspection Started`;
		body = `${equipment} inspection has been started by ${startedByUsername}.`;
		largeIcon = startedByProfUrl;
		notificationData = {
			type: 1,
			title: title,
			msg: body,
			imgUrl: startedByProfUrl,
			ts: admin.firestore.FieldValue.serverTimestamp(),
			seen: false,
			inspectionUid: inspectionId
		}
	} else if (statusBefore === 2 && status === 3) {
		//Inspection completed
		title = `Inspection Ready for Sign-Off`;
		body = `${equipment} inspection finished by ${finishedByUsername}.`;
		largeIcon = finishedByProfUrl;
		notificationData = {
			type: 2,
			title: title,
			msg: body,
			imgUrl: finishedByProfUrl,
			ts: admin.firestore.FieldValue.serverTimestamp(),
			seen: false,
			inspectionUid: inspectionId
		}
	} else {
		return null;
	}

	//send notification
	let minesiteSnapshot = await admin.firestore().collection('minesites').doc('cadia').get();
	let supervisorUid = minesiteSnapshot.get('supervisor');

	if (supervisorUid === null) {
		return console.log('There is no current supervisor');
	}

	return sendNotification(supervisorUid, notificationData, title, body, largeIcon, 'inspection', '', inspectionId);
});

//**************************
// Hold Up Functions
//**************************

exports.holdUpCreated = functions.firestore.
	document('minesites/{minesiteId}/holdUps/{holdUpId}').
	onCreate(async (snapshot, context) => {

		const path = snapshot.ref.path;
		const holdUp = snapshot.data();
		const holdUpId = context.params.holdUpId;
		const attachments = holdUp.att;
		const reporterUid = holdUp.repUid;
		const reporterName = holdUp.repUser;
		const reporterImg = holdUp.repImgUrl;
		const description = holdUp.desc;

		const videoPromises = [];
		const imagePromises = [];

		if (attachments === null) {
			console.log('No attachments')
			return null;
		}

		Object.keys(attachments).forEach((key) => {
			const attachment = attachments[key];
			const mediaType = attachment.mediaType;

			if (mediaType === MediaType.image) {
				imagePromises.push(holdUpThumbnails(attachment.fileName, key))

			} else if (mediaType === MediaType.video) {
				const options = {
					input: attachment.storageUrl,
					playback_policy: 'public',
					passthrough: path + ',' + key,
				}
				videoPromises.push(muxClient.Video.Assets.create(options));
			}
		});

		const videoResults = await Promise.all(videoPromises);
		const imageResults = await Promise.all(imagePromises);

		if (imageResults.length !== 0) {
			var updateData = {};
			imageResults.forEach((result) => {
				updateData[`att.${result.key}.iPhoneThumb`] = result.iPhoneThumbUrl;
				updateData[`att.${result.key}.iPadThumb`] = result.iPadThumbUrl;
			});
			await admin.firestore().doc(path).update(updateData);
		}

		//send notification
		let minesiteSnapshot = await admin.firestore().collection('minesites').doc('cadia').get();
		let supervisorUid = minesiteSnapshot.get('supervisor');

		if (supervisorUid === null) {
			return console.log('There is no current supervisor');
		}

		if (reporterUid === null || reporterUid === supervisorUid) {
			return console.log('Supervisor reported the hold up');
		}

		const title = `New Hold-Up`;
		const body = `${reporterName}: "${description}"`;

		var notificationData = {
			type: 0,
			title: title,
			msg: body,
			imgUrl: reporterImg,
			ts: admin.firestore.FieldValue.serverTimestamp(),
			seen: false,
			holdUpUid: holdUpId
		}

		return sendNotification(supervisorUid, notificationData, title, body, reporterImg, 'holdUp', holdUpId, '');
	});


exports.muxWebhook = functions.https.onRequest(async (req, res) => {
	const asset = req.body;
	const eventType = asset.type;
	const assetId = asset.id;
	const playbackIds = asset.data.playback_ids;
	const passthrough = asset.data.passthrough;

	console.log(asset);

	if (eventType !== 'video.asset.ready') {
		return res.status(200).send();
	}

	console.log('Video asset is ready');

	if (assetId === undefined) {
		console.error(new Error('Asset ID missing'));
		return res.end();
	}

	if (!Array.isArray(playbackIds) || playbackIds[0] === undefined) {
		console.error(new Error('Playback_id error'));
		return res.end();
	}

	if (passthrough === undefined) {
		console.error(new Error('Passthrough data missing'));
		return res.end();
	}

	const pathComponents = passthrough.split(",");
	if (Array.isArray(pathComponents) && pathComponents.length !== 2) {
		console.error(new Error('Error getting path and key from passthrough data'));
		return res.end();
	}

	const path = pathComponents[0];
	const key = pathComponents[1];
	const playbackId = playbackIds[0].id;
	const streamUrl = `https://stream.mux.com/${playbackId}.m3u8`;
	const iPadThumbUrl = `https://image.mux.com/${playbackId}/thumbnail.jpg` +
		`?width=${ThumbSize.iPad.width}&height=${ThumbSize.iPad.height}&fit_mode=crop`;
	const iPhoneThumbUrl = `https://image.mux.com/${playbackId}/thumbnail.jpg` +
		`?width=${ThumbSize.iPhone.width}&height=${ThumbSize.iPhone.height}&fit_mode=crop`;

	var updateData = {};
	updateData[`att.${key}.videoUrl`] = streamUrl;
	updateData[`att.${key}.iPadThumb`] = iPadThumbUrl;
	updateData[`att.${key}.iPhoneThumb`] = iPhoneThumbUrl;
	updateData[`att.${key}.assetId`] = assetId;

	await admin.firestore().doc(path).update(updateData)
	return res.status(200).send();
});


async function holdUpThumbnails(imageName, key) {
	const iPhoneThumb = generateThumbnail(
		imageName,
		StorageDir.holdUp.fullRes,
		StorageDir.holdUp.iPhoneThumb,
		ThumbSize.iPhone.width,
		ThumbSize.iPhone.height
	);

	const iPadThumb = generateThumbnail(
		imageName,
		StorageDir.holdUp.fullRes,
		StorageDir.holdUp.iPadThumb,
		ThumbSize.iPad.width,
		ThumbSize.iPad.height
	);

	const thumbResult = await Promise.all([iPhoneThumb, iPadThumb]);
	return { key: key, iPhoneThumbUrl: thumbResult[0], iPadThumbUrl: thumbResult[1] };
}


async function generateThumbnail(imageName, fileDir, thumbFileDir, width, height) {
	const file = bucket.file(fileDir + imageName);
	const thumbFile = bucket.file(thumbFileDir + imageName);

	const metadata = { contentType: 'image/jpg', };
	const thumbUploadStream = thumbFile.createWriteStream({ metadata });

	const pipeline = sharp();
	const options = { fit: 'inside', withoutEnlargement: true };

	pipeline.withMetadata().resize(width, height, options).pipe(thumbUploadStream);
	file.createReadStream().pipe(pipeline);

	await new Promise((resolve, reject) => thumbUploadStream.on('finish', resolve).on('error', reject));
	const urlData = await thumbFile.getSignedUrl({ action: 'read', expires: '03-01-2500' });
	return urlData[0];
}


//**************************
// Notification Helper Functions
//**************************

exports.notiCreated = functions.firestore.document('minesites/cadia/users/{userId}/notifications/{notiId}').onCreate((snapshot, context) => {
	const userId = context.params.userId;
	const userRef = admin.firestore().collection('minesites').doc('cadia').collection('users').doc(userId);

	const updateData = {
		notiCount: admin.firestore.FieldValue.increment(1)
	};
	return userRef.update(updateData);
});

exports.notiUpdated = functions.firestore.document('minesites/cadia/users/{userId}/notifications/{notiId}').onUpdate((change, context) => {
	const userId = context.params.userId;
	const userRef = admin.firestore().collection('minesites').doc('cadia').collection('users').doc(userId);

	const seenBefore = change.before.data().seen;
	const seen = change.after.data().seen;

	if (seen === true && seenBefore === false) {
		const updateData = {
			notiCount: admin.firestore.FieldValue.increment(-1)
		};
		return userRef.update(updateData);
	}

	return null;
});

exports.notiDeleted = functions.firestore.document('minesites/cadia/users/{userId}/notifications/{notiId}').onDelete((snapshot, context) => {
	const userId = context.params.userId;
	const userRef = admin.firestore().collection('minesites').doc('cadia').collection('users').doc(userId);

	const seenBefore = snapshot.data().seen;

	if (seenBefore === false) {
		const updateData = {
			notiCount: admin.firestore.FieldValue.increment(-1)
		};
		return userRef.update(updateData);
	}

	return null;
});

async function sendNotification(userID, notificationData, title, body, largeIcon, launchId, holdUpId, inspectionId) {
	const userRef = admin.firestore().collection('minesites').doc('cadia').collection('users').doc(userID);
	const user = await userRef.get();
	const notiTokens = Object.keys(user.get('notiTokens') || {}); //as map
	const unseenCount = user.get('notiCount');
	const badgeValue = (unseenCount || 0) + 1;

	await userRef.collection('notifications').add(notificationData);

	if (notiTokens.length === 0) {
		return console.log('There are no notification tokens to send to');
	}

	console.log(`There are ${notiTokens.length} tokens to send notifications to`);

	var payloadData = {
		title: title,
		body: body
	};

	if (largeIcon !== null) {
		payloadData['largeIcon'] = largeIcon;
	}

	const payloadApns = {
		payload: {
			aps: {
				alert: {
					title: title,
					body: body
				},
				badge: badgeValue,
				sound: 'default'
			},
			launch_id: launchId,
			hold_up_id: holdUpId, 
			inspection_id: inspectionId
		}
	};

	var message = {
		data: payloadData,
		apns: payloadApns,
		tokens: notiTokens
	};

	const batchResponse = await admin.messaging().sendMulticast(message);
	const tokensToRemove = [];

	if (batchResponse.failureCount > 0) {
		batchResponse.responses.forEach((sendResponse, index) => {
			const error = sendResponse.error;

			if (error) {
				console.error('Failure sending notification to', tokens[index], error);
				// Cleanup the tokens who are not registered anymore.
				if (error.code === 'messaging/invalid-registration-token' ||
					error.code === 'messaging/registration-token-not-registered') {

					var updateData = {};
					updateData[`notiTokens.${tokens[index]}`] = admin.firestore.FieldValue.delete();
					tokensToRemove.push(userRef.update(updateData));
				}
			}
		});
	} else {
		console.log(`${batchResponse.responses.length} notifications sent with no errors`);
	}

	return Promise.all(tokensToRemove);
}

